﻿using PolyPaint.Services;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Logique d'interaction pour TutorielClassique.xaml
    /// </summary>
    public partial class TutorielClassique : UserControl
    {
        int currentSlide = 0;
        int MAX_SLIDES = 6;

        List<string> descriptions = new List<string>()
        {
            "Général:\nEn équipe de 2, un des membres dessine et l'autre devine.\nDevinez plus de dessins que l'équipe adversaire pour gagner!",

            "Préparation:\nAvant le début de la partie, les joueurs sont répartis en deux équipes: ORANGE et BLEUE.\nSi vous n'êtes pas 4 joueurs, il est possible d'ajouter 1 JOUEUR VIRTUEL par équipe.",

            "Début de partie:\nAprès avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\nLorsque celle-ci disparait, la partie commence immédiatement.",

            "Déroulement de partie:\nVotre tour:\nSi vous voyez un mot en gras en bas de l'écran, vous avez le rôle de DESSINEUR.\nAvec les outils de dessin, essayez de dessiner le mot en gras.",

            "Si vous voyez un champ pour deviner en bas de l'écran, vous avez le rôle de DEVINEUR. Vous avez une tentative pour deviner le mot que votre coéquipier dessine et gagner un point.\nSi vous donnez une mauvaise réponse, un droit de réplique est donné à l'équipe adversaire.\nEnsuite c'est le tour de l'équipe adverse.",

            "Tour de l'équipe adverse:\nSi vous voyez « Attendez votre tour...», c'est le tour de l'équipe adverse.\n Votre tour arrivera après que l'équipe adverse gagne un point ou vous donne un droit de réplique.",

            "Fin de partie:\nUne manche est finie après que chaque équipe joue leur tour une fois.\nLa partie se termine après deux manches. L'équipe gagnante est celle qui a accumulé le plus de points!",
        };

        public TutorielClassique()
        {
            InitializeComponent();

            ImageBorder0.PreviewMouseDown += (s, e) => SlideTo(0);
            ImageBorder1.PreviewMouseDown += (s, e) => SlideTo(1);
            ImageBorder2.PreviewMouseDown += (s, e) => SlideTo(2);
            ImageBorder3.PreviewMouseDown += (s, e) => SlideTo(3);
            ImageBorder4.PreviewMouseDown += (s, e) => SlideTo(4);
            ImageBorder5.PreviewMouseDown += (s, e) => SlideTo(5);
            ImageBorder6.PreviewMouseDown += (s, e) => SlideTo(6);
        }

        public void CloseTutorial(object sender, MouseEventArgs e)
        {
            SlideTo(0);
            Navigator.HideTutorial();
        }

        public void PreviousSlide(object sender, MouseEventArgs e)
        {
            var slide = currentSlide - 1;

            if (slide == -1)
            {
                slide = MAX_SLIDES;
            }

            SlideTo(slide);
        }

        public void NextSlide(object sender, MouseEventArgs e)
        {
            var slide = currentSlide + 1;

            if (slide == MAX_SLIDES + 1)
            {
                slide = 0;
            }

            SlideTo(slide);
        }

        public void SlideTo(int slide)
        {
            Dispatcher.Invoke(() =>
            {
                ((Border)FindName("ImageBorder" + currentSlide)).Background = (SolidColorBrush)new BrushConverter().ConvertFrom("Gray"); ;
                currentSlide = slide;
                Description.Text = descriptions[currentSlide];
                ((Border)FindName("ImageBorder" + currentSlide)).Background = (SolidColorBrush)new BrushConverter().ConvertFrom("RoyalBlue");
                ((Image)FindName("TutorialImage")).Source = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Tutorial/tutorial{currentSlide + 1}.PNG"));
            });
        }
    }
}
