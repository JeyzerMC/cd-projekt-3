package com.example.android.data

import android.graphics.Path
import com.example.android.viewmodel.PaintBrushStyle

class FingerPath(
    var color: Int,
    var strokeWidth: Int,
    var path: Path,
    var brushStyle: PaintBrushStyle
)