﻿using System.Windows.Ink;
using System.Windows.Media;

namespace PolyPaint.Modeles.Dessin
{
    public class DessinStroke
    {
        public double X { get; set; }

        public double Y { get; set; }

        public double X2 { get; set; }

        public double Y2 { get; set; }

        public Color Color { get; set; }

        public StylusTip Tip { get; set; }

        public double Width { get; set; }
    }
}
