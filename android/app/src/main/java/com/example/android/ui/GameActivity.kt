package com.example.android.ui

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.media.MediaPlayer
import android.os.Bundle
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.android.R
import com.example.android.data.EndGameStats
import com.example.android.data.GameMode
import com.example.android.data.GameState
import com.example.android.data.Stroke
import com.example.android.data.getUserColor
import com.example.android.databinding.ActivityGameBinding
import com.example.android.viewmodel.GameViewModel
import com.example.android.viewmodel.Lobby
import com.example.android.viewmodel.PaintBrushStyle
import com.example.android.viewmodel.Player
import com.example.android.viewmodel.Profile
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.UUID

class GameActivity : AppCompatActivity() {
    private var isErasing: Boolean = false
    val model : GameViewModel by viewModels ()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityGameBinding = DataBindingUtil.setContentView(this,
            R.layout.activity_game
        )
        binding.model = model
        binding.lifecycleOwner = this

        model.lobbyId.value = (intent.getStringExtra(id))

        // TODO: change when we handle the deletion of a game where the
        model.isCreator.value = true // (intent.getBooleanExtra(isCreator, false))


        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        paintView!!.init(metrics)
        createToolBar()
        GlobalScope.launch {
            when (val message = model.joinGameChannel()) {
                "ok" -> {
                    toast("Rejoint le jeu ${model.lobbyId!!.value}")
                    paintView.setActivePaintView(true)
                }
                else -> toast("Une erreur s'est produite: $message")
            }
        }

        model.newStroke.observe(this, Observer<Stroke>{ newStroke ->
            paintView.drawOneStroke(newStroke)
        })

        model.gameState.observe(this, Observer<GameState> {gameState ->
            val lobby = model.currentLobby.value?: return@Observer

            paintView.setActivePaintView(false)
            closeKeyboard()
            when(gameState) {
                GameState.WAIT -> {
                    paintView.onClearCanvas()
                    paintView.setActivePaintView(true)
                }
                GameState.GUESS, GameState.CLEAR_WATCH  -> {
                    paintView.onClearCanvas()
                }
                GameState.DRAW -> {
                    paintView.onClearCanvas()
                    paintView.setActivePaintView(true)
                }
                GameState.REPLY, GameState.WATCH_REPLY, GameState.WATCH-> {
                }
                GameState.END -> {
                    toast("fin de la partie")
                    if (lobby.isWinner())
                    {
                        playEndSound(true)
                    }
                }
                GameState.ABOUT_TO_START -> {
                    paintView.onClearCanvas()
                }
                GameState.VOTE -> {
                    println("VOTING TIME")
                }
                GameState.ERROR -> {
                    toast("Une erreur s'est produite")
                    onBackPressed()
                }
            }
        })

        model.goodGuess.observe(this, Observer{ isGoodGuess ->
            // TODO: figure out why this shake on second game join
            if (isGoodGuess) {
                // TODO: play good sound
            } else {
                shake()
            }

        })

        model.lobbies.observe(this, Observer {
            val lobby = it[model.lobbyId.value!!] ?: return@Observer
            model.currentLobby.value = lobby
        })


        model.endGameStats.observe(this, Observer {stats ->
            when (model.gameMode.value!!) {
                GameMode.CLASSIC -> {updateEndGameStatsClassic(stats)}
                GameMode.SOLOSPRINT -> {updateEndGameStatsSolo(stats)}
                GameMode.FFA -> {updateEndGameStatsFfa(stats)}
            }
        })

        model.currentLobby.observe(this, Observer { lobby ->
            val gameMode = model.gameMode.value ?: return@Observer

            when (lobby.gameMode) {
                GameMode.CLASSIC -> {
                    updatePlayersClassic(lobby, lobby.currentDrawer)
                }
                GameMode.SOLOSPRINT -> {
                    updatePlayersSolo(lobby, lobby.currentDrawer)
                }
                GameMode.FFA -> {
                    updatePlayersFfa(lobby, lobby.currentDrawer)
                }
            }

            // TODO: change this when no need for test
            model.canAddAI.value = lobby.nbPlayers < 4 && lobby.teams.any{ (a, b) ->
                // true
                b.count { user -> user.name.startsWith("AI")}  < 1
            }
            // model.canAddAI.value = lobby.teams.any{ (team, users) -> users.size < 2}
            model.canVote.value = lobby.currentDrawer != lobby.myUsername

            // TODO: find better way to get these values
            when(gameMode) {
                GameMode.CLASSIC -> {
                    model.canStartGame.value = lobby.nbPlayers == 4
                }
                GameMode.SOLOSPRINT -> {
                    model.canStartGame.value = true
                }
                GameMode.FFA -> {
                    model.canStartGame.value = lobby.nbPlayers > 1
                }
            }
        })

        val bmpPos = BitmapFactory.decodeResource(resources, resources.getIdentifier("thumbs_up", "drawable", packageName))
        val bmpScaledPos = Bitmap.createScaledBitmap(bmpPos, 50, 50, false)
        val thumbUp =  BitmapDrawable(resources, bmpScaledPos)

        findViewById<Button>(R.id.positiveVoteButton).setCompoundDrawablesWithIntrinsicBounds(thumbUp, null, null, null)

        val bmpNeg = BitmapFactory.decodeResource(resources, resources.getIdentifier("thumbs_down", "drawable", packageName))
        val bmpScaledNeg = Bitmap.createScaledBitmap(bmpNeg, 50, 50, false)
        val thumbDown =  BitmapDrawable(resources, bmpScaledNeg)

        findViewById<Button>(R.id.negativeVoteButton).setCompoundDrawablesWithIntrinsicBounds(thumbDown, null, null, null)

        val guessInputId = when(model.gameMode.value!!) {
            GameMode.CLASSIC -> R.id.guessInputClassic
            GameMode.SOLOSPRINT -> R.id.guessInputSolo
            GameMode.FFA -> R.id.guessInputFfa
            GameMode.NONE -> -1
        }

        findViewById<EditText>(guessInputId).setOnEditorActionListener {v, actionId, _ ->
            when(actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    val editText = v as EditText
                    model.sendGuess(editText.text.toString())
                    editText.setText("")
                    closeKeyboard()

                    return@setOnEditorActionListener true
                }
                else -> false
            }
        }
    }

    private fun updatePlayersFfa(lobby: Lobby, currentTeam: String?) {
        val layout = findViewById<LinearLayout>(R.id.ffaPlayersLayout)
        layout.removeAllViews()

        for (player in lobby.teams["all"]!!)  {
            val userLayout = createPlayerLayout(player, lobby.currentDrawer, currentTeam)
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.MATCH_PARENT )
            params.gravity = Gravity.CENTER_VERTICAL
            userLayout.layoutParams = params
            userLayout.setBackgroundColor(getUserColor(player.name))
            layout.addView(userLayout)
        }
    }

    private fun updatePlayersSolo(lobby: Lobby, currentTeam: String?) {
        val layout = findViewById<LinearLayout>(R.id.soloPlayersLayout)
        layout.removeAllViews()
        val player = lobby.teams["all"]!!.get(0)
        val userLayout = createPlayerLayout(player, lobby.currentDrawer, currentTeam)
        layout.addView(userLayout)
    }

    fun updateEndGameStatsClassic(stats: List<EndGameStats>) {
        val layout = findViewById<LinearLayout>(R.id.classicStatsLayout)
        layout.removeAllViews()

        val headRow = TableRow(this)
        headRow.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f)
        headRow.setBackgroundColor(Color.DKGRAY)

        listOf("Joueur", "Erreurs", "Repliques", "Score").forEach {
            val a = TextView(this)
            a.text = it
            a.textSize *= 1.2.toFloat()
            a.setTextColor(Color.WHITE)
            a.gravity = Gravity.CENTER
            a.setPadding(5,10,5,10)
            headRow.addView(a)
        }
        layout.addView(headRow)

        stats.forEachIndexed { _, playerStats ->
            val row = TableRow(this)
            row.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f)

            val usernameText = TextView(this)
            usernameText.text = playerStats.username
            usernameText.setTextColor(Color.WHITE)
            usernameText.textSize *= 1.1.toFloat()
            usernameText.gravity = Gravity.CENTER
            usernameText.setPadding(5,10,5,10)
            row.addView(usernameText)

            val wrongGuessesText = TextView(this)
            wrongGuessesText.text = "${playerStats.wrongGuesses}"
            wrongGuessesText.setTextColor(Color.WHITE)
            wrongGuessesText.textSize *= 1.1.toFloat()
            wrongGuessesText.gravity = Gravity.CENTER
            wrongGuessesText.setPadding(5,10,5,10)
            row.addView(wrongGuessesText)

            val correctRepliesText = TextView(this)
            correctRepliesText.text = "${playerStats.correctReplies}"
            correctRepliesText.setTextColor(Color.WHITE)
            correctRepliesText.textSize *= 1.1.toFloat()
            correctRepliesText.gravity = Gravity.CENTER
            correctRepliesText.setPadding(5,10,5,10)
            row.addView(correctRepliesText)

            val scoreText = TextView(this)
            scoreText.text = "${playerStats.score}"
            scoreText.setTextColor(Color.WHITE)
            scoreText.textSize *= 1.1.toFloat()
            scoreText.gravity = Gravity.CENTER
            scoreText.setPadding(5,10,5,10)
            row.addView(scoreText)

            layout.addView(row)
        }

    }

    fun updateEndGameStatsSolo(stats: List<EndGameStats>) {
        val layout = findViewById<LinearLayout>(R.id.soloStatsLayout)
        layout.removeAllViews()

        val headRow = TableRow(this)
        // headRow.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT)
        headRow.setBackgroundColor(Color.DKGRAY)
        listOf("Joueur", "Streak", "Erreurs", "Temps moyen").forEach {
            val a = TextView(this)
            a.text = it
            a.textSize *= 1.2.toFloat()
            a.setTextColor(Color.WHITE)
            a.gravity = Gravity.CENTER
            a.setPadding(5,10,5,10)
            // a.layoutParams = TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f)
            headRow.addView(a)
        }

        layout.addView(headRow)

        stats.forEachIndexed { _, playerStats ->
            val row = TableRow(this)
            row.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f)

            val usernameText = TextView(this)
            usernameText.text = playerStats.username

            usernameText.setTextColor(Color.WHITE)
            usernameText.textSize *= 1.1.toFloat()
            usernameText.gravity = Gravity.CENTER
            usernameText.setPadding(5,10,5,10)

            row.addView(usernameText)

            val biggestConsecutiveGuessesText = TextView(this)
            biggestConsecutiveGuessesText.text = "${playerStats.biggestConsecutiveGuesses}"

            biggestConsecutiveGuessesText.setTextColor(Color.WHITE)
            biggestConsecutiveGuessesText.textSize *= 1.1.toFloat()
            biggestConsecutiveGuessesText.gravity = Gravity.CENTER
            biggestConsecutiveGuessesText.setPadding(5,10,5,10)

            row.addView(biggestConsecutiveGuessesText)

            val wrongGuessesText = TextView(this)
            wrongGuessesText.text = "${playerStats.wrongGuesses}"
            wrongGuessesText.setTextColor(Color.WHITE)

            wrongGuessesText.setTextColor(Color.WHITE)
            wrongGuessesText.textSize *= 1.1.toFloat()
            wrongGuessesText.gravity = Gravity.CENTER
            wrongGuessesText.setPadding(5,10,5,10)

            row.addView(wrongGuessesText)

            val averageTimeText = TextView(this)
            averageTimeText.text = "${playerStats.averageTime}"

            averageTimeText.setTextColor(Color.WHITE)
            averageTimeText.textSize *= 1.1.toFloat()
            averageTimeText.gravity = Gravity.CENTER
            averageTimeText.setPadding(5,10,5,10)

            row.addView(averageTimeText)

            layout.addView(row)
        }

    }

    fun updateEndGameStatsFfa(stats: List<EndGameStats>) {
        val layout = findViewById<LinearLayout>(R.id.ffaStatsLayout)
        layout.removeAllViews()

        val headRow = TableRow(this)
        // headRow.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f)
        headRow.setBackgroundColor(Color.DKGRAY)
        listOf("Joueur", "Score").forEach {
            val a = TextView(this)
            a.text = it
            a.textSize *= 1.2.toFloat()
            a.gravity = Gravity.CENTER
            a.setPadding(5,10,5,10)
            // a.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f)
            a.setTextColor(Color.WHITE)
            headRow.addView(a)
        }

        layout.addView(headRow)

        stats.forEachIndexed { _, playerStats ->
            val row = TableRow(this)
            // row.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f)

            val usernameText = TextView(this)
            usernameText.text = playerStats.username
            usernameText.textSize *= 1.1.toFloat()
            usernameText.setPadding(5,10,5,10)
            usernameText.gravity = Gravity.CENTER
            usernameText.setTextColor(Color.WHITE)
            row.addView(usernameText)

            val scoreText = TextView(this)
            scoreText.text = "${playerStats.score}"
            scoreText.textSize *= 1.1.toFloat()
            scoreText.gravity = Gravity.CENTER
            scoreText.setTextColor(Color.WHITE)
            scoreText.setPadding(5,10,5,10)

            row.addView(scoreText)
            layout.addView(row)
        }

    }

    private fun closeKeyboard() {
        val view = findViewById<View>(android.R.id.content);
        val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun updatePlayersClassic(lobby: Lobby, currentTeam: String?) {
        val layout = findViewById<LinearLayout>(R.id.classicPlayersLayout)
        layout.removeAllViews()
        var color = ResourcesCompat.getColor(resources, R.color.orange, null)
        for ((team, players) in lobby.teams) {
            val teamLayout = LinearLayout(this)
            teamLayout.orientation = LinearLayout.VERTICAL
            // TODO: get color from team name or index
            teamLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT)
            teamLayout.setBackgroundColor(color)
            color = ResourcesCompat.getColor(resources, R.color.bluePrimary, null)

            if (currentTeam == team) {
                teamLayout.background = resources.getDrawable(R.drawable.border)
            }

            // val teamTitle = TextView(this)
            //TODO : get a better team name based on index

            // teamTitle.text = "$team"
            // teamTitle.textSize -= 10
            // teamTitle.setTextColor(Color.WHITE)
            // teamTitle.setGravity(Gravity.CENTER)
            // teamLayout.addView(teamTitle)

            val usersLayout = LinearLayout(this)
            usersLayout.orientation = LinearLayout.HORIZONTAL

            for (player in players)  {
                val userLayout = createPlayerLayout(player, lobby.currentDrawer, currentTeam)
                usersLayout.addView(userLayout)
            }
            teamLayout.addView(usersLayout)
            layout.addView(teamLayout)
        }
    }

    fun createPlayerLayout(player: Player, currentDrawer: String?, currentTeam: String?): LinearLayout {
        val userLayout = LinearLayout(this)
        userLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT)
        userLayout.gravity = Gravity.CENTER_VERTICAL
        userLayout.setPadding(50, 0, 50, 0)
        userLayout.orientation = LinearLayout.HORIZONTAL

        val userAvatar = ImageView(this)
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0,0,10,0)
        userAvatar.layoutParams = params

        val bitmapAvatar = BitmapFactory.decodeResource(resources, resources.getIdentifier("avatar_${player.avatarId}", "drawable", packageName))
        userAvatar.setImageBitmap(
            Bitmap.createScaledBitmap(
                bitmapAvatar,
                60,
                60,
                true)
        )

        userLayout.addView(userAvatar)

        val userInfoLayout = LinearLayout(this)

        userInfoLayout.orientation = LinearLayout.VERTICAL
        userLayout.addView(userInfoLayout)

        if (player.name == currentDrawer && player.team == currentTeam) {
            val drawerImage = ImageView(this)
            drawerImage.setImageResource(R.drawable.crayon)
            userLayout.addView(drawerImage)
        }



        val username = TextView(this)
        username.text = player.name
        username.setTextColor(Color.WHITE)
        userInfoLayout.addView(username)

        if (!player.isAI) {
            val layout = LinearLayout(this)
            layout.orientation = LinearLayout.HORIZONTAL

            val titleText = TextView(this)
            titleText.text = Profile.titles[player.titleId].name
            titleText.setTextColor(Color.YELLOW)

            val badgeImage = ImageView(this)
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT)
            params.setMargins(0,0,10,0)
            userAvatar.layoutParams = params

            val bitmapAvatar = BitmapFactory.decodeResource(resources, resources.getIdentifier("badge${player.titleId}", "drawable", packageName))
            badgeImage.setImageBitmap(
                Bitmap.createScaledBitmap(
                    bitmapAvatar,
                    20,
                    20,
                    true)
            )

            layout.addView(badgeImage)
            layout.addView(titleText)

            userInfoLayout.addView(layout)
        }

        val score = TextView(this)
        score.text = "${player.score} points"
        score.setTextColor(Color.WHITE)
        userInfoLayout.addView(score)

        return userLayout
    }

    fun shake() {
        runOnUiThread {
            val layout = findViewById<ConstraintLayout>(R.id.layout)
            layout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        }
    }

    fun onSaveDrawingBtnClick(view: View) {
        try {
            val uri: String = saveImageToInternalStorage(paintView.getDrawableCanvas())
            toast("Imgage sauvegardee")
        } catch (e: Exception) {
            toast("Erreur durant la sauvegarde")
        }
    }

    private fun saveImageToInternalStorage(drawable: Drawable): String {
        // Get the bitmap from drawable object
        val bitmap = (drawable as BitmapDrawable).bitmap

        return MediaStore.Images.Media.insertImage(contentResolver, bitmap, UUID.randomUUID().toString(), "life is beautiful");
    }

    fun onBackToLobbyBtnClick(view: View) {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        leaveLobby()
        model.gameMode.postValue(GameMode.NONE)
    }

    fun onAddAiButtonClick(view: View) {
        model.addAI()
    }

    fun onPositiveVoteBtnClick(view: View) {
        model.sendVote(1)
    }

    fun onNegativeVoteBtnClick(view: View) {
        model.sendVote(-1)
    }

    private fun leaveLobby() {
        model.leaveLobby()
    }

    private fun toast(message: String) {
        runOnUiThread {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(this, message, duration)
            toast.show()
        }
    }

    private fun createToolBar(){
        val toolLayout = findViewById<LinearLayout>(R.id.toolBarLayout)
        val colorLayout = LinearLayout(this)
        colorLayout.orientation = LinearLayout.VERTICAL

        addEraseToggleButton(toolLayout,colorLayout)
        // addClearButton(toolLayout)
        addBrushStyleButton(toolLayout)
        addSizeControls(toolLayout)
        addColorButtons(toolLayout, colorLayout)
    }

    private fun addEraseToggleButton(layout: LinearLayout, colorLayout : LinearLayout) {
        val eraseButton = ImageButton(this)
        eraseButton.id = View.generateViewId()
        eraseButton.setImageResource(R.drawable.efface)

        val params = LinearLayout.LayoutParams(100,100) //TODO: change to flexible size??
        eraseButton.layoutParams = params
        eraseButton.scaleType = ImageView.ScaleType.FIT_XY

        eraseButton.setOnClickListener {
            paintView.onToggleEraser()
            if(!isErasing) {
                eraseButton.setImageResource(R.drawable.crayon)
                colorLayout.visibility = View.INVISIBLE
                isErasing = true
            } else {
                eraseButton.setImageResource(R.drawable.efface)
                colorLayout.visibility = View.VISIBLE
                isErasing = false
            }
        }
        layout.addView(eraseButton)
    }

    fun onStartGameButtonClick(view: View) {
        model.startGame()
    }

    private fun addClearButton(layout: LinearLayout) {
        val clearButton = Button(this)
        clearButton.id = View.generateViewId()
        clearButton.text = "Effacer tout"
        clearButton.setOnClickListener {
            paintView.onClearCanvas()
        }
        layout.addView(clearButton)
    }

    private fun addBrushStyleButton(layout: LinearLayout) {
        val brushStyleLayout = LinearLayout(this)
        brushStyleLayout.orientation = LinearLayout.HORIZONTAL
        brushStyleLayout.weightSum = 9f

        val roundButton = Button(this)
        roundButton.id = View.generateViewId()
        roundButton.text = "●"
        roundButton.setOnClickListener {
            paintView.onChangeBrushStyle(PaintBrushStyle.ROUND)
        }

        val params = LinearLayout.LayoutParams(70, LinearLayout.LayoutParams.WRAP_CONTENT)
        roundButton.layoutParams = params

        val squareButton = Button(this)
        squareButton.id = View.generateViewId()
        squareButton.text = "■"
        squareButton.setOnClickListener {
            paintView.onChangeBrushStyle(PaintBrushStyle.SQUARE)
        }
        squareButton.layoutParams = params

        brushStyleLayout.addView(roundButton)
        brushStyleLayout.addView(squareButton)
        layout.addView(brushStyleLayout)
    }

    private fun addSizeControls(layout: LinearLayout){
        val bar = SeekBar(this)
        bar.setPadding(20, 20, 20, 20)
        bar.max = 150
        bar.progress = 2
        bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                 paintView.onChangeSize(progress)
            }
        })

        layout.addView(bar)
    }

    private fun addColorButtons(layout : LinearLayout, colorLayout : LinearLayout){
        val colors = listOf(Color.BLACK, Color.GRAY, Color.BLUE, Color.CYAN, Color.MAGENTA, Color.RED, Color.GREEN, Color.YELLOW)

        for(color in colors){
            val button = Button(this)

            val shape = GradientDrawable()
            shape.shape = GradientDrawable.OVAL
            shape.setColor(color)
            button.background = shape

            val params = LinearLayout.LayoutParams(70, 70)
            params.gravity = Gravity.CENTER_HORIZONTAL
            params.setMargins(0,10,0,10)
            button.layoutParams = params

            button.setOnClickListener { paintView.onChangeColor(color) }
            button.id = View.generateViewId()
            colorLayout.addView(button)
        }
        layout.addView(colorLayout)
    }

    fun playEndSound(isWinner: Boolean) {
        var sound = MediaPlayer.create(this, if (isWinner) R.raw.victory_sound else R.raw.lose_sound)
        sound.start()
    }
    companion object {
        const val id = "id"
        const val isCreator = "isCreator"
    }
}
