defmodule Server.Agents.FfaGameAgent do
  use Agent
  alias Server.Repo
  alias Server.Structs.User
  alias Server.Structs.Game
  alias Server.Models.User, as: UserModel

  alias Server.Game.BaseGame
  @name __MODULE__
  @min_players 1
  @max_players 4

  def start_link(_) do
    Agent.start_link(fn -> Map.new() end, name: @name)
  end

  def create_lobby(username) do
    # id = Ecto.UUID.generate()
    id = BaseGame.get_random_word() <> BaseGame.get_random_word()

    user = get_user_from_repo(username)

    Agent.update(@name, fn map ->
      map
      |> Map.put_new(id, %Game{
        nb_players: @min_players,
        max_players: @max_players,
        users: [
          %User{username: user.username, avatar: user.current_avatar, title: user.selected_title}
        ],
        type: "ffa"
      })
    end)

    IO.puts("creating lobby for ffa")

    %{id: id, team: ""}
  end

  def join_lobby(id, username) do
    lobby = get_lobby(id)

    user = get_user_from_repo(username)

    update_lobby(id, %Game{
      lobby
      | open: !(lobby.nb_players + 1 == @max_players),
        nb_players: lobby.nb_players + 1,
        users:
          lobby.users ++
            [
              %User{
                username: user.username,
                avatar: user.current_avatar,
                title: user.selected_title
              }
            ]
    })

    ""
  end

  def leave_lobby(id, username) do
    lobby = get_lobby(id)

    # IO.puts("FFA GAME BEFORE LEAVING=====================")
    # IO.inspect(lobby)
    # IO.puts("FFA GAME BEFORE LEAVING=====================")

    if lobby do
      game_contains_user =
        Enum.any?(lobby.users, fn user ->
          user.username == username
        end)

      if game_contains_user do
        case lobby.nb_players do
          @min_players ->
            if lobby.timer_pid do
              Process.exit(lobby.timer_pid, :ok)
            end

            Agent.update(@name, fn map ->
              map
              |> Map.delete(id)
            end)

          _ ->
            update_lobby(id, %{
              lobby
              | users: Enum.filter(lobby.users, fn user -> username != user.username end),
                open:
                  if lobby.end_game do
                    false
                  else
                    true
                  end,
                nb_players: lobby.nb_players - 1
            })
        end
      end
    end
  end

  # def change_drawer(id) do
  #   lobby = get_lobby(id)
  #   next_drawer = Enum.random(lobby.users)

  #   if next_drawer == lobby.current_drawer do
  #     change_drawer(id)
  #   else
  #     update_lobby(id, %{
  #       lobby
  #       | current_drawer: next_drawer,
  #         current_word: Enum.random(@random_words)
  #     })
  #   end
  # end

  # def start_game(id) do
  #   lobby = get_lobby(id)

  #   update_lobby(id, %{lobby | started: true})
  # end

  def delete_lobby(id) do
    Agent.update(@name, fn map ->
      map
      |> Map.delete(id)
    end)
  end

  def get_lobbies_for_update() do
    lobbies = Agent.get(@name, fn map -> map end)

    for {id, lobby} <- lobbies, into: %{}, do: {id, %{lobby | ai_draw_pid: nil, timer_pid: nil}}
  end

  def get_lobbies() do
    lobbies = Agent.get(@name, fn map -> map end)

    # IO.puts("FFA GET LOBBIES==================")
    # IO.inspect(lobbies)
    # IO.puts("FFA GET LOBBIES==================")
    lobbies
  end

  def get_open_lobbies() do
    all_lobbies = get_lobbies()

    :maps.filter(
      fn _id, lobby_params -> lobby_params.open and !lobby_params.started end,
      all_lobbies
    )
  end

  def get_lobby(id) do
    lobbies = get_lobbies()
    lobby = Map.get(lobbies, id)
    lobby
  end

  def is_started(id) do
    lobby = get_lobby(id)
    lobby.started
  end

  def update_lobby(id, lobby_params) do
    Agent.update(@name, fn map ->
      map
      |> Map.put(id, lobby_params)
    end)
  end

  def get_user_from_repo(username) do
    Repo.get_by!(UserModel, username: username)
  end
end
