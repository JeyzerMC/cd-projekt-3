package com.example.android.data

data class RegisterInfo(var firstName: String, var lastName: String, var username: String, var password: String, var passwordConfirmation: String) {
    fun validate(): ArrayList<String> {
        val errors: ArrayList<String> = ArrayList()

        if (firstName.isEmpty()) {
            errors.add("Le prenom doit etre rempli")
        }
        if (lastName.isEmpty()) {
            errors.add("Le nom de famille doit etre rempli")
        }
        if (username.isEmpty()) {
            errors.add("Le nom d'utilisateur doit etre rempli")
        }
        if (password.isEmpty()) {
            errors.add("Le mot de passe doit etre rempli")
        }
        if (passwordConfirmation.isEmpty()) {
            errors.add("La confirmation de mot de passe doit etre rempli")
        }

        if (password != passwordConfirmation) {
           errors.add("La confirmation de mot de passe doit etre pareil au mot de passe")
        }

        return errors
    }

    override fun toString(): String {
        return "firstname:$firstName lastname:$lastName username:$username password:$password password2:$passwordConfirmation"
    }
}