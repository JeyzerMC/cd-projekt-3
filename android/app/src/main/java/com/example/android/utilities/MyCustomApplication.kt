package com.example.android.utilities

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class MyCustomApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        appGlobalContext = applicationContext

    }

    companion object {
        lateinit var appGlobalContext: Context
    }
}