﻿using PolyPaint.Modeles;
using PolyPaint.Services;
using PolyPaint.VueModeles;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        private LoginViewModel loginViewModel;

        public string LoginUsername { get; set; } = HttpService.DefaultUser;

        public LoginPage()
        {
            InitializeComponent();
            usernameInput.Focus();
            loginViewModel = new LoginViewModel();
            Navigator.GoBackButton.Visibility = Visibility.Collapsed;
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            SendClick();
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Vues/RegisterPage.xaml");
        }

        private void ConnectionChanged(int connectionStatus)
        {
            switch(connectionStatus)
            {
                case 0:
                    Dispatcher.Invoke(() =>
                    {
                        loginButton.IsEnabled = true;
                        errorLabel.Text = "Erreur de connexion: vérifier les informations d'authentification";
                    });
                    break;
                case 1:
                    Dispatcher.Invoke(() =>
                    {
                        SocketService.ConnectionChange -= ConnectionChanged;
                        Navigator.Navigate("Vues/HomePage.xaml");
                        Navigator.OnLogin();
                    });
                    break;
                case 2:
                    Dispatcher.Invoke(() =>
                    {
                        loginButton.IsEnabled = true;
                        errorLabel.Text = "Error occured while connecting.";
                    });
                    break;
                default:
                    break;
            }
        }

        private void LoginKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SendClick();
            }
        }

        private void SendClick()
        {
            errorLabel.Text = "";
            string username = usernameInput.Text;
            string password = passwordInput.Password;

            loginButton.IsEnabled = false;
            if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
            {
                loginViewModel.LoginUser(username, password).ContinueWith(_ =>
                {
                    if (UserInformation.Token != null)  // If Http request to server is successful
                    {
                        UserInformation.Username = username;
                        SocketService.ConnectionChange += ConnectionChanged;
                        loginViewModel.ConnectToSocket();
                    }
                    else
                        ConnectionChanged(0);
                });
            }
            else
            {
                loginButton.IsEnabled = true;
                errorLabel.Text = "Fields can't consist of only blank characters.";
            }
        }
    }
}
