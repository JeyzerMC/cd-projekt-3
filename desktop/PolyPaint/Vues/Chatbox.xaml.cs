﻿using Phoenix;
using PolyPaint.Modeles;
using PolyPaint.Services;
using PolyPaint.Services.Channels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PolyPaint.Vues.Clavardage
{
    /// <summary>
    /// Interaction logic for Chatbox.xaml
    /// </summary>
    public partial class Chatbox : UserControl
    {
        private HashSet<string> notifications;
        private ObservableCollection<Modeles.Message> messages;

        // ==================================================================
        // ==================================================================
        //                          INITIALIZATION
        // ==================================================================
        // ==================================================================

        public Chatbox()
        {
            InitializeComponent();
        }

        public void ChatBoxLoaded(object sender, RoutedEventArgs e)
        {
            InitializeDefaultChannels();

            // Subscriptions
            ChatService.updateControl.CollectionChanged += UpdateControl;
            ChatService.players.CollectionChanged += ListPlayersChanged;
            ChatService.CanauxExistants.CollectionChanged += ExistingChannelsChanged;
            ChatService.triggerGameMessage.CollectionChanged += TriggerGameLogCheck;

            // Reset the chat service
            ChatService.Reset();
        }

        public void InitializeDefaultChannels()
        {
            // Init the default data
            notifications = new HashSet<string>();
            messages = new ObservableCollection<Modeles.Message>();

            // Add general chat and welcome message
            ChatService.Discussions.Add(ChatService.currentChat, new Discussion());
            ChatService.Discussions[ChatService.currentChat].Type = "GN";
            ChatService.Discussions[ChatService.currentChat].Messages.CollectionChanged += (s, a) => RegisterNotify("General");
            ChatService.Discussions[ChatService.currentChat].Messages.Add(new Modeles.Message("Bienvenue sur CD Project 3!"));
            ChatService.Discussions[ChatService.currentChat].Messages.Add(new Modeles.Message("Les messages envoyés ou reçus vont apparaitre ici!"));

            // Default chat log
            UpdateChatLog();
            chatlog.ItemsSource = messages;

            InitDefaultChannels();
            //JoinAllChat();

            // Update initial overlay
            UpdateUserList();
            UpdateExistingChannelsPanel();
        }

        public void JoinAllChat()
        {
            ChatService.roomChannel = SocketService.Socket.MakeChannel("chat:general");

            ChatService.roomChannel.On("shout", m => {
                ReceiveGeneralMessage(m);
            });

            ChatService.roomChannel.On("canal_message", m =>
            {
                ReceiveChannelMessage(m);
            });

            ChatService.roomChannel.On("private_message", m =>
            {
                ReceivePrivateMessage(m);
            });

            ChatService.roomChannel.On("connected-users", m =>
            {
                ChatService.UpdateUsersList(m.payload["users"].ToString());
            });

            ChatService.roomChannel.On("existing-channels", m =>
            {
                UpdateCanauxExistants(m);
            });

            ChatService.roomChannel.Join()
              .Receive(Reply.Status.Ok, r => Trace.WriteLine("Joined Room."))
              .Receive(Reply.Status.Error, r => Trace.WriteLine("Error joining room."));
        }

        // ==================================================================
        // ==================================================================
        //                          SOCKET EVENTS
        // ==================================================================
        // ==================================================================

        // GENERAL MESSAGE
        public void ReceiveGeneralMessage(Phoenix.Message m)
        {
            string username = m.payload["username"].ToString();
            string message = m.payload["message"].ToString();

            //Trace.Write("Username: " + username);
            //Trace.WriteLine("Message:" + message);

            ChatService.Discussions["General"].Messages.Add(new Modeles.Message(username, message, Navigator.GetCurrentTime()));
            UpdateChatLog();
        }

        // EXISTING CHANNEL MESSAGE
        public void ReceiveChannelMessage(Phoenix.Message m)
        {
            var sender = m.payload["sender"].ToString();
            var channel = m.payload["channel"].ToString();
            var message = m.payload["message"].ToString();

            if (ChatService.Discussions.ContainsKey(channel) && ChatService.Discussions[channel].Type == "CC")
            {
                ChatService.Discussions[channel].Messages.Add(new Modeles.Message(sender, message, Navigator.GetCurrentTime()));
            }

            UpdateChatLog();
        }

        // PRIVATE MESSAGE
        public void ReceivePrivateMessage(Phoenix.Message m)
        {
            var sender = m.payload["sender"].ToString();
            var receiver = m.payload["receiver"].ToString();
            var message = m.payload["message"].ToString();

            if (receiver == UserInformation.Username)
            {
                if (!ChatService.Discussions.ContainsKey(sender))
                {
                    ChatService.Discussions.Add(sender, new Discussion());
                    ChatService.Discussions[sender].Type = "PM";
                    ChatService.Discussions[sender].Messages.CollectionChanged += (s, a) => RegisterNotify(sender);
                }

                ChatService.Discussions[sender].Messages.Add(new Modeles.Message(sender, message, Navigator.GetCurrentTime()));
            }
            else if (sender == UserInformation.Username)
            {
                if (!ChatService.Discussions.ContainsKey(receiver))
                {
                    ChatService.Discussions.Add(receiver, new Discussion());
                    ChatService.Discussions[receiver].Type = "PM";
                    ChatService.Discussions[receiver].Messages.CollectionChanged += (s, a) => RegisterNotify(receiver);
                }

                ChatService.Discussions[receiver].Messages.Add(new Modeles.Message(sender, message, Navigator.GetCurrentTime()));
            }

            UpdateChatLog();
        }

        public void UpdateCanauxExistants(Phoenix.Message m)
        {
            ChatService.CanauxExistants.Clear();

            foreach (var canal in m.payload)
            {
                //Trace.WriteLine(canal);
                var name = canal.Key;
                var players = canal.Value.ToObject<List<string>>();
                var tp = new Tuple<string, int>(name, players.Count);
                ChatService.CanauxExistants.Add(tp);
            }
        }

        // ==================================================================
        // ==================================================================
        //                  OBSERVABLES & PERIODIC UPDATES
        // ==================================================================
        // ==================================================================

        // Refresh Control
        public void UpdateControl(object sender, NotifyCollectionChangedEventArgs e)
        {
            InitializeDefaultChannels();
            JoinAllChat();
            SwitchChat(ChatService.currentChat);
            inputBox.Focus();
        }

        // CONNECTED USERS 
        public void ListPlayersChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateUserList();
        }

        public void ExistingChannelsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateExistingChannelsPanel();
        }

        // NOTIFICATION CHECK
        public void TriggerGameLogCheck(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (ChatService.Discussions.ContainsKey("Game")) { 
                if (ChatService.currentChat == "Game")
                {
                    UpdateChatLog();
                } else
                {
                    RegisterNotify("Game");
                }
            }
        }

        public void RegisterNotify(string name)
        {
            if (ChatService.currentChat != name)
            {
                notifications.Add(name);
                EffectService.PlaySound("bloop");
            }
        }

        // ==================================================================
        // ==================================================================
        //                           DIRECT USER EVENTS
        // ==================================================================
        // ==================================================================

        // CHANGE CURRENT CHAT CHANNEL
        public void SwitchChat(string name)
        {
            headerChat.Text = name;
            ChatService.currentChat = name;

            if (ChatService.currentChat == "General")
            {
                CloseChatBtn.Visibility = Visibility.Collapsed;
            }
            else
            {
                CloseChatBtn.Visibility = Visibility.Visible;
            }

            if (notifications.Contains(ChatService.currentChat))
            {
                notifications.Remove(ChatService.currentChat);
            }

            UpdateChatLog();

            UpdateChannelList();
            OverlayPanel.Visibility = Visibility.Collapsed;
        }

        // FOCUS CHAT
        private void InputBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (inputBox.Text == "Entrez votre message ici.")
            {
                inputBox.Text = string.Empty;
            }
        }

        // UNFOCUS CHAT
        private void InputBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (inputBox.Text.Length == 0)
            {
                inputBox.Text = "Entrez votre message ici.";
            }
        }

        // SEND MESSAGE VIA ENTER
        private void InputBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Send_Message(sender, e);
            }
        }

        // SEND MESSAGE VIA CLICK
        private void Send_Message(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(inputBox.Text))
            {
                if (ChatService.currentChat == "General")
                {
                    Dictionary<string, object> payload = new Dictionary<string, object>();
                    payload.Add("message", inputBox.Text);
                    ChatService.roomChannel.Push("shout", payload);
                }
                else if (ChatService.currentChat == "Game")
                {
                    GameChannelService.Shout(inputBox.Text);
                }
                else if (ChatService.Discussions[ChatService.currentChat].Type == "CC")
                {
                    SendMessageToChannel(inputBox.Text);
                }
                else
                {
                    SendMessageToRecipient(inputBox.Text);
                }

                inputBox.Text = string.Empty;
            }

            Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(() => inputBox.Focus()));
        }

        // TOGGLE OVERLAY VISIBILITY
        public void ToggleChatPannel(object sender, MouseButtonEventArgs e)
        {
            if (OverlayPanel.Visibility == Visibility.Visible)
            {
                OverlayPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                OverlayPanel.Visibility = Visibility.Visible;
            }

            UpdateChannelList();
        }

        // CLICK ON AJOUTER CHANNEL
        public void AjouterChannel(object sender, RoutedEventArgs e)
        {
            // Hide or show channel
            if (CreateChannelTB.Visibility == Visibility.Collapsed)
            {
                ShowCreateChannel();
            } else
            {
                HideCreateChannel();
                CreateChannel();
            }
        }

        // CREATE CHANNEL FROM KEYBOARD
        private void CreateChannelKD(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (CreateChannelTB.Text.Length > 0)
                {
                    HideCreateChannel();
                }
                CreateChannel();
            }
        }


        public void LeaveCurrentChat(object sender, RoutedEventArgs e)
        {
            var oldChat = ChatService.currentChat;
            SwitchChat("General");

            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("name", oldChat);
            ChatService.roomChannel.Push("leave_channel", payload);
            ChatService.Discussions.Remove(oldChat);
        }

        // ==================================================================
        // ==================================================================
        //                        INDIRECT USER EVENTS
        // ==================================================================
        // ==================================================================

        // CREATE THE CHANNEL ON THE SERVER
        public void CreateChannel()
        {
            if (CreateChannelTB.Text.Length > 0)
            {
                Dictionary<string, object> payload = new Dictionary<string, object>();
                payload.Add("name", CreateChannelTB.Text);
                ChatService.roomChannel.Push("create_channel", payload);

                JoinChannelDiscussion(CreateChannelTB.Text);

                CreateChannelTB.Text = string.Empty;
            }
        }

        public void JoinChannelDiscussion(string name)
        {
            if (!ChatService.Discussions.ContainsKey(name))
            {
                ChatService.Discussions.Add(name, new Discussion());
                ChatService.Discussions[name].Type = "CC";
                ChatService.Discussions[name].Messages.CollectionChanged += (s, a) => RegisterNotify(name);
            }

            SwitchChat(name);
        }

        // SHOW CREATE CHANNEL INPUT
        public void ShowCreateChannel()
        {
            var offset = VisualTreeHelper.GetOffset(AddCanalBtn);
            TranslateTransform trans = new TranslateTransform();

            AddCanalBtn.RenderTransform = trans;
            DoubleAnimation anim = new DoubleAnimation(offset.X - 80, offset.X - 10, TimeSpan.FromMilliseconds(500));
            anim.Completed += (a, b) =>
            {
                CreateChannelTB.Visibility = Visibility.Visible;
            };

            trans.BeginAnimation(TranslateTransform.XProperty, anim);
        }

        // HIDE CREATE CHANNEL INPUT
        public void HideCreateChannel()
        {
            CreateChannelTB.Visibility = Visibility.Collapsed;

            var offset = VisualTreeHelper.GetOffset(AddCanalBtn);
            TranslateTransform trans = new TranslateTransform();

            AddCanalBtn.RenderTransform = trans;
            DoubleAnimation anim = new DoubleAnimation(offset.X, offset.X - 120, TimeSpan.FromMilliseconds(500));

            trans.BeginAnimation(TranslateTransform.XProperty, anim);
        }

        private void SendMessageToChannel(string message)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("channel", ChatService.currentChat);
            payload.Add("message", inputBox.Text);
            ChatService.roomChannel.Push("canal_message", payload);
        }

        // INBOX USER
        private void SendMessageToRecipient(string message)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("sender", UserInformation.Username);
            payload.Add("receiver", ChatService.currentChat);
            payload.Add("message", inputBox.Text);
            ChatService.roomChannel.Push("private_message", payload);
        }

        private void JoinChannel(string name)
        {
            if (!ChatService.Discussions.ContainsKey(name))
            {
                Dictionary<string, object> payload = new Dictionary<string, object>();
                payload.Add("name", name);
                ChatService.roomChannel.Push("join_channel", payload);
            }

            JoinChannelDiscussion(name);
        }

        private void JoinPrivateMessage(string name)
        {
            if (!ChatService.Discussions.ContainsKey(name))
            {
                ChatService.Discussions.Add(name, new Discussion());
                ChatService.Discussions[name].Type = "PM";
                ChatService.Discussions[name].Messages.CollectionChanged += (s, a) => RegisterNotify(name);
            }

            SwitchChat(name);
        }

        // ==================================================================
        // ==================================================================
        //                           UI MANIPULATION
        // ==================================================================
        // ==================================================================

        // INITIALIZE DEFAULT CONNECTED CHANNELS
        public void InitDefaultChannels()
        {
            Dispatcher.Invoke(() =>
            {
                CurrentChannelsPanel.Children.Clear();
                AddPanelToggler();
                AddChannelButton("General");

                if (ChatService.Discussions.ContainsKey("Game"))
                {
                    AddChannelButton("Game");
                }
            });
        }

        // UPDATE THE CHANNEL LIST
        public void UpdateChannelList()
        {
            InitDefaultChannels();

            Dispatcher.Invoke(() =>
            {
                foreach(var channel in ChatService.Discussions)
                {
                    if (channel.Value.Type == "CC") 
                    {
                        AddChannelButton(channel.Key);
                    } else if (channel.Value.Type == "PM")
                    {
                        if (ChatService.players.Contains(channel.Key))
                        {
                            AddChannelButton(channel.Key);
                        }
                        else
                        {
                            AddChannelButton(channel.Key, false);
                        }
                    }
                }
            });
        }

        // UPDATE THE USER LIST
        public void UpdateUserList()
        {
            Dispatcher.Invoke(() =>
            {
                ConnectedUsersPanel.Children.Clear();
                foreach (var player in ChatService.players)
                {
                    AddConnectedUser(player);
                }
            });

            UpdateChannelList();
        }

        public void UpdateExistingChannelsPanel()
        {
            Dispatcher.Invoke(() =>
            {
                ChannelListPannel.Children.Clear();
                foreach (var canal in ChatService.CanauxExistants)
                {
                    AddChannelEntry(canal.Item1, canal.Item2);
                }
            });
        }

        // UPDATE THE CHAT LOG
        public void UpdateChatLog()
        {
            Dispatcher.Invoke(() =>
            {
                messages.Clear();

                foreach (var msg in ChatService.Discussions[ChatService.currentChat].Messages)
                {
                    messages.Add(msg);
                }

                chatlog.SelectedIndex = chatlog.Items.Count - 1;
                chatlog.ScrollIntoView(chatlog.SelectedItem);
            });
        }

        // ==================================================================
        // ==================================================================
        //                           UI ELEMENTS
        // ==================================================================
        // ==================================================================

        // CONNECTED USERS ENTRY
        public void AddConnectedUser(string name)
        {
            // Grid container of the user
            var gd = new Grid();
            var col1 = new ColumnDefinition();
            col1.Width = new GridLength(12, GridUnitType.Star);
            var col2 = new ColumnDefinition();
            col2.Width = new GridLength(4, GridUnitType.Star);
            gd.ColumnDefinitions.Add(col1);
            gd.ColumnDefinitions.Add(col2);

            // Label containing the name of the user
            var lb = new Label();
            lb.Content = name;
            lb.FontSize = 13;
            lb.Height = 30;
            lb.Margin = new Thickness(10, 0, 0, 0);
            lb.SetValue(Grid.ColumnProperty, 0);
            lb.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("LightSkyBlue");
            lb.HorizontalAlignment = HorizontalAlignment.Left;
            lb.VerticalAlignment = VerticalAlignment.Top;

            gd.Children.Add(lb);

            // Button to chat with the user
            var btn = new Button();
            btn.FontSize = 12;
            btn.Width = 45;
            btn.Height = 22;
            btn.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFrom("#2196F3"); 
            btn.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#2196F3");
            btn.HorizontalContentAlignment = HorizontalAlignment.Left;
            btn.HorizontalAlignment = HorizontalAlignment.Center;
            btn.FontWeight = FontWeights.Normal;
            btn.Margin = new Thickness(3, 0, 0, 0);
            btn.SetValue(Grid.ColumnProperty, 1);
            btn.Click += (s, e) => JoinPrivateMessage(name);

            // Text block of the button
            var tb = new TextBlock();
            tb.HorizontalAlignment = HorizontalAlignment.Center;
            tb.Text = "Parler";
            tb.Width = 45;
            tb.Height = 22;
            tb.Margin = new Thickness(-10, -1, -35, -5);

            btn.Content = tb;
            gd.Children.Add(btn);

            // Add to the panel
            ConnectedUsersPanel.Children.Add(gd);
        }

        // EXISTING CHANNEL ENTRY
        public void AddChannelEntry(string name, int participants)
        {
            // Grid container of the channel
            var gd = new Grid();
            var col0 = new ColumnDefinition();
            col0.Width = new GridLength(12, GridUnitType.Star);
            var col1 = new ColumnDefinition();
            col1.Width = new GridLength(4, GridUnitType.Star);
            gd.ColumnDefinitions.Add(col0);
            gd.ColumnDefinitions.Add(col1);

            // Channel name label
            var lb = new Label();
            lb.Content = name;
            lb.FontSize = 13;
            lb.Height = 30;
            lb.MaxWidth = 150;
            lb.Margin = new Thickness(10, 0, 0, 0);
            lb.SetValue(Grid.ColumnProperty, 0);
            lb.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("LightSkyBlue");
            lb.HorizontalAlignment = HorizontalAlignment.Left;
            lb.VerticalAlignment = VerticalAlignment.Top;

            gd.Children.Add(lb);

            // Button to join the channel
            var btn = new Button();
            btn.FontSize = 12;
            btn.Width = 45;
            btn.Height = 22;
            btn.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFrom("#2196F3");
            btn.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#2196F3");
            btn.HorizontalContentAlignment = HorizontalAlignment.Left;
            btn.HorizontalAlignment = HorizontalAlignment.Center;
            btn.FontWeight = FontWeights.Normal;
            btn.Margin = new Thickness(3, 0, 0, 0);
            btn.SetValue(Grid.ColumnProperty, 1);
            btn.Click += (s, e) => JoinChannel(name);

            // Text block of the button
            var tb = new TextBlock();
            tb.HorizontalAlignment = HorizontalAlignment.Center;
            tb.Text = "Entrer";
            tb.Width = 45;
            tb.Height = 22;
            tb.Margin = new Thickness(-13, -1, -35, -5);

            btn.Content = tb;
            gd.Children.Add(btn);

            ChannelListPannel.Children.Add(gd);
        }

        // PANEL TOGGLER BUTTON
        public void AddPanelToggler()
        {
            var border = new Border();
            border.Name = "SelectionChat";
            border.CornerRadius = new CornerRadius(30);
            border.Height = 37;
            border.Width = 37;
            border.Cursor = Cursors.Hand;
            border.Margin = new Thickness(2, 5, 0, 0);
            border.BorderThickness = new Thickness(2);
            border.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("Transparent");
            border.PreviewMouseDown += ToggleChatPannel;
            //border.BorderBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("LightCoral"));

            var tb = new TextBlock();
            tb.Name = "SelectionChatTB";
            if (OverlayPanel.Visibility == Visibility.Visible)
            {
                tb.Text = "-";
            } else
            {
                tb.Text = "+";
            }
            tb.VerticalAlignment = VerticalAlignment.Center;
            tb.HorizontalAlignment = HorizontalAlignment.Center;
            tb.FontSize = 40;
            tb.FontWeight = FontWeights.Bold;
            tb.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("AliceBlue");
            tb.Margin = new Thickness(0, 0, 0, 0);

            tb.ToolTip = "Choisir canal";

            border.Child = tb;
            CurrentChannelsPanel.Children.Add(border);

            var sep = new Separator();
            sep.Width = 30;
            sep.Margin = new Thickness(0, 15, 0, 0);
            sep.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("AliceBlue");

            CurrentChannelsPanel.Children.Add(sep);
        }

        // CHANNEL BUTTON
        public void AddChannelButton(string name, bool connected = true)
        {
            var border = new Border();
            border.CornerRadius = new CornerRadius(30);
            border.Height = 37;
            border.Width = 37;
            border.Cursor = Cursors.Hand;
            border.Margin = new Thickness(2, 5, 0, 0);
            border.BorderThickness = new Thickness(2);
            border.PreviewMouseDown += (s, a) => SwitchChat(name);

            var tb = new TextBlock();
            tb.Text = name.Substring(0, 2).ToUpper();
            tb.VerticalAlignment = VerticalAlignment.Center;
            tb.HorizontalAlignment = HorizontalAlignment.Center;
            tb.FontSize = 12;

            if (connected)
            {
                border.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#BBDEFB");
                tb.ToolTip = name;
            }
            else
            {
                border.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#777777");
                tb.ToolTip = name + " [Disconnected]";
            }

            var gd = new Grid();
            gd.Children.Add(tb);

            if (ChatService.currentChat == name)
            {
                border.CornerRadius = new CornerRadius(10);
                border.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#88EFFC");
            }
            else if (notifications.Contains(name))
            {
                gd.Children.Add(AddNotificationButton());
            }

            border.Child = gd;
            CurrentChannelsPanel.Children.Add(border);
        }

        // NOTIFICATION BUTTON
        public Border AddNotificationButton()
        {
            var bd = new Border();
            bd.CornerRadius = new CornerRadius(30);
            bd.Height = 8;
            bd.Width = 8;
            bd.Margin = new Thickness(0, 1, 0, 0);

            bd.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#FF0000");

            bd.VerticalAlignment = VerticalAlignment.Top;
            bd.HorizontalAlignment = HorizontalAlignment.Right;
            return bd;
        }
    }
}