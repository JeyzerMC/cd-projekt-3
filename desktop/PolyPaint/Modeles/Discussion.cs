﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyPaint.Modeles
{
    public class Discussion
    {
        public string Type { get; set; }

        public ObservableCollection<Message> Messages { get; set; }

        public Discussion()
        {
            Messages = new ObservableCollection<Message>();
        }
    }
}
