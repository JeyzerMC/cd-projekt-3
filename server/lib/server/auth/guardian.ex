defmodule Server.Auth.Guardian do
  use Guardian, otp_app: :server

  alias Server.Accounts
  alias Server.Models.User

  def subject_for_token(%User{} = user, _claims) do
    sub = to_string(user.id)
    {:ok, sub}
  end

  def resource_from_claims(%{"sub" => id}) do
    resource = Accounts.get_user!(id)
    {:ok,  resource}
  end
end
