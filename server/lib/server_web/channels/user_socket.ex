defmodule ServerWeb.UserSocket do
  use Phoenix.Socket

  ## Channels
  channel "chat:*", ServerWeb.ChatChannel
  channel "lobby:*", ServerWeb.LobbyChannel
  channel "ffa_game:*", ServerWeb.FfaGameChannel
  channel "classic_game:*", ServerWeb.ClassicGameChannel
  channel "sprint_solo_game:*", ServerWeb.SprintSoloChannel
  channel "draw:*", ServerWeb.DrawChannel

  def connect(%{"token" => token}, socket) do
    with {:ok, user, _} <- Server.Auth.Guardian.resource_from_token(token) do
      socket =
        assign(socket, :username, user.username)
        |> assign(:user_id, user.id)
        |> assign(:jwt_token, token)

      {:ok, socket}
    end
  end

  def id(socket), do: "users_socket:#{socket.assigns.user_id}"
end
