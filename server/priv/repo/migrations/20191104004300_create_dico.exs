defmodule Server.Repo.Migrations.CreateDico do
  use Ecto.Migration

  def change do
    create table(:dico) do
      add :name, :string
    end
  end
end
