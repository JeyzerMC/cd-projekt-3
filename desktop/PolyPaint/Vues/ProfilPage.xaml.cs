﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PolyPaint.Modeles;
using PolyPaint.Services;
using PolyPaint.VueModeles;
using PolyPaint.Modeles;

namespace PolyPaint.Vues.ProfilView
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class ProfilPage : Page
    {
        Profil profil;
        public ProfilPage()
        {
            InitializeComponent();
            ProfilViewModel profilModel = new ProfilViewModel();
            profil = profilModel.getProfil();
            LLS.ItemsSource = this.profil.contacts;


        }

        private void Chatbox_Loaded(object sender, RoutedEventArgs e)
        {

        }


        private void Back_Click(object sender, RoutedEventArgs e)
        {
            NavigateToPage("Vues/HomePage.xaml");
            //NavigationService nav = NavigationService.GetNavigationService(this);
            //nav.Navigate(new Uri("Vues/HomePage.xaml", UriKind.RelativeOrAbsolute));
        }
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Uri("Vues/Creation Jeu/CreateWord.xaml", UriKind.RelativeOrAbsolute));
        }
        private void NavigateToPage(string URIstring)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Uri(URIstring, UriKind.RelativeOrAbsolute));
        }

       
    }
}
