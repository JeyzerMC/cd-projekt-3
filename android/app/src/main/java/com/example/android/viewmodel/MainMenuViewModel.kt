package com.example.android.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.android.data.ChatRepository

class MainMenuViewModel: ViewModel(){
    val repository = ChatRepository.getInstance()


    suspend fun getMyUser() = repository.getMyUser()
    suspend fun connectToSocket() = repository.connectToSocket()
    suspend fun joinChannel() = repository.joinChannel()

    fun onUserDisconnect() = repository.signOut()
}