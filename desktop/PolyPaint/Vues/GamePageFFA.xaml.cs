﻿using Phoenix;
using PolyPaint.Services;
using PolyPaint.VueModeles;
using PolyPaint.Modeles;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using PolyPaint.Services.Channels;
using Newtonsoft.Json;
using System.Windows.Threading;
using System.Windows.Media.Imaging;

namespace PolyPaint.Vues
{
    enum GameStateFFA { Wait, AboutToStart, Draw, Guess, Watch, Vote, End };

    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class GamePageFFA : Page
    {
        private double widthCanvas;
        private double heightCanvas;
        private string lobbyId;
        private GameStateFFA gameState;
        private string currentWord = "";
        private string currentDrawer = "";
        private List<Player> players;
        private DispatcherTimer timer;
        private TimeSpan timeLeft;
        private bool gameBegan;
        private static int DRAW_TIME = 45; //seconds

        public GamePageFFA()
        {
            InitializeComponent();
            DataContext = new VueModele();
            widthCanvas = this.DrawingCanvas.Width;
            heightCanvas = this.DrawingCanvas.Height;
            gameBegan = false;
            Navigator.NavigationService.LoadCompleted += NavigationService_LoadCompleted;
        }

        void NavigationService_LoadCompleted(object sender, NavigationEventArgs e)
        {
            gameState = GameStateFFA.Wait;
            lobbyId = ((Dictionary<string, object>)e.ExtraData)["id"].ToString();

            if(!(bool)((Dictionary<string, object>)e.ExtraData)["isCreator"]) //if is not lobby owner
            {
                StartButton.IsEnabled = false;
                StartButton.Content = "Attendre que le maître de la partie débute le jeu";
            }
            Navigator.NavigationService.LoadCompleted -= NavigationService_LoadCompleted;
            JoinGameChannel();
            players = LobbyChannelService.team1players;
            RefreshPlayerList();
            CanStartGame();
            LobbyChannelService.lobbyChannel.On("lobbies_update", m => {
                Dictionary<string, Lobby> activeLobbies = JsonConvert.DeserializeObject<Dictionary<string, Lobby>>(m.payload.ToString());
                if (activeLobbies.ContainsKey(lobbyId))
                {
                    Lobby currentLobby = activeLobbies[lobbyId];
                    players = currentLobby.users;
                    RefreshPlayerList();
                    CanStartGame();
                }
            });
        }

        // Pour gérer les points de contrôles.
        private void GlisserCommence(object sender, DragStartedEventArgs e) => (sender as Thumb).Background = Brushes.Black;
        private void GlisserTermine(object sender, DragCompletedEventArgs e) => (sender as Thumb).Background = Brushes.White;
        private void GlisserMouvementRecu(object sender, DragDeltaEventArgs e)
        {
            String nom = (sender as Thumb).Name;
            //    if (nom == "horizontal" || nom == "diagonal") colonne.width = new gridlength(math.max(32, colonne.width.value + e.horizontalchange));
            //    if (nom == "vertical" || nom == "diagonal") ligne.height = new GridLength(Math.Max(32, ligne.Height.Value + e.VerticalChange));
        }

        // Pour la gestion de l'affichage de position du pointeur.
        //private void surfaceDessin_MouseLeave(object sender, MouseEventArgs e) => textBlockPosition.Text = "";
        private void surfaceDessin_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.GetPosition(DrawingCanvas);
            //textBlockPosition.Text = Math.Round(p.X) + ", " + Math.Round(p.Y) + "px";
        }

        private void DupliquerSelection(object sender, RoutedEventArgs e)
        {
            DrawingCanvas.CopySelection();
            DrawingCanvas.Paste();
        }

        private void SupprimerSelection(object sender, RoutedEventArgs e) => DrawingCanvas.CutSelection();

        private void DrawingCanvas_GetInkStrokes(object sender, MouseEventArgs e)
        {
            StrokeCollection strokes = this.DrawingCanvas.Strokes;
            if (strokes.Count == 0) return;

            Stroke lastStroke = strokes[strokes.Count - 1];

            SendStroke(lastStroke);
        }

        private void SendStroke(Stroke lastStroke)
        {
            // convert color to int
            Color color = lastStroke.DrawingAttributes.Color;
            int colorInt = ColorToInt(color);
            string stylusInt = null;
            switch (lastStroke.DrawingAttributes.StylusTip)
            {
                case (StylusTip.Ellipse):
                    stylusInt = "ellipse";
                    break;
                case (StylusTip.Rectangle):
                    stylusInt = "rectangle";
                    break;
            }

            double width = lastStroke.DrawingAttributes.Width;

            StylusPointCollection points = lastStroke.StylusPoints;

            for (int i = 1; i < points.Count - 1; i++)
            {
                Dictionary<String, object> point = new Dictionary<string, object>
                {
                    { "color", colorInt },
                    { "width", width/widthCanvas },
                    { "startX", points[i-1].X/widthCanvas },
                    { "startY", points[i-1].Y/heightCanvas },
                    { "endX", points[i].X/widthCanvas },
                    { "endY",  points[i].Y/heightCanvas },
                    { "style",  stylusInt}
                };

                GameChannelService.gameChannel.Push("draw", point);
            }
        }

        private void JoinGameChannel()
        {
            GameChannelService.JoinGameChannel("ffa_game:" + lobbyId);

            GameChannelService.gameChannel.On("start_game", m =>
            {
                gameState = GameStateFFA.AboutToStart;
                this.Dispatcher.Invoke(() => {
                    Window.GetWindow(this).Opacity = 0.80;
                    WarningLabel.Visibility = Visibility.Visible;
                    this.IsEnabled = false;
                    StartButton.Visibility = Visibility.Hidden;
                    GameTab.Visibility = Visibility.Visible;
                });
            });

            GameChannelService.gameChannel.On("change_turn", m =>
            {
                this.Dispatcher.Invoke(() => {
                    ClearGuessBox();
                    ClearCanvas();
                    GuessContainer.Visibility = Visibility.Hidden;
                    WordContainer.Visibility = Visibility.Hidden;
                    VotingPanel.Visibility = Visibility.Hidden;
                    TimePanel.Visibility = Visibility.Visible;
                    CurrentWordDisplay.Content = "";
                });
                if (!gameBegan)
                {
                    this.Dispatcher.Invoke(() => {
                        Window.GetWindow(this).Opacity = 1;
                        WarningLabel.Visibility = Visibility.Hidden;
                        this.IsEnabled = true;
                        gameBegan = true;
                        LaunchTimer();
                    });
                }
                Trace.WriteLine(m.payload);
                ResetTime();
                currentDrawer = m.payload["drawer"]["username"].ToString();
                currentWord = m.payload["word"].ToString();
                if (currentDrawer == UserInformation.Username)
                {
                    gameState = GameStateFFA.Draw;
                    this.Dispatcher.Invoke(() => {
                        DrawingGrid.IsEnabled = true;
                        WordContainer.Visibility = Visibility.Visible;
                        CurrentWordDisplay.Content = currentWord;
                    });
                }
                else
                {
                    gameState = GameStateFFA.Guess;
                    this.Dispatcher.Invoke(() => {
                        GuessBox.IsEnabled = true;
                        this.DrawingGrid.IsEnabled = false;
                        GuessContainer.Visibility = Visibility.Visible;
                    });
                }
                Trace.WriteLine("GameSate: " + gameState);
            });

            GameChannelService.gameChannel.On("score", m => {
                Trace.WriteLine("Game channel score !");
                players = new List<Player>(JsonConvert.DeserializeObject<Dictionary<string, Player[]>>(m.payload.ToString())["users"]);
                RefreshPlayerList();
            });

            GameChannelService.gameChannel.On("game_error", m =>
            {
                this.Dispatcher.Invoke(() => {
                    ExitGameView(null, null);
                });
            });

            GameChannelService.gameChannel.On("draw", m => {
                //Trace.WriteLine("somebody drawing!");

                StylusPoint startPoint = new StylusPoint((double)m.payload["startX"] * widthCanvas, (double)m.payload["startY"] * heightCanvas);
                StylusPoint endPoint = new StylusPoint((double)m.payload["endX"] * widthCanvas, (double)m.payload["endY"] * heightCanvas);

                StylusPointCollection col = new StylusPointCollection
                {
                    startPoint,
                    endPoint
                };

                Stroke stroke = new Stroke(col);

                stroke.DrawingAttributes.Color = IntToColor((int)m.payload["color"]);
                stroke.DrawingAttributes.Width = (double)m.payload["width"] * widthCanvas;
                stroke.DrawingAttributes.Height = (double)m.payload["width"] * widthCanvas;

                StylusTip style = new StylusTip();
                switch ((string)m.payload["style"])
                {
                    case ("ellipse"):
                        style = StylusTip.Ellipse;
                        break;
                    case ("rectangle"):
                        style = StylusTip.Rectangle;
                        break;
                }
                stroke.DrawingAttributes.StylusTip = style;

                this.Dispatcher.Invoke(() =>
                {
                    this.DrawingCanvas.Strokes.Add(stroke);
                });
            });

            GameChannelService.gameChannel.On("vote", (m =>
            {
                Trace.WriteLine("Vote");
                gameState = GameStateFFA.Vote;
                if (currentDrawer != UserInformation.Username)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        DrawingGrid.IsEnabled = false;
                        GuessContainer.Visibility = Visibility.Hidden;
                        CurrentWordDisplay.Content = currentWord;
                        WordContainer.Visibility = Visibility.Visible;
                        VotingPanel.Visibility = Visibility.Visible;
                    });
                }
            }));

            GameChannelService.gameChannel.On("end_game", m =>
            {
                gameState = GameStateFFA.End;
                Trace.WriteLine("end_game");
                this.Dispatcher.Invoke(() =>
                {
                    List<Player> winners = new List<Player>();
                    int highestScore = 0;
                    foreach(Player player in players)   //find winners
                    {
                        if(player.score == highestScore)
                        {
                            winners.Add(player);
                            highestScore = player.score;
                        } else if(player.score > highestScore)
                        {
                            winners.Clear();
                            winners.Add(player);
                            highestScore = player.score;
                        }
                    }
                    if(winners.Count > 1)   //display winners
                    {
                        WinnerLabel.Content = "Joueurs ";
                        foreach (Player player in winners)
                        {
                            WinnerLabel.Content += player.username + " ";
                        }
                        WinnerLabel.Content += "ont gagné";
                    } else {
                        WinnerLabel.Content = "Joueur " + winners[0].username + " gagne";
                    }
                    List<Player> endgamePlayers = new List<Player>(JsonConvert.DeserializeObject<Dictionary<string, Player[]>>(m.payload.ToString())["users"]);
                    List<object> displayList = new List<object>();
                    foreach (Player player in endgamePlayers)
                    {
                        displayList.Add(new
                        {
                            Username = player.username,
                            Score = player.score,
                        });
                    }
                    FinalScoreDisplay.ItemsSource = displayList;
                    ConfettiEffect.Visibility = Visibility.Visible;
                    FinalScorePanel.Visibility = Visibility.Visible;
                });
                EffectService.PlaySound("applause");
            });
        }

        private Color IntToColor(int colorInt)
        {
            var color = System.Drawing.Color.FromArgb(colorInt);

            return Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        private int ColorToInt(Color color)
        {
            string colorString = color.ToString().Split('#')[1];
            int colorInt = Int32.Parse(colorString, System.Globalization.NumberStyles.HexNumber);

            return colorInt;
        }

        private void Chatbox_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void GuessBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                GameChannelService.gameChannel.Push("guess", new Dictionary<string, object>() { { "word", GuessBox.Text } }).Receive(new Reply.Status(), reply => {
                    Phoenix.Reply message = (Phoenix.Reply)reply;
                    if (message.response["guessed"].ToString() != "True")   //false guess
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            EffectService.animationFrame(HeightProperty);
                        });
                    } else
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            TimePanel.Visibility = Visibility.Hidden;
                            WordContainer.Visibility = Visibility.Visible;
                        });
                    }
                });
                this.Dispatcher.Invoke(() =>
                {
                    ClearGuessBox();
                });
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            GameChannelService.gameChannel.Push("start_game");
        }

        private void ClearCanvas()
        {
            this.DrawingCanvas.Strokes.Clear();
        }

        private void ClearGuessBox()
        {
            this.GuessBox.Text = "";
            this.GuessBox.Background = SystemColors.WindowBrush;
        }

        private void ExitGameView(object sender, RoutedEventArgs e)
        {
            LobbyChannelService.ResetLobbyChannel();
            GameChannelService.LeaveGame();
            Navigator.Navigate("Vues/HomePage.xaml");
        }

        private void Click_Upvote(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("rating", 1);
            GameChannelService.gameChannel.Push("vote", payload);
            VotingPanel.Visibility = Visibility.Hidden;
        }

        private void Save_Image(object sender, RoutedEventArgs e)
        {
            Rect rect = new Rect(DrawingCanvas.RenderSize);
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)rect.Right,
              (int)rect.Bottom, 96d, 96d, PixelFormats.Default);
            rtb.Render(DrawingCanvas);
            //endcode as PNG
            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

            //save to memory stream
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            pngEncoder.Save(ms);
            ms.Close();
            System.IO.File.WriteAllBytes($"../{currentWord}-by-{currentDrawer}.png", ms.ToArray());
        }

        private void Click_Downvote(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("rating", -1);
            GameChannelService.gameChannel.Push("vote", payload);
            VotingPanel.Visibility = Visibility.Hidden;
        }

        private void RefreshPlayerList()
        {
            List<object> displayList = new List<object>();
            int columnIndex = 0;
            foreach (Player player in players)
            {
                ImageSource avatarSource = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Avatars/Avatar{player.avatar}.png"));
                avatarSource.Freeze();
                ImageSource badgeSource = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Badges/badge{player.title}.png"));
                badgeSource.Freeze();
                displayList.Add(new
                {
                    Avatar = avatarSource,
                    Username = player.username,
                    Score = player.score,
                    Badge = badgeSource,
                    Title = Titles.title[player.title],
                    Y = columnIndex,
                });
                columnIndex++;
            }
            this.Dispatcher.Invoke(() =>
            {
                PlayerList.ItemsSource = displayList;
            });
        }

        private bool CanStartGame()
        {
            if (!gameBegan && players.Count >= 2)
            {
                this.Dispatcher.Invoke(() =>
                {
                    StartButton.IsEnabled = true;
                });
                return true;
            }
            else
            {
                this.Dispatcher.Invoke(() =>
                {
                    StartButton.IsEnabled = false;
                });
                return false;
            }
        }

        private void LaunchTimer()
        {
            timeLeft = TimeSpan.FromSeconds(DRAW_TIME);
            timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                TimeLabel.Content = timeLeft.ToString("c");
                if (timeLeft == TimeSpan.Zero)
                {
                    timer.Stop();
                    DrawingGrid.IsEnabled = false;
                }
                timeLeft = timeLeft.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);
        }

        private void ResetTime()
        {
            timeLeft = TimeSpan.FromSeconds(DRAW_TIME);
        }
    }
}