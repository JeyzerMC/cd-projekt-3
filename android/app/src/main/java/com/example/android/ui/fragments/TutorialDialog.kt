package com.example.android.ui.fragments

import android.media.Image
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.android.R
import com.example.android.data.ChatRepository
import com.example.android.data.GameMode
import com.example.android.databinding.FragmentTutorialDialogBinding
import com.example.android.viewmodel.Profile.Companion.titles
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ViewListener
import org.w3c.dom.Text

class TutorialDialog : DialogFragment() {
    val repository = ChatRepository.getInstance()
    val gameMode = repository.gameMode
    lateinit var carouselView: CarouselView

    var titlesClassique = arrayOf("Mode de jeu: CLASSIQUE", "DÉBUT", "Déroulement: VOTRE TOUR","Déroulement: VOTRE TOUR", "Déroulement: LEUR TOUR", "FIN")
    var imagesClassique = arrayOf(R.drawable.classic_1, R.drawable.classic_2, R.drawable.classic_6, R.drawable.classic_3, R.drawable.classic_5, R.drawable.classic_4)
    var infosClassique = arrayOf(
        "En équipe de 2, un des membres dessine et l'autre devine.\nDevinez plus de dessins que l'équipe adversaire pour gagner!\n\n" +
        "Avant le début de la partie, les joueurs sont répartis en deux équipes: ORANGE et BLEUE.\n\n" +
            "Si vous n'êtes pas 4 joueurs, il est possible d'ajouter 1 JOUEUR VIRTUEL par équipe.\n\n" +
            "Lorsque vous êtes prêts, un joueur peut choisir de COMMENCER LA PARTIE.\n",

        "Après avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\n\n" +
            "Lorsque celle-ci disparaît, la partie commence immédiatement.\n",

        "Si vous voyez un mot en gras en bas de l'écran, vous avez le rôle de DESSINEUR.\n\n" +
        "Avec les outils de dessin, essayez de dessiner le mot en gras.\n\n",

        "Si vous voyez un champ pour deviner en bas de l'écran, vous avez le rôle de DEVINEUR.\n\n\n" +
        "Vous avez une tentative pour deviner le mot que votre coéquiper dessine et gagner un point.\n\n" +
        "Si vous donnez une mauvaise réponse, un droit de réplique est donné à l'équipe adversaire.\n\n\n" +
        "Ensuite, c'est le tour de l'équipe adversaire.\n\n",

        "Si vous voyez « Attendez votre tour...», c'est le tour de l'équipe adversaire.\n\n" +
            "Votre tour arrivera après que l'équipe adversaire gagne un point ou vous donne un droit de réplique.\n",

        "Une manche est finie après que chaque équipe joue leur tour une fois.\n\n" +
            "La partie se termine apès deux manches.\n\n" +

            "L'équipe gagnante est celle qui a accumulé le plus de points!\n"
    )

    var titlesSprintSolo = arrayOf("Mode de jeu: SPRINT SOLO", "DÉROULEMENT", "FIN")
    var imagesSprintSolo = arrayOf(R.drawable.solo_1, R.drawable.solo_2, R.drawable.solo_3)
    var infosSprintSolo = arrayOf(
        "Vous jouez solo contre la montre! Devinez le plus de dessins possibles dans le laps de temps donné.\n\n" +
            "Lorsque vous êtes prêt, vous pouvez choisir de COMMENCER LA PARTIE.\n\n" +
            "Après avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\n" +
            "Lorsque celle-ci disparaît, la partie commence immédiatement.",


        "Devinez le mot qui est en train de se faire dessiner en entrant votre réponse dans le champ en bas de l'écran.\n\n" +
            "Le nombre de tentatives varie selon la difficulté du dessin.\n\n" +
            "Lorsque vous donnez la bonne réponse, vous passez au prochain dessin.",

        "La partie se termine lorsque le temps est écoulé."
    )

    var titlesFfa = arrayOf("Mode de jeu: MÊLÉE GÉNÉRALE", "Déroulement: DESSINATEUR", "Déroulement: DEVINEUR", "Déroulement: VOTE ET SAUVEGARDE", "FIN")
    var imagesFfa = arrayOf(R.drawable.ffa_1, R.drawable.ffa_2, R.drawable.ffa_3, R.drawable.ffa_4, R.drawable.ffa_5)
    var infosFfa = arrayOf(
        "Chacun joue à son compte. Il peut y avoir jusqu'à 4 joueurs.\n" +
            "Devinez le plus de dessins pour gagner!\n\n\n" +
            "Début:\n\n" +
            "Lorsque vous êtes prêts, un joueur peut choisir de COMMENCER LA PARTIE.\n\n" +
            "Après avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\n" +
            "Lorsque celle-ci disparaît, la partie commence immédiatement.",

        "Si vous voyez un mot en gras en bas de l'écran, vous avez le rôle de DESSINEUR.\n" +
            "Avec les outils de dessin, essayez de dessiner le mot en gras.",

        "Si vous voyez un champ pour deviner en bas de l'écran, vous avez le rôle de DEVINEUR.\n\n" +
            "Essayez de deviner ce que le DESSINATEUR dessine.\n\n" +
            "Vous pouvez réessayer jusqu'à l'obtention de la bonne réponse OU jusqu'à l'écoulement du temps alloué.",

        "Lorsque le temps est écoulé, le mot à deviner s'affiche. Il possible de voter pour le dessin et de le sauvegarder.\n\n" +
            "Ensuite, c'est le tour d'un autre joueur d'être le DESSINATEUR.",
        "Une manche est finie après que chaque joueur a été une fois le DESSINATEUR.\n" +
            "La partie se termine apès deux manches.\n\n" +
            "Le gagnant est celui qui a amassé le plus de points!\n"
    )
    var titlesDefault = arrayOf("Poly Paint Pro") + titlesClassique + titlesSprintSolo + titlesFfa
    var imagesDefault = arrayOf(R.drawable.polypaint_menu) + imagesClassique + imagesSprintSolo + imagesFfa
    var infosDefault = arrayOf(
        "Poly Paint Pro est un jeu de style 'Fais moi un dessin'\n\n" +
            "Le but général est de deviner un mot à partir d'un dessin.\n\n" +
            "Il y a 3 modes de jeu:\n" +
            "1. Classique\n" +
            "2. Sprint Solo\n" +
            "3. Mêlée générale\n\n\n") + infosClassique + infosSprintSolo + infosFfa

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentTutorialDialogBinding.inflate(layoutInflater,  container, false)
        val view = binding.root

        binding.lifecycleOwner = this
        binding.gameMode = gameMode
        carouselView = CarouselView(context)
        carouselView = view.findViewById(R.id.carouselView)
        carouselView.pageCount = when(gameMode.value) {
            GameMode.CLASSIC -> titlesClassique.size
            GameMode.SOLOSPRINT -> titlesSprintSolo.size
            GameMode.FFA -> titlesFfa.size
            else -> titlesDefault.size
        }
        carouselView.setViewListener(viewListener)
        return view

    }

    var viewListener: ViewListener = ViewListener { position ->
        val customView = layoutInflater.inflate(R.layout.tutorial_view, null)

        val title = customView.findViewById<TextView>(R.id.tutorialTitle)
        val info = customView.findViewById<TextView>(R.id.tutorialInfo)
        val image = customView.findViewById<ImageView>(R.id.tutorialImage)

        var images: Array<Int>
        var titles: Array<String>
        var infos: Array<String>

        when(gameMode.value) {
            GameMode.CLASSIC -> {
                images = imagesClassique
                titles = titlesClassique
                infos = infosClassique
            }
            GameMode.SOLOSPRINT -> {
                images = imagesSprintSolo
                titles = titlesSprintSolo
                infos = infosSprintSolo
            }
            GameMode.FFA -> {
                images = imagesFfa
                titles = titlesFfa
                infos = infosFfa
            }
            else -> {
                images = imagesDefault
                titles = titlesDefault
                infos = infosDefault
            }
        }

        image.setImageResource(images[position])
        title.text = titles[position]
        info.text = infos[position]

        carouselView.indicatorGravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP

        customView
    }
}
