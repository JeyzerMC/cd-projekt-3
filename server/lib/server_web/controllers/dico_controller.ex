defmodule ServerWeb.DicoController do
  use ServerWeb, :controller

  alias Server.Repo
  alias Server.Models.Dico

  def fetch_words(conn, _) do
    words = Repo.all(Dico)
    res = Jason.encode!(words)
    IO.puts "THIS IS THE FETCH WORDS"
    IO.puts res
    conn |> json(%{body: res})
  end

  def add_word(conn, entry) do
    with {:ok, %Dico{}} <- add_word(entry) do
      conn |> json(%{body: "Entry added"})
    else
      err ->
        conn |> json(%{body: err})
    end
  end

  defp add_word(word) do
    %Dico{}
    |> Dico.changeset(word)
    |> Repo.insert()
  end
end
