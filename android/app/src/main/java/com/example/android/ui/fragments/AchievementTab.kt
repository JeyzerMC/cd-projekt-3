package com.example.android.ui.fragments

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.android.R
import com.example.android.databinding.FragmentAchievementTabBinding
import com.example.android.viewmodel.Profile
import com.example.android.viewmodel.ProfileViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch



class AchievementTab : Fragment() {
    val model: ProfileViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentAchievementTabBinding.inflate(layoutInflater, container, false)

        binding.model = model
        binding.lifecycleOwner = this

        model.profile.observe(this, Observer<Profile> {profile ->
            updateAchievementsList(profile.achievements, profile.selected_title)
        })

        return binding.root
    }

    private fun updateAchievementsList(achivementsList: List<Int>, selectedAchievement: Int) {
        val titlesList = activity!!.findViewById<RadioGroup>(R.id.titlesList)
        titlesList.removeAllViews()

        Profile.titles.mapIndexed { index, title ->
            val radioButton = RadioButton(context)
            radioButton.id = index
            radioButton.setTextColor(ResourcesCompat.getColor(resources, R.color.bluePrimary, null))
            radioButton.textSize *= 1.15f
            radioButton.text = title.name


            val bmp = BitmapFactory.decodeResource(resources, resources.getIdentifier("badge${index}", "drawable", activity!!.packageName))
            val bmpScaled = Bitmap.createScaledBitmap(bmp, 20, 20, false)
            val badge =  BitmapDrawable(resources, bmpScaled)

            radioButton.setCompoundDrawablesWithIntrinsicBounds(badge, null, null, null)

            val titleDescriptionText = TextView(context)
            titleDescriptionText.text = title.description
            titleDescriptionText.setTextColor(Color.WHITE)

            if (index == selectedAchievement) {
                radioButton.isChecked = true
            }

            // TODO: enable only the unlocked achievements

            radioButton.setOnClickListener(this::onTitleRadioButtonSelect)

            titlesList.addView(radioButton)
            titlesList.addView(titleDescriptionText)
        }
    }


    fun onTitleRadioButtonSelect(view: View) {
        val titlesListRadioGroup = activity!!.findViewById<RadioGroup>(R.id.titlesList)

        // titlesListRadioGroup.isEnabled = false

        // val n = titlesListRadioGroup.childCount - 1

        // for (i in 0..n) {
        //     titlesListRadioGroup.getChildAt(i).isEnabled = false
        // }

        GlobalScope.launch {
            val (profile, error ) = model.updateTitle(titlesListRadioGroup.checkedRadioButtonId)
            // titlesListRadioGroup.isEnabled = true
            // for (i in 0..n) {
            //     titlesListRadioGroup.getChildAt(i).isEnabled = true
            // }

            if (profile != null) {
                toast("titre mis a jour")
                model.profile.postValue(profile)
            }

            if (error != null) {
                toast("erreur durant la mise a jour du titre")
            }
        }
    }

    private fun toast(message: String) {
        activity!!.runOnUiThread {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(activity!!, message, duration)
            toast.show()
        }
    }
}
