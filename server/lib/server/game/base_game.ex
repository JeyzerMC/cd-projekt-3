defmodule Server.Game.BaseGame do
  alias Server.Repo
  alias Server.Accounts

  alias Server.Models.User, as: UserModel

  @random_words ["apple", "patato", "anime", "test", "pear", "poly", "something"]

  def endgame(game) do
    game_end_time = :os.system_time(:seconds)

    users =
      if game.type == "classic" do
        game.team1 ++ game.team2
      else
        game.users
      end

    users
    |> Enum.filter(fn user ->
      user.type != "AI"
    end)
    |> Enum.each(fn user ->
      current_user = Repo.get_by(UserModel, username: user.username)

      case game.type do
        "sprint_solo" ->
          IO.puts("UPDATING DB FOR SPRINT SOLO")

          Accounts.update_user(current_user, %{
            nb_games: current_user.nb_games + 1,
            hs_solo: max(user.score, current_user.hs_solo),
            total_hours: current_user.total_hours + (game_end_time - game.game_start_time - 3)
          })

        "ffa" ->
          Accounts.update_user(current_user, %{
            nb_games: current_user.nb_games + 1,
            hs_ffa: max(user.score, current_user.hs_ffa),
            total_hours: current_user.total_hours + (game_end_time - game.game_start_time - 3)
          })

        "classic" ->
          IO.puts("UPDATING DB FOR CLASSIC for #{user.username}")

          Accounts.update_user(current_user, %{
            nb_games: current_user.nb_games + 1,
            hs_classic: max(user.score, current_user.hs_classic),
            total_hours: current_user.total_hours + (game_end_time - game.game_start_time - 3)
          })
      end
    end)

    %{game | started: false, end_game: true}
  end

  def game_contains_user(game, username) do
    Enum.any?(game.users, fn user ->
      user.username == username
    end)
  end

  def get_random_word() do
    # IO.puts("reading words from dico")
    pt = Path.join(:code.priv_dir(:server), "data/game_dico.txt")
    {:ok, words} = File.read(pt)

    String.split(words, "\n")
    |> Enum.random()

    # Enum.random(@random_words)
  end

  def get_random_name() do
    pt = Path.join(:code.priv_dir(:server), "data/names.txt")
    {:ok, words} = File.read(pt)

    String.split(words, "\n")
    |> Enum.random()
  end
end
