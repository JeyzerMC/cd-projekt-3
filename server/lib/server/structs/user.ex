defmodule Server.Structs.User do
  @derive {Jason.Encoder, except: [:__meta__]}
  defstruct username: "",
            avatar: 42,
            title: 42,
            score: 0,
            number_guesses: 0,
            wrong_guesses: 0,
            correct_replies: 0,
            personality: "",
            consecutive_guesses: 0,
            biggest_consecutive_guesses: 0,
            avg_guess_time: 0,
            type: "human"
end
