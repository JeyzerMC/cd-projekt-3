# Server

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Heroku deployment

TEST
For first time: heroku git:remote -a kai26-test -r kai26-test
Push to Heroku with: git subtree push --prefix server kai26-test master
DB: heroku pg:psql postgresql-cylindrical-00739 --app kai26-test

Pour push une branche autre que master sur kai26-test:
git push kai26-test `git subtree split --prefix server [BRANCH]`:master

PRODUCTION
For first time: heroku git:remote -a kai26-test -r kai26
Push to Heroku with: git subtree push --prefix server kai26 master
heroku pg:psql postgresql-closed-23286 --app kai26

Force push with: git push heroku git subtree split --prefix server name-of-your-branch:master --force

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
