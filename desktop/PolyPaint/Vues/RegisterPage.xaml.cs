﻿using PolyPaint.Modeles;
using PolyPaint.Services;
using PolyPaint.VueModeles;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Interaction logic for RegisterPage.xaml
    /// </summary>
    public partial class RegisterPage : Page
    {
        RegisterViewModel registerViewModel;

        public RegisterPage()
        {
            InitializeComponent();
            usernameInput.Focus();
            registerViewModel = new RegisterViewModel();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Vues/LoginPage.xaml");
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            registerbutton.IsEnabled = false;
            if(VerifyInputs())
            {
                string user = usernameInput.Text;
                registerViewModel.RegisterUser(firstNameInput.Text, lastNameInput.Text, user, passwordInput.Password, confirmInput.Password).ContinueWith(_ =>
                {
                    if (UserInformation.Token != null)  // If Http request to server is successful
                    {
                        UserInformation.Username = user;
                        SocketService.ConnectionChange += ConnectionChanged;
                        registerViewModel.ConnectToSocket();
                    }
                    else
                        ConnectionChanged(0);

                });
            }
        }

        private bool VerifyInputs()
        {
            bool fieldsCorrect = true;

            //Clear socket error message, if present
            ClearErrorMessages(navigationControls);

            //Verify that no input is empty
            EmptyInputVerification(firstNameInput, firstNameInput.Text);
            EmptyInputVerification(lastNameInput, lastNameInput.Text);
            EmptyInputVerification(usernameInput, usernameInput.Text);
            EmptyInputVerification(passwordInput, passwordInput.Password);
            EmptyInputVerification(confirmInput, confirmInput.Password);

            //Verify if passwords match
            if (passwordInput.Password != confirmInput.Password)
            {
                passwordInput.Background = Brushes.LightCoral;
                confirmInput.Background = Brushes.LightCoral;
                DisplayErrorMessage((StackPanel)VisualTreeHelper.GetParent(passwordInput), "Les mots de passe doivent correspondre");
                DisplayErrorMessage((StackPanel)VisualTreeHelper.GetParent(confirmInput), "Les mots de passe doivent correspondre");
                fieldsCorrect = false;
            }
            if(!fieldsCorrect)
                registerbutton.IsEnabled = true;
            return fieldsCorrect;

            void EmptyInputVerification(Control field, string fieldText)
            {
                //Clear error messages if there are any
                field.Background = Brushes.LightBlue;
                StackPanel parentContainer = (StackPanel)VisualTreeHelper.GetParent(field);
                ClearErrorMessages(parentContainer);

                if (string.IsNullOrWhiteSpace(fieldText))
                {
                    field.Background = Brushes.LightCoral;
                    DisplayErrorMessage(parentContainer, "Aucun champ de saisie doit être vide");
                    fieldsCorrect = false;
                }
            }
        }
        private void ClearErrorMessages(StackPanel parentContainer)
        {
            while (parentContainer.Children.Count > 2)
                parentContainer.Children.RemoveAt(parentContainer.Children.Count - 1);
        }

        private void DisplayErrorMessage(StackPanel parentContainer, string errorMessage)
        {
            Border border = new Border
            {
                CornerRadius = new CornerRadius(5),
                BorderThickness = new Thickness(3),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                BorderBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d92525")),
                Margin = new Thickness(20, 5, 0, 5)
            };
            TextBlock messageBlock = new TextBlock
            {
                Text = errorMessage,
                TextWrapping = TextWrapping.Wrap,
                Width = Double.NaN,
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d92525")),
                Foreground = Brushes.White,
                FontSize = 12,
                Padding = new Thickness(5)
            };
            border.Child = messageBlock;
            parentContainer.Children.Add(border);
        }

        private void SubmitIfEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                registerbutton.IsEnabled = false;
                if (VerifyInputs())
                {
                    string user = usernameInput.Text;
                    registerViewModel.RegisterUser(firstNameInput.Text, lastNameInput.Text, user, passwordInput.Password, confirmInput.Password).ContinueWith(_ =>
                    {
                        if (UserInformation.Token != null)  // If Http request to server is successful
                        {
                            UserInformation.Username = user;
                            SocketService.ConnectionChange += ConnectionChanged;
                            registerViewModel.ConnectToSocket();
                        }
                        else
                            ConnectionChanged(0);

                    });
                }
            }
        }

        private void ConnectionChanged(int connectionStatus)
        {
            switch (connectionStatus)
            {
                case 0:
                    Dispatcher.Invoke(() =>
                    {
                        DisplayErrorMessage(navigationControls, "Erreur lors de l'enregistrement.");
                        registerbutton.IsEnabled = true;
                    });
                    break;
                case 1:
                    Dispatcher.Invoke(() =>
                    {
                        SocketService.ConnectionChange -= ConnectionChanged;
                        Navigator.ShowTutorial();
                        Navigator.Navigate("Vues/HomePage.xaml");
                        Navigator.OnLogin();
                    });
                    break;
                case 2:
                    Dispatcher.Invoke(() =>
                    {
                        registerbutton.IsEnabled = true;
                        DisplayErrorMessage(navigationControls, "Erreur lors de la connection au serveur");
                    });
                    break;
                default:
                    break;
            }
        }
    }
}
