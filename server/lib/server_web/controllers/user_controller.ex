defmodule ServerWeb.UserController do
  use ServerWeb, :controller

  alias Server.Accounts
  alias Server.Models.User
  alias Server.Auth.Guardian

  alias ServerWeb.Presence

  action_fallback ServerWeb.FallbackController

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.json", users: users)
  end

  def sign_in(conn, %{"username" => username, "password" => password}) do
    with {:ok, _} <- Presence.verify_connected(username),
         {:ok, user} <- Accounts.authenticate_user(username, password),
         {:ok, token, _claims} <- Guardian.encode_and_sign(user)
    do
      conn |> render("jwt.json", jwt: token)
    end
  end

  def sign_up(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params),
         {:ok, token, _claims} <- Guardian.encode_and_sign(user)
    do
      conn |> render("jwt.json", jwt: token)
      # TODO: FIX ERROR WHEN IT'S NOT OK
    end
  end

  def sign_out(conn, _) do
    case Guardian.Plug.current_token(conn) do
      nil ->
        {:error, :no_token}
      jwt ->
        # SIGN OUT DOESN'T DO ANYTHING: MUST REVOKE MANUALLY
        Guardian.revoke(jwt)
        conn |> json(%{body: "Revoked token"})
    end
  end

  def get_profile(conn, _) do
    IO.puts "USERNAME TOKEN"
    user = Guardian.Plug.current_resource(conn)
    IO.puts user.username

    get_public_profile(conn, %{"username" => user.username})
  end

  def update_profile(conn, params) do
    res = Guardian.Plug.current_resource(conn)
    with {:ok, %User{} = user} <- Accounts.get_by_username(res.username) do
        {:ok, new_user} = Accounts.update_user(user, params)
        respz = Jason.encode!(%{new_user | __meta__: nil})
      conn |> json(%{body: respz})
    else
      err ->
        IO.puts "COULDN'T GET BY USERNAME"
      conn |> json(%{err: err})
    end
  end

  def get_public_profile(conn, %{"username" => username}) do
    IO.puts "USERNAME IS"
    IO.puts username

    with {:ok, %User{} = user} <- Accounts.get_by_username(username) do
      res = Jason.encode!(%{user | __meta__: nil})
      conn |> json(%{body: res})
    else
      _ ->
      conn |> json(%{err: "Couldn't get the user"})
    end
  end
end
