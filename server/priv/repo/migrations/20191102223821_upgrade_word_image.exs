defmodule Server.Repo.Migrations.UpgradeWordImage do
  use Ecto.Migration

  def change do
    alter table(:words) do
      modify :image, :text
    end
  end
end
