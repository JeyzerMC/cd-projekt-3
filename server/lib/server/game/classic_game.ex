defmodule Server.Game.ClassicGame do
  alias Server.Structs.User
  alias Server.Repo
  alias Server.Models.Word
  alias Server.Game.BaseGame

  @max_round_counter 2
  @max_round 2
  @random_words ["apple", "patato", "anime", "test", "pear", "poly", "something"]
  @max_players 4
  @personality ["angry", "cocky", "friendly"]

  def validate_guess(game, word, username) do
    cond do
      game.current_drawer.username == "-1" && game.word == word ->
        game =
          game
          |> add_score(username)
          |> increment_correct_replies(username)
          |> change_drawer()
          |> change_word()

        if game.end_game_classic_hack do
          %{game | end_game: true}
        else
          game
        end

      game.current_drawer.username == "-1" && game.word != word ->
        game =
          game
          |> increment_wrong_guesses(username)
          |> change_drawer()
          |> change_word()

        if game.end_game_classic_hack do
          %{game | end_game: true}
        else
          game
        end

      game.current_drawer.username != "-1" && game.word == word ->
        game
        |> add_score(username)
        |> increment_round(true)
        |> change_team()
        |> change_drawer()
        |> change_word()

      game.current_drawer.username != "-1" && game.word != word ->
        game
        |> increment_wrong_guesses(username)
        |> increment_round(false)
        |> change_team()
        |> change_drawer(true)
    end
  end

  def change_word(game) do
    new_word =
      if game.current_drawer.type == "AI" do
        word =
          Repo.all(Word)
          |> Enum.random()

        word.name
      else
        BaseGame.get_random_word()
      end

    if new_word == game.word do
      change_word(game)
    else
      Map.put(game, :word, new_word)
    end
  end

  def add_score(game, username) do
    team_key = String.to_atom(game.current_team)
    team_members = Map.get(game, team_key)

    new_team_members =
      Enum.map(team_members, fn user ->
        if user.username == username do
          %User{user | score: user.score + 1}
        else
          user
        end
      end)

    Map.put(game, team_key, new_team_members)
  end

  def increment_round(game, good_guess) do
    new_game = increment_round_counter(game)

    if new_game.round_counter == 0 do
      if new_game.round + 1 == @max_round do
        if good_guess do
          %{new_game | round: new_game.round + 1, end_game: true}
        else
          %{new_game | round: new_game.round + 1, end_game_classic_hack: true}
        end
      else
        %{new_game | round: new_game.round + 1}
      end
    else
      new_game
    end
  end

  def increment_round_counter(game) do
    round_counter =
      if game.round_counter + 1 == @max_round_counter do
        0
      else
        game.round_counter + 1
      end

    %{game | round_counter: round_counter}
  end

  def increment_wrong_guesses(game, username) do
    team_key = String.to_atom(game.current_team)
    team_members = Map.get(game, team_key)

    new_team_members =
      Enum.map(team_members, fn user ->
        if user.username == username do
          %User{user | wrong_guesses: user.wrong_guesses + 1}
        else
          user
        end
      end)

    Map.put(game, team_key, new_team_members)
  end

  def increment_correct_replies(game, username) do
    team_key = String.to_atom(game.current_team)
    team_members = Map.get(game, team_key)

    new_team_members =
      Enum.map(team_members, fn user ->
        if user.username == username do
          %User{user | correct_replies: user.correct_replies + 1}
        else
          user
        end
      end)

    Map.put(game, team_key, new_team_members)
  end

  def change_team(game) do
    new_team =
      if game.current_team == "team1" do
        "team2"
      else
        "team1"
      end

    %{game | current_team: new_team}
  end

  def change_drawer(game, no_drawer \\ false) do
    if no_drawer do
      %{game | current_drawer: %User{username: "-1"}}
    else
      team_key = String.to_atom(game.current_team)
      team_members = Map.get(game, team_key)
      contains_AI = Enum.any?(team_members, fn user -> user.type == "AI" end)

      if contains_AI do
        ai_members =
          Enum.filter(team_members, fn user ->
            user.type == "AI"
          end)

        %{game | current_drawer: Enum.random(ai_members)}
      else
        %{game | current_drawer: Enum.random(team_members)}
      end
    end
  end

  def add_ai(game) do
    is_open =
      if game.nb_players + 1 >= @max_players do
        false
      else
        true
      end

    cond do
      Enum.count(game.team1) <= 1 ->
        %{
          game
          | team1:
              game.team1 ++
                [
                  %User{
                    username: "Ai " <> BaseGame.get_random_name(),
                    type: "AI",
                    personality: Enum.random(@personality)
                  }
                ],
            nb_players: game.nb_players + 1,
            open: is_open
        }

      Enum.count(game.team2) <= 1 ->
        %{
          game
          | team2:
              game.team2 ++
                [
                  %User{
                    username: "Ai " <> BaseGame.get_random_name(),
                    type: "AI",
                    personality: Enum.random(@personality)
                  }
                ],
            nb_players: game.nb_players + 1,
            open: is_open
        }

      true ->
        {:error, %{reason: "lobby already full"}}
    end
  end

  def start_game(game) do
    new_game = %{
      game
      | current_team: "team1",
        started: true,
        game_start_time: :os.system_time(:seconds)
    }

    new_game
    |> change_drawer()
    |> change_word()
  end
end
