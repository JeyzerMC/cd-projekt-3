defmodule Server.Game.Ai do
  alias Server.Repo
  alias Server.Models.Word

  @angry_ai_start ["Dépêchez vous à jouer merde"]
  @cocky_ai_start ["Ça va être une victoire facile"]
  @friendly_ai_start ["Bonne chance à tous"]

  @angry_ai_change_turn ["*** ****** *****", "********", "**** ***", "**** de *******", "*****"]
  @cocky_ai_change_turn [
    "Vous allez perdre",
    "Hahhahahaha",
    "La victoire est à nous",
    "Vous ne méritez pas la victoire",
    "Vous ne pouvez pas nous battre"
  ]
  @friendly_ai_change_turn [
    ":)",
    "C'est amusant jouer avec vous",
    "Cette ronde va être amusante",
    ":D"
  ]

  def get_hint(game) do
    current_word = Repo.get_by(Word, name: game.word)
    tip = Enum.random(current_word.tips)
    IO.puts("the hint is #{tip}")
    "Voici un indice: #{tip}"
  end

  def get_start_message(game, topic, gametype) do
    get_users(game, gametype)
    |> Enum.each(fn user ->
      if user.type == "AI" do
        message =
          case user.personality do
            "angry" ->
              Enum.random(@angry_ai_start)

            "cocky" ->
              Enum.random(@cocky_ai_start)

            "friendly" ->
              Enum.random(@friendly_ai_start)

            _ ->
              IO.puts("no personality")
          end

        ServerWeb.Endpoint.broadcast!(topic, "shout", %{
          username: user.username,
          message: message
        })
      end
    end)
  end

  def get_change_turn_message(game, topic, gametype) do
    # IO.puts("ai change turn message")

    get_users(game, gametype)
    |> Enum.each(fn user ->
      if user.type == "AI" do
        message =
          case user.personality do
            "angry" ->
              Enum.random(@angry_ai_change_turn)

            "cocky" ->
              Enum.random(@cocky_ai_change_turn)

            "friendly" ->
              Enum.random(@friendly_ai_change_turn)

            _ ->
              IO.puts("no personality")
          end

        # IO.puts("message")
        # IO.puts(topic)

        ServerWeb.Endpoint.broadcast!(topic, "shout", %{
          username: user.username,
          message: message
        })
      end
    end)
  end

  def get_users(game, gametype) do
    case gametype do
      "sprint_solo_game" ->
        [game.current_drawer]

      "classic_game" ->
        game.team1 ++ game.team2
    end
  end
end
