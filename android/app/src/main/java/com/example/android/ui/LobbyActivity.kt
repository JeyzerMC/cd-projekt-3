
package com.example.android.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.android.R
import com.example.android.data.GameMode
import com.example.android.databinding.ActivityLobbyBinding
import com.example.android.viewmodel.Lobby
import com.example.android.viewmodel.LobbyViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.w3c.dom.Text

class LobbyActivity : AppCompatActivity() {
    private val model: LobbyViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityLobbyBinding = DataBindingUtil.setContentView(this,
            R.layout.activity_lobby
        )

        binding.model = model
        binding.lifecycleOwner = this

        val gameMode = intent.getSerializableExtra(paramGameMode) as GameMode
        Log.v("lobby", gameMode.modeName)
        model.gameMode.value = gameMode

        GlobalScope.launch {
            when (val message = model.joinLobbySelectionChannel()) {
                "ok" -> toast("Rejoint le lobby de selection de lobby")
                else -> toast("Une erreur s'est produite: $message")
            }
        }

        model.lobbies.observe(this, Observer {lobbies ->
            updateLobbiesList(lobbies.values.toList())
            val selectedLobby = model.selectedLobby.value?: return@Observer
            if (!lobbies.containsKey(selectedLobby.id)) {
                model.selectedLobby.value = null
            }
        })

        model.selectedLobby.observe(this, Observer { lobby ->
            val gameMode = intent.getSerializableExtra(paramGameMode) as GameMode
            lobby?: return@Observer
            updateSelectedLobbyUsers(lobby, gameMode)
        })
    }

    private fun updateSelectedLobbyUsers(selectedLobby: Lobby, gameMode: GameMode) {
        when (gameMode) {
            GameMode.CLASSIC -> {
                val teamAPlayersLayout =  findViewById<LinearLayout>(R.id.team1PlayerNamesList)
                teamAPlayersLayout.removeAllViews()
                selectedLobby.teams["team1"]!!.forEach {
                    val textView = TextView(this)
                    textView.text = it.name
                    teamAPlayersLayout.addView(textView)
                }
                val teamBPlayersLayout = findViewById<LinearLayout>(R.id.team2PlayerNamesList)
                teamBPlayersLayout.removeAllViews()
                selectedLobby.teams["team2"]!!.forEach {
                    val textView = TextView(this)
                    textView.text = it.name
                    teamBPlayersLayout.addView(textView)
                }
            }
            GameMode.SOLOSPRINT -> {

            }
            GameMode.FFA -> {
                val teamAPlayersLayout =  findViewById<LinearLayout>(R.id.team1PlayerNamesList)
                teamAPlayersLayout.removeAllViews()
                selectedLobby.teams["all"]!!.forEach {
                    val textView = TextView(this)
                    textView.text = it.name
                    teamAPlayersLayout.addView(textView)
                }
            }
        }

    }

    private fun toast(message: String) {
        runOnUiThread {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(applicationContext, message, duration)
            toast.show()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        model.leave()
        model.gameMode.postValue(GameMode.NONE)
    }

    private fun startGame(lobbyId: String, isCreator: Boolean){
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra(GameActivity.id, lobbyId)
        intent.putExtra(GameActivity.isCreator, isCreator)

        startActivity(intent)
    }

    private fun updateLobbiesList(lobbies: List<Lobby>) {
        val layout : LinearLayout = findViewById(R.id.lobbiesLayout)
        layout.removeAllViews()


        for (lobby in lobbies) {
            val layoutH = LinearLayout(this)
            layoutH.orientation = LinearLayout.HORIZONTAL
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutH.layoutParams = params
            layoutH.setOnClickListener {
                model.selectLobby(lobby)
                toggleSelectedGroupColor(layout, layoutH)
            }

            params.weight = 0.5f

            val nameTextView = TextView(this)
            nameTextView.text = lobby.id
            nameTextView.textSize = 20f
            if (lobby.open) {
                nameTextView.setTextColor(ContextCompat.getColor(this,R.color.bluePale))
            } else {
                nameTextView.setTextColor(Color.RED)
            }
            nameTextView.gravity = Gravity.CENTER_HORIZONTAL
            nameTextView.layoutParams = params

            val statusTextView = TextView(this)
            statusTextView.text = lobby.nbPlayers.toString()
            statusTextView.textSize = 20f
            statusTextView.setTextColor(ContextCompat.getColor(this,R.color.bluePale))
            statusTextView.gravity = Gravity.CENTER_HORIZONTAL
            statusTextView.layoutParams = params

            layoutH.addView(nameTextView)
            layoutH.addView(statusTextView)

            layout.addView(layoutH)
        }
    }

    private fun toggleSelectedGroupColor(layout : LinearLayout, selected : LinearLayout){
        for (i in 0 until layout.childCount) {
            val child = layout.getChildAt(i)
            child.setBackgroundColor(ContextCompat.getColor(this,R.color.blueDark))
        }
        selected.setBackgroundColor(ContextCompat.getColor(this,R.color.bluePrimary))
    }

    fun onJoinRandomTeamButtonClick(view: View) {
        (view as Button).isEnabled = false
        model.joinSelectedLobby()
        startGame(model.selectedLobby.value!!.id, false)
        model.selectedLobby.postValue(null)
        view.isEnabled = false
    }
    

    fun onCreateLobbyBtnClick(view: View){
        model.loading.postValue(true)
        (view as Button).isEnabled = false
        GlobalScope.launch {
            val lobbyId = model.createLobby()
            model.loading.postValue(false)
            startGame(lobbyId, true)
            runOnUiThread {
                (view as Button).isEnabled = true
            }
        }
    }

    companion object {
        const val paramGameMode = "gameMode"
    }
}
