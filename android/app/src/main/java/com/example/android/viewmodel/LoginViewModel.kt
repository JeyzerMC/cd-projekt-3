package com.example.android.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.data.ChatRepository
import com.example.android.data.SignInParams

class LoginViewModel: ViewModel() {
    private val chatRepository = ChatRepository.getInstance()
    val signInParams: MutableLiveData<SignInParams> = chatRepository.signInParams
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    init {
        loading.value = false
    }
    suspend fun onUserLogin(): String = chatRepository.signIn()


}