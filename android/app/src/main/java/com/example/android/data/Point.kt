package com.example.android.data

data class Point(val x: Float, val y: Float)
