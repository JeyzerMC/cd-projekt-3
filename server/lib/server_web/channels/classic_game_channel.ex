defmodule ServerWeb.ClassicGameChannel do
  use ServerWeb, :channel
  alias Server.Game.Ai
  alias Server.Game.ClassicGame
  alias Server.Game.Strokes
  alias Server.Agents.ClassicGamesAgent
  alias Server.Repo
  alias Server.Game.BaseGame
  alias Server.Models.Word
  alias ServerWeb.ChatChannel

  @gametype "classic_game"

  def join("classic_game:" <> _game_id, _payload, socket) do
    {:ok, socket}
  end

  def ai_draw(socket) do
    "classic_game:" <> game_id = socket.topic

    {:ok, task_pid} =
      Task.start(fn ->
        lobby = ClassicGamesAgent.get_lobby(game_id)

        word_name = lobby.word
        IO.puts("the word is #{word_name}")
        word = Repo.get_by(Word, name: word_name)
        strokes = word.strokes

        Enum.each(strokes, fn stroke ->
          # IO.inspect(stroke)
          new_stroke = Strokes.parse_stroke(stroke)

          broadcast!(socket, "draw", new_stroke)
          :timer.sleep(10)
        end)
      end)

    current_lobby = ClassicGamesAgent.get_lobby(game_id)
    ClassicGamesAgent.update_lobby(game_id, %{current_lobby | ai_draw_pid: task_pid})
  end

  def handle_info(:ai_draw, socket) do
    # TODO: get strokes
    ai_draw(socket)

    {:noreply, socket}
  end

  def handle_info(:change_turn, socket) do
    "classic_game:" <> game_id = socket.topic
    # TODO: change drawer in agent

    lobby = ClassicGamesAgent.get_lobby(game_id)

    Ai.get_change_turn_message(lobby, socket.topic, @gametype)

    broadcast(socket, "change_turn", %{
      drawer: lobby.current_drawer,
      word: lobby.word,
      team: lobby.current_team
    })

    # IO.puts("=--------------------------------------------=")
    # IO.inspect(ClassicGamesAgent.get_lobbies())
    # IO.puts("=--------------------------------------------=")

    broadcast(socket, "score", %{team1: lobby.team1, team2: lobby.team2})

    if lobby.current_drawer.type == "AI" do
      IO.puts("AI is going to draw")
      send(self(), :ai_draw)
    end

    {:noreply, socket}
  end

  def handle_in("start_game", _payload, socket) do
    # TODO: start game in agent
    "classic_game:" <> game_id = socket.topic

    # TODO: validate guess
    lobby = ClassicGamesAgent.get_lobby(game_id)
    lobby = ClassicGame.start_game(lobby)

    ClassicGamesAgent.update_lobby(game_id, lobby)
    # TODO: broadcast update_lobbies to lobby channel
    ServerWeb.Endpoint.broadcast!(
      "lobby:classic",
      "lobbies_update",
      ClassicGamesAgent.get_lobbies_for_update()
    )

    Ai.get_start_message(lobby, socket.topic, @gametype)

    broadcast!(socket, "start_game", %{})
    Process.send_after(self(), :change_turn, 3000)

    {:noreply, socket}
  end

  def handle_in("guess", %{"word" => word}, socket) do
    "classic_game:" <> game_id = socket.topic

    # TODO: validate guess
    lobby = ClassicGamesAgent.get_lobby(game_id)

    if lobby.current_drawer.type == "AI" do
      IO.puts("killing ai draw process")
      Process.exit(lobby.ai_draw_pid, :ok)
    end

    lobby = ClassicGame.validate_guess(lobby, word, socket.assigns.username)

    if lobby.end_game do
      IO.puts("ending the game")
      # IO.inspect(lobby)
      game = BaseGame.endgame(lobby)
      ClassicGamesAgent.update_lobby(game_id, game)

      broadcast(socket, "score", %{team1: lobby.team1, team2: lobby.team2})
      broadcast!(socket, "end_game", %{users: lobby.team1 ++ lobby.team2})
      # TODO: delete lobby
    else
      IO.puts("changing turn")
      ClassicGamesAgent.update_lobby(game_id, lobby)

      send(self(), :change_turn)
    end

    {:noreply, socket}
  end

  def handle_in("add_AI", _payload, socket) do
    "classic_game:" <> game_id = socket.topic

    lobby =
      ClassicGamesAgent.get_lobby(game_id)
      |> ClassicGame.add_ai()

    ClassicGamesAgent.update_lobby(game_id, lobby)

    ServerWeb.Endpoint.broadcast!(
      "lobby:classic",
      "lobbies_update",
      ClassicGamesAgent.get_lobbies_for_update()
    )

    {:noreply, socket}
  end

  def handle_in("draw", payload, socket) do
    broadcast_from!(socket, "draw", payload)
    {:noreply, socket}
  end

  def handle_in("shout", %{"message" => message}, socket) do
    IO.puts("received message in classic game")
    "classic_game:" <> game_id = socket.topic
    game = ClassicGamesAgent.get_lobby(game_id)

    message = ChatChannel.check_censure(message)

    broadcast(socket, "shout", %{
      username: socket.assigns.username,
      message: message
    })

    if message =~ "hint" && game.current_drawer.type == "AI" do
      IO.puts("giving a hint")

      broadcast(socket, "shout", %{
        username: game.current_drawer.username,
        message: Ai.get_hint(game)
      })
    end

    {:noreply, socket}
  end

  def terminate(reason, socket) do
    "classic_game:" <> game_id = socket.topic

    IO.puts("user leaved game #{game_id}")
    IO.puts(socket.assigns.username)
    IO.inspect(reason)

    lobbies = ClassicGamesAgent.get_lobbies()

    if Map.has_key?(lobbies, game_id) do
      game = ClassicGamesAgent.get_lobby(game_id)

      if game.started do
        IO.puts("user leave during classic game")
        broadcast!(socket, "game_error", %{})
        ClassicGamesAgent.delete_lobby(game_id)
      else
        ClassicGamesAgent.leave_lobby(game_id, socket.assigns.username)
      end

      ServerWeb.Endpoint.broadcast(
        "lobby:classic",
        "lobbies_update",
        ClassicGamesAgent.get_lobbies_for_update()
      )
    else
      IO.puts("lobbies already terminated")
    end

    :ok
  end
end
