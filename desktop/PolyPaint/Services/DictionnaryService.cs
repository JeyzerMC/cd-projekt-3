﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PolyPaint.Services
{
    public static class DictionnaryService
    {

        public async static Task<IEnumerable<string>> FetchDictionnary()
        {
            var httpResponse = await HttpService.Client.GetAsync(HttpService.ApiEndpoint + "/fetch_dico");
            var res = await httpResponse.Content.ReadAsStringAsync();

            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);
            var body = responseObj["body"];
            var words = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(body);

            return words.Select(x => x["name"]);
        }

        public async static Task<string> AddEntry(string word)
        {
            Dictionary<string, string> wordDic = new Dictionary<string, string>();
            wordDic.Add("name", word);

            var httpContent = new StringContent(JsonConvert.SerializeObject(wordDic), Encoding.UTF8, "application/json");
            var httpResponse = await HttpService.Client.PostAsync(HttpService.ApiEndpoint + "/add_dico_entry", httpContent);
            Trace.WriteLine(httpResponse);

            var res = await httpResponse.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);

            return responseObj["body"];
        }
    }
}
