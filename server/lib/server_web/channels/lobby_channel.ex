defmodule ServerWeb.LobbyChannel do
  use ServerWeb, :channel
  alias Server.Agents.FfaGameAgent
  alias Server.Agents.ClassicGamesAgent
  alias Server.Agents.SprintSoloAgent

  def join("lobby:" <> _gametype, _, socket) do
    send(self(), :after_join)
    {:ok, socket}
  end

  def get_agent(socket) do
    "lobby:" <> gametype = socket.topic

    agent =
      case gametype do
        "classic" ->
          ClassicGamesAgent

        "ffa" ->
          FfaGameAgent

        "sprint_solo" ->
          SprintSoloAgent
      end

    agent
  end

  def handle_info(:update_lobbies, socket) do
    agent = get_agent(socket)
    # IO.puts("AGENT IS ")
    # IO.inspect(agent)
    # IO.puts("=++++++++++++=")
    broadcast(socket, "lobbies_update", agent.get_lobbies_for_update())
    # IO.puts("================================")
    # IO.inspect(agent.get_open_lobbies())
    # IO.puts("================================")

    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do
    agent = get_agent(socket)

    push(socket, "lobbies_update", agent.get_lobbies_for_update())
    IO.puts("FINISHED AFTER JOIN AND LOBBIES UPDATE")

    {:noreply, socket}
  end

  def handle_in("create_lobby", _payload, socket) do
    agent = get_agent(socket)

    "lobby:" <> game_mode = socket.topic

    IO.puts("creating a #{game_mode} game")

    # TODO: PATTERN MATCH WITH OPTIONAL
    %{id: id, team: team} = agent.create_lobby(socket.assigns.username)

    send(self(), :update_lobbies)

    {:reply, {:ok, %{message: "Lobby successfully created", id: id, team: team}}, socket}
  end

  def handle_in("join_lobby", %{"id" => id}, socket) do
    agent = get_agent(socket)

    # TODO: PATTERN MATCH WITH OPTIONAL

    team = agent.join_lobby(id, socket.assigns.username)

    send(self(), :update_lobbies)
    {:reply, {:ok, %{team: team}}, socket}
  end

  def handle_in("leave_lobby", %{"id" => id}, socket) do
    agent = get_agent(socket)
    IO.puts("leave lobby handle in lobby channel")
    agent.leave_lobby(id, socket.assigns.username)

    send(self(), :update_lobbies)
    {:noreply, socket}
  end

  # def terminate(reason, socket) do
  #   "lobby:" <> gametype = socket.topic
  #   agent = get_agent(socket)

  #   IO.puts("user leaved lobby #{gametype}")
  #   IO.puts(socket.assigns.username)
  #   IO.inspect(reason)

  #   lobbies = agent.get_lobbies()

  #   if Map.has_key?(lobbies, game_id) do
  #     FfaGameAgent.leave_lobby(game_id, socket.assigns.username)

  #     ServerWeb.Endpoint.broadcast(
  #       "lobby:#{gametype}",
  #       "lobbies_update",
  #       agent.get_lobbies_for_update()
  #     )
  #   else
  #     IO.puts("lobbies already terminated")
  #   end

  #   :ok
  # end
end
