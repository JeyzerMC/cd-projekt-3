﻿using System;

namespace PolyPaint.Modeles
{
    public static class UserInformation
    {
        public static String FirstName { get; set; }

        public static String LastName { get; set; }

        public static String Username { get; set; }

        public static String Token { get; set; }
    }
}
