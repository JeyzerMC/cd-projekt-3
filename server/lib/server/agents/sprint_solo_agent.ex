defmodule Server.Agents.SprintSoloAgent do
  use Agent

  alias Server.Structs.User
  alias Server.Structs.Game

  alias Server.Models.User, as: UserModel
  alias Server.Repo

  alias Server.Game.BaseGame

  @name __MODULE__
  @personality ["angry", "cocky", "friendly"]

  def start_link(_) do
    Agent.start_link(fn -> Map.new() end, name: @name)
  end

  def create_lobby(username) do
    # id = Ecto.UUID.generate()
    id = BaseGame.get_random_word() <> BaseGame.get_random_word()

    user = get_user_from_repo(username)

    Agent.update(@name, fn map ->
      map
      |> Map.put_new(id, %Game{
        users: [
          %User{username: user.username, avatar: user.current_avatar, title: user.selected_title}
        ],
        current_drawer: %User{
          username: "Ai " <> BaseGame.get_random_name(),
          type: "AI",
          personality: Enum.random(@personality)
        },
        open: false,
        type: "sprint_solo",
        nb_players: 1
      })
    end)

    IO.puts("creating lobby for sprint solo")

    %{id: id, team: ""}
  end

  def leave_lobby(id, _username) do
    lobby = get_lobby(id)

    if lobby do
      if lobby.ai_draw_pid do
        Process.exit(lobby.ai_draw_pid, :ok)
      end

      Agent.update(@name, fn map ->
        map
        |> Map.delete(id)
      end)
    end

    # IO.puts("AFTER LEAVE LOBBY IN SPRINT SOLO AGENT ==========================")
    # IO.inspect(get_lobby(id))
    # IO.puts("AFTER LEAVE LOBBY IN SPRINT SOLO AGENT ==========================")
  end

  def get_user_from_repo(username) do
    Repo.get_by!(UserModel, username: username)
  end

  def get_lobbies_for_update() do
    lobbies = Agent.get(@name, fn map -> map end)

    for {id, lobby} <- lobbies, into: %{}, do: {id, %{lobby | ai_draw_pid: nil, timer_pid: nil}}
  end

  def get_lobbies() do
    lobbies = Agent.get(@name, fn map -> map end)
    # IO.puts("SPRINT SOLO GET LOBBIES==================")
    # IO.inspect(lobbies)
    # IO.puts("SPRINT SOLO GET LOBBIES==================")
    lobbies
  end

  def get_open_lobbies() do
    all_lobbies = get_lobbies()

    :maps.filter(
      fn _id, lobby_params -> lobby_params.open and !lobby_params.started end,
      all_lobbies
    )
  end

  def get_lobby(id) do
    lobbies = get_lobbies()
    lobby = Map.get(lobbies, id)
    lobby
  end

  def is_started(id) do
    lobby = get_lobby(id)
    lobby.started
  end

  def update_lobby(id, lobby_params) do
    Agent.update(@name, fn map ->
      map
      |> Map.put(id, lobby_params)
    end)
  end
end
