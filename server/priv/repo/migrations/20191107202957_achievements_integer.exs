defmodule Server.Repo.Migrations.AchievementsInteger do
  use Ecto.Migration

  def change do
    alter table(:users) do
      remove :achievements
      add :achievements, {:array, :integer}
    end
  end
end
