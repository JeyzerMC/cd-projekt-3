package com.example.android.data

import android.os.SystemClock
import android.provider.Settings
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.example.android.services.HttpService
import com.example.android.services.SharedPreferencesService
import com.example.android.ui.fragments.OnlineChannel
import com.example.android.utilities.MyCustomApplication
import com.example.android.viewmodel.Lobby
import com.example.android.viewmodel.PaintBrushStyle
import com.example.android.viewmodel.Player
import com.example.android.viewmodel.Profile
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.phoenixframework.Channel
import org.phoenixframework.Socket
import java.text.DateFormat
import java.util.Date
import java.util.Timer
import java.util.TimerTask
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

fun now(): String {
    return DateFormat.getTimeInstance(DateFormat.SHORT).format(Date())
}

enum class GameMode(val modeName: String, val enModeName: String) {
    CLASSIC("Classique", "classic"),
    SOLOSPRINT("Sprint Solo", "sprint_solo"),
    FFA("Mêlée générale", "ffa"),
    NONE("", "")
}

enum class GameState(val stateName: String ) {
    WAIT("attente"),
    ABOUT_TO_START("bientot"),
    VOTE("vote"),
    DRAW("dessine"),
    GUESS("devine"),
    REPLY("replique"),
    WATCH_REPLY("regarde replique"),
    WATCH("regarde"),
    CLEAR_WATCH("regarde"),
    END("fin de partie"),
    ERROR("erreur")
}

// const val BASE_URL = "https://kai26-test.herokuapp.com"
const val BASE_URL = "https://kai26.herokuapp.com"

data class EndGameStats(val username: String, val biggestConsecutiveGuesses: Int?, val wrongGuesses: Int?, val averageTime: Double?, val score: Int?, val correctReplies: Int?)

data class SignInParams(
    var username: String, var password: String) {
    fun validate(): ArrayList<String> {
        val errors: ArrayList<String> = ArrayList()

        if (username.isEmpty()) {
            errors.add("Le nom d'utilisateur doit etre rempli")
        }

        if (password.isEmpty()) {
            errors.add("Le mot de passe doit etre rempli")
        }

        return errors
    }

    override fun toString(): String {
        return "username: $username, password: $password"
    }
}


class ChatRepository {
    private var joinedChannels = mutableListOf<String>()
    private lateinit var socket: Socket
    private lateinit var chatroom: Channel
    private lateinit var gameChannel: Channel
    private lateinit var lobbySelectionChannel: Channel

    val httpService: HttpService = HttpService.getInstance()
    val sharedPreferencesService: SharedPreferencesService = SharedPreferencesService.getInstance()

    var username: String = ""
    var myTeam: String = ""
    val gameMode: MutableLiveData<GameMode?> = MutableLiveData()
    val newStroke: MutableLiveData<Stroke> = MutableLiveData()
    val newMessageSender: MutableLiveData<String> = MutableLiveData()
    val gameState: MutableLiveData<GameState> = MutableLiveData()

    val currentWord: MutableLiveData<String> = MutableLiveData()
    val currentLobby: MutableLiveData<Lobby> = MutableLiveData()
    val remainingTime: MutableLiveData<Double> = MutableLiveData()
    val goodGuess: MutableLiveData<Boolean> = MutableLiveData()
    var remainingTimeTimer: Timer? = null
    val endGameStats: MutableLiveData<List<EndGameStats>> = MutableLiveData()

    val bonusTimeLD : MutableLiveData<Double> = MutableLiveData<Double>().apply{value = 0.0}

    val android_id = Settings.Secure.getString(MyCustomApplication.appGlobalContext.getContentResolver(), Settings.Secure.ANDROID_ID).substring(0, 4)

    val chats: MutableLiveData<MutableMap<String, List<Message>>> = MutableLiveData<MutableMap<String, List<Message>>>().apply{ value = mutableMapOf()}

    val onlineUsers: MutableLiveData<List<String>> = MutableLiveData()
    val onlineChannels: MutableLiveData<List<OnlineChannel>> = MutableLiveData()
    val registerInfo: MutableLiveData<RegisterInfo> = MutableLiveData<RegisterInfo>().apply{
        value = RegisterInfo("abder", "mu", "abder${android_id}", "aa", "aa")
    }
    val signInParams: MutableLiveData<SignInParams> = MutableLiveData<SignInParams>().apply{
        value = SignInParams("abder${android_id}", "aa")
    }
    val lobbies: MutableLiveData<Map<String, Lobby>> = MutableLiveData()

    init {
        gameState.value = GameState.WAIT
        clearMessages()
    }

    private fun clearMessages() {
        GlobalScope.launch(Dispatchers.Main) {
            synchronized(chats) {
                chats.value = mutableMapOf("GENERAL" to emptyList(), "GROUPE" to emptyList())
            }
        }
    }

    suspend fun connectToSocket() = suspendCoroutine<String>  { cont ->
        val token= sharedPreferencesService.get("token")
        val params = hashMapOf("token" to token)
        socket = Socket("$BASE_URL/socket/websocket", params)

        socket.logger = {
            // Log.d("SOCKET", it)
        }

        socket.onOpen {
            cont.resume("open")
        }

        socket.onError { a, response ->
            Log.d("SOCKET", response.toString())
            socket.disconnect {
                Log.v("TAG", "DISCONNECTED")
            }
        }

        socket.onClose {
            Log.d("SOCKET", "closed socket")
        }

        socket.connect()

    }

    suspend fun joinChannel() = suspendCoroutine<String> {cont ->
        clearMessages()
        chatroom = socket.channel("chat:general")
        chatroom.on("shout") { message ->
            newMessageSender.postValue("GENERAL")
            val payload = message.payload
            onMessageReceive(payload["username"] as String, payload["message"] as String)
        }

        chatroom.on("user-joined") { message ->
            val payload = message.payload

            onMessageReceive("System", payload["message"] as String)
        }

        chatroom.on("private_message") { message ->
            val payload = message.payload
            val sender = payload["sender"] as String
            val receiver = payload["receiver"] as String
            val content = payload["message"] as String

            if (sender == username) {
                val newChats = chats.value!!
                newChats["DM: $receiver"] = newChats["DM: $receiver"]!!.plus(Message(now(), sender, content))
                GlobalScope.launch(Dispatchers.Main) {
                    synchronized(chats) {
                        chats.value = newChats
                    }
                }
            }

            if (receiver == username) {
                val newChats = chats.value!!
                newChats["DM: $sender"] = newChats["DM: $sender"]!!.plus(Message(now(), sender, content))
                GlobalScope.launch(Dispatchers.Main) {
                    synchronized(chats) {
                        chats.value = newChats
                        newMessageSender.value = "DM: $sender"
                    }
                }
            }
        }

        chatroom.on("canal_message") { message ->
            val payload = message.payload
            val sender = payload["sender"] as String
            val channel = payload["channel"] as String
            val content = payload["message"] as String

            GlobalScope.launch(Dispatchers.Main) {
                synchronized(chats) {
                    val newChats = chats.value!!
                    if(newChats.containsKey("CH: $channel")) {
                        newChats["CH: $channel"] =
                            newChats["CH: $channel"]!!.plus(Message(now(), sender, content))
                            chats.value = newChats
                            newMessageSender.value = "CH: $channel"
                    }
                }
            }
        }

        chatroom.on("connected-users") { message ->
            val payload = message.payload
            val users = payload["users"] as List<String>
            onlineUsers.postValue(users as List<String>)
            val usersDM = mutableListOf<String>()
            for(user in users) {
                usersDM.add("DM: $user")
            }
            val newChats = chats.value!!
            newChats.keys.minus(usersDM.toSet().plus("GENERAL").plus("GROUPE")).forEach{
                if(it.substring(0,2) == "DM")
                    newChats.remove(it)
            }

            users.forEach{user ->
                if(!newChats.containsKey("DM: $user"))
                    newChats["DM: $user"] = emptyList()
            }
            GlobalScope.launch (Dispatchers.Main) {
                synchronized(chats) {
                    chats.value = newChats
                }
            }
        }

        chatroom.on("existing-channels") { message ->
            val payload = message.payload
            onlineChannels.postValue(payload.map {
                val channelName = it.key
                val users = it.value as List<String>
                OnlineChannel(channelName, users.size.toString())
            })
        }

        chatroom.join()
            .receive("ok") {
                cont.resume("ok")
            }
            .receive("error") {
                cont.resume(it.toString())
            }

    }

    private fun onMessageReceive(sender: String, content: String) {
        GlobalScope.launch(Dispatchers.Main) {
            synchronized(chats) {
            val newChats = chats.value!!
            var generalChat = newChats["GENERAL"]
            generalChat = generalChat!!.plus(
                Message(
                    sender = sender,
                    time = now(),
                    content = content
            ))
            newChats["GENERAL"] = generalChat
                chats.value = newChats
            }
        }
    }

    fun onMessageSend(content: String, currentChat: String) {
        Log.v(javaClass.name, "new outgoing message")
        val payload = hashMapOf("message" to content)
        when(currentChat) {
            "GENERAL" -> chatroom.push("shout", payload)
            "GROUPE" -> gameChannel.push("shout", payload)
            else -> {
                val currentChatName = currentChat.substring(4)
                if(currentChat.substring(0, 2) == "DM") {
                    val payload = hashMapOf("message" to content, "sender" to username, "receiver" to currentChatName)
                    chatroom.push("private_message", payload)
                }
                else {
                    val payload = hashMapOf("message" to content, "sender" to username, "channel" to currentChatName)
                    chatroom.push("canal_message", payload)
                }
            }

        }
    }

    fun onLeaveChannel(name: String){
        val newChats = chats.value!!
        val nameCH = "CH: $name"
        if(newChats.containsKey(nameCH))
            newChats.remove(nameCH)
        if(joinedChannels.contains(nameCH))
            joinedChannels.remove(nameCH)

        chats.postValue(newChats)

        chatroom.push("leave_channel", hashMapOf("name" to name))
    }

    fun onJoinChannel(name: String){
        val newChats = chats.value!!
        val nameCH = "CH: $name"
        if(!newChats.containsKey(nameCH))
            newChats[nameCH] = emptyList()

        joinedChannels.add(nameCH)

        chats.postValue(newChats)
        chatroom.push("join_channel", hashMapOf("name" to name))
    }

    fun onCreateChannel(name: String) {
        chatroom.push("create_channel", hashMapOf("name" to name))
    }

    fun onAddAI() {
        gameChannel.push("add_AI", hashMapOf())
    }

    suspend fun onCreateLobby() = suspendCoroutine<String> {cont ->
        lobbySelectionChannel.push("create_lobby", hashMapOf()).receive("ok") {
            val response  = it.payload["response"] as LinkedTreeMap<String, Any>
            val lobbyId = response["id"]

            when (gameMode.value!!) {
                GameMode.CLASSIC -> {
                    myTeam = response["team"] as String
                }
                GameMode.SOLOSPRINT -> {}
                GameMode.FFA -> {}
            }

            cont.resume(lobbyId.toString())
         }
    }

    fun onJoinLobby(lobby: Lobby) {
        lobbySelectionChannel.push("join_lobby", hashMapOf("id" to lobby.id)).receive("ok") {
            val response  = it.payload["response"] as LinkedTreeMap<String, Any>
            when (gameMode.value!!) {
                GameMode.CLASSIC -> {
                    myTeam = response["team"] as String
                }
                GameMode.SOLOSPRINT -> {

                }
                GameMode.FFA -> {

                }
            }
        }
    }

    fun vote(rating: Int) {
       gameChannel.push("vote", hashMapOf("rating" to rating))
    }

    suspend fun signUp() = suspendCoroutine<String> {cont ->
        val url = "$BASE_URL/api/sign_up"

        val request = JsonObjectRequest(
            Request.Method.POST,
            url,
            JSONObject(
                hashMapOf("user" to hashMapOf(
                    "first_name" to registerInfo.value!!.firstName,
                    "last_name" to registerInfo.value!!.lastName,
                    "username" to registerInfo.value!!.username,
                    "password" to registerInfo.value!!.password,
                    "password_confirmation" to registerInfo.value!!.passwordConfirmation
                )))
            ,
            Response.Listener<JSONObject> { response ->
                val token: String = response.getString("jwt")
                sharedPreferencesService.set("token", token)
                username = registerInfo.value!!.username
                cont.resume("ok")
            },

            Response.ErrorListener {
                cont.resume(it.toString())
            })

        httpService.addToRequestQueue(request)
    }

    fun onStrokeSend(stroke: Stroke) {
        val payload = hashMapOf(
            "color" to stroke.color,
            "width" to stroke.width,
            "startX" to stroke.start.x,
            "startY" to stroke.start.y,
            "endX" to stroke.end.x,
            "endY" to stroke.end.y,
            "style" to when(stroke.style) {
                PaintBrushStyle.SQUARE -> "rectangle"
                PaintBrushStyle.ROUND -> "ellipse"
            }
        )
        when (gameMode.value) {
            GameMode.SOLOSPRINT -> {}
            else -> {
                gameChannel.push("draw", payload)
            }
        }
    }

    private fun onStrokeReceive(stroke: Stroke) {
        newStroke(stroke)
    }

    private fun newStroke(stroke: Stroke) {
        GlobalScope.launch (Dispatchers.Main){
            newStroke.value = stroke
        }
    }

    suspend fun joinLobbySelectionChannel(gameMode: GameMode) = suspendCoroutine<String> {cont ->
        lobbySelectionChannel = socket.channel("lobby:${gameMode.enModeName}")

        lobbySelectionChannel.on("lobbies_update") { message ->
            val payload = message.payload
            val gameState = gameState.value?: return@on

            // TODO: this is a hack because the is winner is updated
            if (gameState == GameState.END) {
                return@on
            }

            lobbies.postValue(payload.map {
                val id = it.key
                val content = it.value as LinkedTreeMap<String, Any>
                val nbPlayers = (content["nb_players"] as Double).toInt()
                val open = content["open"] as Boolean

                var teams: Map<String, List<Player>> = mapOf()
                when (gameMode) {
                    GameMode.CLASSIC -> {
                        val team1 = (content["team1"] as ArrayList<LinkedTreeMap<String, Any>>).map { user -> Player(
                            user["username"] as String, "team1", (user["score"] as Double).toInt(), (user["title"] as Double).toInt(), (user["avatar"] as Double).toInt(), user["type"] == "AI") }.toList()
                        val team2 = (content["team2"] as ArrayList<LinkedTreeMap<String, Any>>).map { user -> Player(
                            user["username"] as String, "team2", (user["score"] as Double).toInt(), (user["title"] as Double).toInt(), (user["avatar"] as Double).toInt(), user["type"] == "AI") }.toList()
                        teams = mapOf("team1" to team1, "team2" to team2)
                    }
                    GameMode.SOLOSPRINT, GameMode.FFA -> {
                        val users = (content["users"] as ArrayList<LinkedTreeMap<String, Any>>).map { user -> Player(
                            user["username"] as String, "all", (user["score"] as Double).toInt(), (user["title"] as Double).toInt(), (user["avatar"] as Double).toInt(), user["type"] == "AI") }.toList()
                        teams = mapOf("all" to users)
                    }
                }
                id to Lobby(id, username, gameMode, open, nbPlayers, teams, null, null)
            }.toMap())
        }

        lobbySelectionChannel.join()
            .receive("ok") {
                cont.resume("ok")
            }
            .receive("error") {
                cont.resume(it.toString())
            }

    }

    fun sendOutOfTime() {
       gameChannel.push("time_out", hashMapOf())
    }

    suspend fun joinGameChannel(gameMode:GameMode, lobbyId : String) = suspendCoroutine<String> {cont ->
        // gameChannel = socket.channel("game:$lobbyId")
        // TODO: cleanup
        // clearing old messages in group channel
        val newChats = chats.value!!
        newChats["GROUPE"] = emptyList()
        chats.postValue(newChats)

        gameChannel = socket.channel("${gameMode.enModeName}_game:$lobbyId")
        gameState.postValue(GameState.WAIT)
        gameChannel.on("draw") { message ->
            val payload = message.payload
            val stroke = Stroke(
                // Color.BLACK,
                (payload["color"] as Double).toInt(),
                (payload["width"] as Double),
                when (payload["style"]) {
                    "ellipse" -> PaintBrushStyle.ROUND
                    "rectangle" -> PaintBrushStyle.SQUARE
                    else -> PaintBrushStyle.SQUARE
                },
                Point((payload["startX"] as Double).toFloat(), (payload["startY"] as Double).toFloat()),
                Point((payload["endX"] as Double).toFloat(), (payload["endY"] as Double).toFloat()))
            onStrokeReceive(stroke)
        }

        gameChannel.on("shout") { message ->
            val payload = message.payload

            val sender = payload["username"] as String
            val content = payload["message"] as String

            val newChats = chats.value!!
            var generalChat = newChats["GROUPE"]
            generalChat = generalChat!!.plus(
                Message(
                    sender = sender,
                    time = now(),
                    content = content
                ))
            newChats["GROUPE"] = generalChat
            GlobalScope.launch(Dispatchers.Main) {
                chats.value = newChats
            }
        }

        gameChannel.on("start_game") {
            gameState.postValue(GameState.ABOUT_TO_START)
            when (gameMode) {
                GameMode.CLASSIC -> {

                }
                GameMode.SOLOSPRINT -> {
                    // TODO: move time logic to server
                    val gameDuration = 90.0 + 3

                    GlobalScope.launch(Dispatchers.Main) {
                        remainingTime.value = gameDuration
                    }
                    remainingTimeTimer = Timer()
                    val soloTimerTask = object: TimerTask() {
                        var elapsedTime = 0.0
                        // var timeLeft: Double = gameDuration - elapsedTime
                        val initialTime = SystemClock.elapsedRealtime()
                        var totalBonus = 0.0

                        override fun run() {
                            val a = remainingTime.value!!
                            val bonus = bonusTimeLD.value!!
                            totalBonus += bonus

                            if (a <= 0.0) {
                                sendOutOfTime()
                                remainingTimeTimer!!.cancel()
                            } else {
                                GlobalScope.launch(Dispatchers.Main) {
                                    elapsedTime = (SystemClock.elapsedRealtime() - initialTime ) / 1000.0
                                    remainingTime.value = gameDuration - elapsedTime + totalBonus
                                    if (bonus > 0) {
                                        bonusTimeLD.value = 0.0
                                    }
                                }
                            }
                        }
                    }
                    remainingTimeTimer!!.scheduleAtFixedRate(soloTimerTask, 0, 1000 )
                }
                GameMode.FFA -> {

                }
            }
        }

        gameChannel.on("vote") {
            gameState.postValue(GameState.VOTE)
        }


        gameChannel.on("game_error") {
            gameState.postValue(GameState.ERROR)
        }

        gameChannel.on("change_turn") { message ->
            val payload = message.payload
            val word = payload["word"] as String
            //TODO: create data structure for this
            val drawer = (payload["drawer"] as Map<String, *>)["username"] as String
            var team: String

            when (gameMode) {
                GameMode.CLASSIC -> {
                    team = payload["team"] as String

                    when {
                        drawer == "-1"  && myTeam == team-> {
                            gameState.postValue(GameState.REPLY)
                        }
                        drawer == "-1"  && myTeam != team-> {
                            gameState.postValue(GameState.WATCH_REPLY)
                        }
                        drawer == username -> {
                            currentWord.postValue(word)
                            gameState.postValue(GameState.DRAW)
                        }
                        team == myTeam -> {
                            gameState.postValue(GameState.GUESS)
                        }
                        else -> {
                            gameState.postValue(GameState.CLEAR_WATCH)
                        }
                    }
                }
                GameMode.SOLOSPRINT -> {
                    team = "all"
                    gameState.postValue(GameState.GUESS)
                }
                GameMode.FFA ->  {
                    val ffaGameDuration = 45.0f

                    GlobalScope.launch(Dispatchers.Main) {
                        remainingTime.value = ffaGameDuration.toDouble()
                    }


                    remainingTimeTimer?.cancel()
                    remainingTimeTimer = Timer()
                    remainingTimeTimer!!.scheduleAtFixedRate(object: TimerTask() {
                        var elapsedTime = 0.0
                        var timeLeft = ffaGameDuration - elapsedTime
                        val initialTime = SystemClock.elapsedRealtime()

                        override fun run() {
                            if(remainingTime.value != null) {
                                GlobalScope.launch(Dispatchers.Main) {
                                    elapsedTime = (SystemClock.elapsedRealtime() - initialTime ) / 1000.0
                                    timeLeft = ffaGameDuration - elapsedTime
                                    remainingTime.value = timeLeft
                                }
                            }
                        }
                    }, 0, 1000 )
                    currentWord.postValue(word)
                    when {
                        drawer == username -> {
                            gameState.postValue(GameState.DRAW)
                        }
                        else -> {
                            gameState.postValue(GameState.GUESS)
                        }
                    }
                    team = "all"
                }
                else -> team = ""
            }

            val lobby = currentLobby.value!!
            currentLobby.postValue(Lobby(lobby.id, username, lobby.gameMode, lobby.open, lobby.nbPlayers, lobby.teams, drawer, team))
        }

        gameChannel.on("score") {
            val content = it.payload as LinkedTreeMap<String, *>

            val lobby = currentLobby.value!!
            var teams: Map<String, List<Player>> = mapOf()
            when (gameMode) {
                GameMode.CLASSIC -> {
                    val team1 = (content["team1"] as ArrayList<LinkedTreeMap<String, Any>>).map { user -> Player(
                        user["username"] as String, "team1", (user["score"] as Double).toInt(), (user["title"] as Double).toInt(), (user["avatar"] as Double).toInt(), user["type"] == "AI") }.toList()
                    val team2 = (content["team2"] as ArrayList<LinkedTreeMap<String, Any>>).map { user -> Player(
                        user["username"] as String, "team2", (user["score"] as Double).toInt(), (user["title"] as Double).toInt(), (user["avatar"] as Double).toInt(), user["type"] == "AI") }.toList()
                    teams = mapOf("team1" to team1, "team2" to team2)
                }
                GameMode.SOLOSPRINT -> {
                    val users = (content["users"] as ArrayList<LinkedTreeMap<String, Any>>).map { user -> Player(
                        user["username"] as String, "team1", (user["score"] as Double).toInt(), (user["title"] as Double).toInt(), (user["avatar"] as Double).toInt(), user["type"] == "AI") }.toList()
                    teams = mapOf("all" to users)
                }
                GameMode.FFA -> {
                    val users = (content["users"] as ArrayList<LinkedTreeMap<String, Any>>).map { user -> Player(
                        user["username"] as String, "team1", (user["score"] as Double).toInt(), (user["title"] as Double).toInt(), (user["avatar"] as Double).toInt(), user["type"] == "AI") }.toList()
                    teams = mapOf("all" to users)
                }
            }

            currentLobby.postValue(Lobby(lobby.id, lobby.myUsername, lobby.gameMode, lobby.open, lobby.nbPlayers, teams, lobby.currentDrawer, lobby.currentDrawingTeam))
        }

        gameChannel.on("end_game") { response ->
            val content = response.payload as LinkedTreeMap<String, *>
            val users = content["users"] as List<LinkedTreeMap<String, *>>
            when (gameMode) {
                GameMode.CLASSIC -> {
                    endGameStats.postValue(users.map {user ->
                        EndGameStats(
                            user["username"] as String,
                            null,
                            (user["wrong_guesses"] as Double).toInt(),
                            null,
                            (user["score"] as Double).toInt(),
                            (user["correct_replies"] as Double).toInt()
                        )
                    })

                }
                GameMode.SOLOSPRINT -> {
                    endGameStats.postValue(users.map {user ->
                        EndGameStats(
                            user["username"] as String,
                            (user["biggest_consecutive_guesses"] as Double).toInt(),
                            (user["wrong_guesses"] as Double).toInt(),
                            user["avg_guess_time"] as Double,
                            null,
                            null
                        )
                    })
                }
                GameMode.FFA -> {
                    endGameStats.postValue(users.map {user ->
                        EndGameStats(
                            user["username"] as String,
                            null,
                            null,
                            null,
                            (user["score"] as Double).toInt(),
                            null
                        )
                    })
                }
            }
            gameState.postValue(GameState.END)
        }

        gameChannel.join()
            .receive("ok") {
                cont.resume("ok")
            }
            .receive("error") {
                cont.resume(it.toString())
            }
    }

    suspend fun getMyUser() = suspendCoroutine<Boolean> {cont ->
        val url = "$BASE_URL/api/my_user"
        val token = sharedPreferencesService.get("token")

        val request = object: JsonObjectRequest(
            Method.GET,
            url,
            null,
            Response.Listener<JSONObject> { response ->
                Log.v("marcus", "$response")
                cont.resume(true)
            },

            Response.ErrorListener {
                Log.v("marcus", "error: $it")
                cont.resume(false)
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $token"
                return headers
            }
        }

        httpService.addToRequestQueue(request)
    }


    suspend fun signIn() = suspendCoroutine<String> {cont ->
        val url = "$BASE_URL/api/sign_in"

        val request = JsonObjectRequest(
            Request.Method.POST,
            url,
            JSONObject(
                hashMapOf(
                    "username" to signInParams.value!!.username,
                    "password" to signInParams.value!!.password
                )
            )
            ,

            Response.Listener<JSONObject> { response ->
                val token: String = response.getString("jwt")
                sharedPreferencesService.set("token", token)
                username = signInParams.value!!.username
                cont.resume("ok")
            },

            Response.ErrorListener {

                cont.resume(it.toString())
            })

        httpService.addToRequestQueue(request)
    }

    fun leaveLobby(lobbyId: String){
        lobbySelectionChannel.push("leave_lobby", hashMapOf("id" to lobbyId))
        remainingTimeTimer?.cancel()
        gameChannel.leave()
    }

    fun leaveSelectionLobby(){
        lobbySelectionChannel.leave()
    }

    fun signOut() {
        chats.postValue(mutableMapOf())
        clearMessages()

        joinedChannels.forEach{onLeaveChannel(it)}
        joinedChannels = mutableListOf()

        sharedPreferencesService.remove("token")
        socket?.disconnect {  }
    }

    fun startGame(lobbyId: String) {
        gameChannel.push("start_game", hashMapOf())
    }

    fun onGuessSend(text: String) {
        println("GUESSSSSSSSSS: $text")
        gameChannel.push("guess", hashMapOf("word" to text)).receive("ok") {
            val response  = it.payload["response"] as LinkedTreeMap<String, Any>
            val guessed: Boolean = response["guessed"] as Boolean

            when (gameMode.value) {
                GameMode.CLASSIC -> {

                }
                GameMode.SOLOSPRINT -> {
                    val bonusTime: Double = response["time_bonus"] as Double
                    val currentRemainingTime = remainingTime.value!!
                    bonusTimeLD.postValue(bonusTime)
                    // remainingTime.postValue(currentRemainingTime + bonusTime)
                }
                GameMode.FFA -> {

                }
            }

            goodGuess.postValue(guessed)

            if (guessed) {
                gameState.postValue(GameState.WATCH)
            }
        }
    }

    suspend fun getProfile() = suspendCoroutine<Pair<Profile?, VolleyError?>> {cont ->
        val url = "$BASE_URL/api/profile"
        val token = sharedPreferencesService.get("token")

        val request = object: JsonObjectRequest(
            Method.GET,
            url,
            null,
            Response.Listener<JSONObject> { response ->
                cont.resume(Pair(Gson().fromJson(response.getString("body"), Profile::class.java), null))
            },

            Response.ErrorListener {error ->
                cont.resume(Pair(null, error))
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $token"
                return headers
            }
        }

        httpService.addToRequestQueue(request)
    }

    suspend fun updateProfile(newProfile: Profile) = suspendCoroutine<Pair<Profile?, VolleyError?>>{ cont ->
        val url = "$BASE_URL/api/update_profile"
        val token = sharedPreferencesService.get("token")

        val json  = JSONObject( Gson().toJson(newProfile))
        val request = object: JsonObjectRequest(
            Method.POST,
            url,
            json,
            Response.Listener<JSONObject> { response ->
                cont.resume(Pair(Gson().fromJson(response.getString("body"), Profile::class.java), null))
            },
            Response.ErrorListener {error ->
                cont.resume(Pair(null, error))
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $token"
                return headers
            }}

        httpService.addToRequestQueue(request)
    }

    suspend fun getPublicProfile(username: String) = suspendCoroutine<Pair<Profile?, VolleyError?>> {cont ->
        val url = "$BASE_URL/api/public_profile?username=$username"
        val token = sharedPreferencesService.get("token")
        val request = object: JsonObjectRequest(
            Method.GET,
            url,
            null,
            Response.Listener<JSONObject> { response ->
                if (response.has("body"))
                    cont.resume(Pair(Gson().fromJson(response.getString("body"), Profile::class.java), null))
                else
                    cont.resume(Pair(null, VolleyError(response.getString("err"))))
            },

            Response.ErrorListener {error ->
                cont.resume(Pair(null, error))
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $token"
                return headers
            }
        }

        httpService.addToRequestQueue(request)

    }


    companion object {
        @Volatile private var instance: ChatRepository? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: ChatRepository().also { instance = it }
            }
    }

}
