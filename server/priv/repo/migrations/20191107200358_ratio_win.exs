defmodule Server.Repo.Migrations.RatioWin do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :ratio_win, :float, default: 1.0
    end
  end
end
