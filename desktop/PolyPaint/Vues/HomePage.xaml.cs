﻿using PolyPaint.Modeles;
using PolyPaint.Services;
using PolyPaint.Vues.UserProfile;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        public HomePage()
        {
            InitializeComponent();
            (Application.Current.MainWindow as MainWindow).dialogFrame.Source = new Uri("Chatbox.xaml", UriKind.RelativeOrAbsolute);
            Navigator.GoBackButton.Visibility = Visibility.Visible;
            Navigator.GoBackButton.Content = "Quitter";
            BrushConverter bc = new BrushConverter();
            homePageBackground.Background = (Brush)bc.ConvertFrom("#002045");
            btnPlay.Background = (Brush)bc.ConvertFrom("#2196F3");
            btnCreation.Background = (Brush)bc.ConvertFrom("#2196F3");
            btnProfil.Background = (Brush)bc.ConvertFrom("#2196F3");
            Navigator.GameMode = 0;
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Vues/ModeSelect.xaml");
        }

        private void ProfileButton_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Vues/UserProfile/selectionProfil.xaml");
        }



        private void Creation_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Vues/Creation Jeu/ListWord.xaml");
        }
    }
}
