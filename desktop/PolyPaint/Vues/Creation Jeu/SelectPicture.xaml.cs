﻿using CsPotrace;
using Microsoft.Win32;
using PolyPaint.Modeles;
using PolyPaint.Modeles.Dessin;
using PolyPaint.Services;
using Svg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    /// 

    public partial class SelectPicture : Page
    {
        public Word word;
        public Bitmap bitmap;
        public bool isLoaded;
        public SelectPicture(Word newWord, Bitmap btm = null)
        {
            InitializeComponent();
            isLoaded = false;
            word = newWord;

            if (btm != null) {
                bitmap = btm;
                isLoaded = true;
            }
        }

        public SelectPicture()
        {
            isLoaded = false;
            word = new Word();
        }

        public void SelectPictureLoaded(object sender, RoutedEventArgs e)
        {
            if (isLoaded)
            {
                //Dispatcher.Invoke(async () =>
                //{
                //    await Task.Delay(500);
                //    pictureTraitment();
                //    showImage();
                //});
                pictureTraitment();
                pictureTraitment();
                showImage();
            }
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            Navigator.GoBack();
        }

        private void word_keyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Add_Click(sender, e);
                e.Handled = true;
            }
        }

        public void SlidePreview(object sender, DragCompletedEventArgs e)
        {
            if (isLoaded)
            {
                pictureTraitment();
                showImage();
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (isLoaded)
            {
                pictureTraitment();
                //validateDraw(bitmap);
                Navigator.Navigate(new Previsualisation(word));
            }
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.bmp)|*.bmp";

            if (openFileDialog.ShowDialog() == true)
            {
                image.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                bitmap = conversionBmpImageToBmp(new BitmapImage(new Uri(openFileDialog.FileName)));
                isLoaded = true;

                pictureTraitment();
                showImage();
            }
        }

        private void SearchOnline_Click(object sender, RoutedEventArgs e) {
            Navigator.Navigate(new SearchOnlinePage(word));
        }

        private Bitmap conversionBmpImageToBmp(BitmapImage bmpImage)
        {

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bmpImage));
                enc.Save(outStream);
                Bitmap bitmap = new Bitmap(outStream);

                return new Bitmap(bitmap);
            }

        }

        private void validateDraw(Bitmap bmp, int turdSize = 2, double treshHold = 0.5, double alphaMax = 1, double optolerance = 0.2, bool curveoptimizing = true)
        {
            Potrace.turdsize = turdSize;
            //Potrace.turnpolicy = new TurnPolicy();
            Potrace.Treshold = treshHold; // important
            Potrace.alphamax = alphaMax;
            Potrace.opttolerance = optolerance;
            Potrace.curveoptimizing = curveoptimizing;


            Potrace.Potrace_Trace(bmp, new List<List<Curve>>());
            word.image = Potrace.getSVG();
            Potrace.Clear();
        }

        private void Preview_Click(object sender, RoutedEventArgs e)
        {
            if (isLoaded) {
                pictureTraitment();
                showImage();
            }
        }

        private void pictureTraitment() {
            int turdSize = (int)InputTurdSize.Value;
            double treshHold = InputTreshOld.Value;
            double alphaMax = InputAlphaMax.Value;
            double optolerance = InputOptolerance.Value;
            bool curveoptimizing = false;
            string curveOpti = "";
            foreach (var radioButton in (CurveOpti as Grid).Children)
            {
                if ((radioButton as RadioButton).IsChecked == true)
                    curveOpti = (radioButton as RadioButton).Content.ToString();
            }
            if (curveOpti == "True")
                curveoptimizing = true;

            validateDraw(bitmap, turdSize, treshHold, alphaMax, optolerance, curveoptimizing);
        }

        private void showImage()
        {
            SvgDocument document = SvgDocument.FromSvg<SvgDocument>(word.image);
            Bitmap bmp = document.Draw();
            image.Source = BitmapToBitmapImage(bmp, ImageFormat.Png);
        }

        public static BitmapImage BitmapToBitmapImage(Bitmap bitmap, ImageFormat imageFormat)
        {
            BitmapImage bitmapImage = new BitmapImage();
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, imageFormat);
                memory.Position = 0;
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
            }
            return bitmapImage;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
