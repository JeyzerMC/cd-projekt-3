﻿using System.Collections.Generic;

namespace PolyPaint.Modeles
{
    class Lobby
    {
        public Lobby(string id, int status, List<Player> players, bool open)
        {
            this.Id = id;
            this.users = players;
            this.Nb_players = status;
            this.open = open;
        }

        public string Id { get; set; }
        public int Nb_players { get; set; }
        public List<Player> users { get; set; }
        public bool open { get; set; }
    }
}
