package com.example.android.viewmodel

import android.text.SpannedString
import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.android.data.ChatRepository
import com.example.android.data.Message

class ChatViewModel: ViewModel() {
    private val chatRepository = ChatRepository.getInstance()

    val onlineUsers: MutableLiveData<List<String>> = chatRepository.onlineUsers
    val chats = chatRepository.chats
    val username: String = chatRepository.username
    var showAddChannelsList: MutableLiveData<Boolean> = MutableLiveData()
    val currentLobby: MutableLiveData<Lobby> = chatRepository.currentLobby
    val currentChatContent: MutableLiveData<SpannedString> = MutableLiveData<SpannedString>().apply {value = SpannedString("")}
    var currentChat = "GENERAL"
    var onlineChannels = chatRepository.onlineChannels

    val newMessageSender = chatRepository.newMessageSender

    val chatsContents: LiveData<Map<String, SpannedString>> = Transformations.map(chats) { newChats: Map<String, List<Message>> ->
        synchronized(chats) {
             newChats.map { (a,b) ->
                 val x = b.fold(SpannedString("")) { acc: SpannedString, message: Message ->
                     TextUtils.concat(acc, "\n", message.toSpannedString()) as SpannedString
                 }
                a to x
            }.toMap()
        }
    }

    fun onLeaveChannel(name: String){
        chatRepository.onLeaveChannel(name)
    }

    fun onJoinChannel(name: String){
        chatRepository.onJoinChannel(name)
    }

    fun onCreateChannel(name: String){
        chatRepository.onCreateChannel(name)
    }

    fun onMessageSend(content:String) {
        chatRepository.onMessageSend(content, currentChat)
    }
}