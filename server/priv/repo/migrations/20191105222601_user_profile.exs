defmodule Server.Repo.Migrations.UserProfile do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :hs_classic, :integer, default: 0
      add :hs_solo, :integer, default: 0
      add :hs_ffa, :integer, default: 0

      add :nb_games, :integer, default: 0
      add :ratioWin, :float, default: 1.0
      add :total_hours, :integer, default: 0

      add :selected_title, :integer
      add :achievements, {:array, :string}

      add :current_avatar, :integer, default: 0
      add :exp_points, :integer, default: 0
    end
  end
end
