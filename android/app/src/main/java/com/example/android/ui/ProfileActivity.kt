package com.example.android.ui

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.example.android.R
import com.example.android.databinding.ActivityProfileBinding
import com.example.android.ui.fragments.PrestigeTab
import com.example.android.ui.fragments.MyProfileTab
import com.example.android.ui.fragments.AchievementTab
import com.example.android.utilities.linkTabViewPager
import com.example.android.viewmodel.Profile
import com.example.android.viewmodel.ProfileViewModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_game.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ProfileActivity : AppCompatActivity() {
    val model: ProfileViewModel by viewModels()
    lateinit var include: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityProfileBinding = DataBindingUtil.setContentView(this,
            R.layout.activity_profile
        )
        binding.model = model
        binding.lifecycleOwner = this
        val isMyProfile = intent.getBooleanExtra(paramProfile, true)
        model.isMyProfile.value = isMyProfile

        val tabList = listOf(
            Pair("Mon Profil", MyProfileTab::class),
            Pair("Prestige", PrestigeTab::class),
            Pair("Accomplissements", AchievementTab::class)
        )

        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        val viewPager = findViewById<ViewPager>(R.id.profileViewPager)

        linkTabViewPager(supportFragmentManager, tabLayout, viewPager, tabList)

        val searchView = findViewById<SearchView>(R.id.publicProfileSearchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                onPublicProfileSearch(query)
                return false
            }
        })
        include = findViewById<View>(R.id.publicProfileInclude)


        model.publicProfile.observe(this, Observer{ profile ->
            updateAvatar(profile)
        })

        GlobalScope.launch {
            val res = model.getProfile()

            if (res.first != null) {
                val profile: Profile = res.first!!
                model.profile.postValue(profile)
                return@launch
            }

            if (res.second != null) {
                return@launch
            }
        }

        model.profile
    }

    fun onPublicProfileSearch(publicProfileName: String) {
        GlobalScope.launch {
            val (profile, error) = model.getPublicProfile(publicProfileName)

            if (profile != null) {
                model.publicProfile.postValue(profile)
            }

            if (error != null)
                toast(error.toString())
        }
    }

    private fun toast(message: String) {
        this.runOnUiThread {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(this, message, duration)
            toast.show()
        }
    }

    fun updateAvatar(profile: Profile) {
        val bitmapAvatar = BitmapFactory.decodeResource(resources, resources.getIdentifier("avatar_${profile.current_avatar}", "drawable", packageName))
        findViewById<ImageView>(R.id.avatar).setImageBitmap(
            Bitmap.createScaledBitmap(
                bitmapAvatar,
                400,
                400,
                true)
        )
    }

    companion object {
        const val paramProfile = "profile"
    }

}

