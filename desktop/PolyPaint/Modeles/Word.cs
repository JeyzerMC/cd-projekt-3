﻿using System.Collections.Generic;

namespace PolyPaint.Modeles
{
    public class Word
    {
        public string name { get; set; }

        public List<string> strokes { get; set; }

        public string difficulty { get; set; }

        public string image;

        public List<string> tips;

        public Word() {
            name = "";
            strokes = new List<string>();
            difficulty = "";
            image = "";
            tips = new List<string>();
        }
    }
}
