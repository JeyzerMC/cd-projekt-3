﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PolyPaint.VueModeles;
using PolyPaint.Modeles;
using System.Collections.ObjectModel;
using PolyPaint.Services;
using Phoenix;
using System.Diagnostics;
using Newtonsoft.Json;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Interaction logic for ClassicLobbiesPage.xaml
    /// </summary>
    public partial class SprintSoloLobbiesPage : Page
    {
        public SprintSoloLobbiesPage()
        {
            InitializeComponent();
            this.DataContext = new LobbySprintSoloViewModel();
        }

        private void Chatbox_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Vues/ModeSelect.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
