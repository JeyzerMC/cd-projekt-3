defmodule Server.Repo.Migrations.AlterSelectedTitle do
  use Ecto.Migration

  def change do
    alter table(:users) do
      modify :selected_title, :integer, default: 0
    end
  end
end
