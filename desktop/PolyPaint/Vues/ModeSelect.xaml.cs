﻿using PolyPaint.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Interaction logic for ModeSelect.xaml
    /// </summary>
    public partial class ModeSelect : Page
    {
        public ModeSelect()
        {
            InitializeComponent();
            Navigator.GoBackButton.Content = "Retour";

            BrushConverter bc = new BrushConverter();
            modeSelectBackground.Background = (Brush)bc.ConvertFrom("#002045");
            btnClassique.Background = (Brush)bc.ConvertFrom("#2196F3");
            btnFfa.Background = (Brush)bc.ConvertFrom("#2196F3");
            btnSolo.Background = (Brush)bc.ConvertFrom("#2196F3");
            Navigator.GameMode = 0;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Vues/HomePage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void ClassicButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Vues/ClassicLobbiesPage.xaml", UriKind.RelativeOrAbsolute));
            Navigator.GameMode = 1;
        }
        private void SprintButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Vues/SprintSoloLobbiesPage.xaml", UriKind.RelativeOrAbsolute));
            Navigator.GameMode = 2;
        }
        private void FreeForAllButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Vues/FFALobbiesPage.xaml", UriKind.RelativeOrAbsolute));
            Navigator.GameMode = 3;
        }
    }
}
