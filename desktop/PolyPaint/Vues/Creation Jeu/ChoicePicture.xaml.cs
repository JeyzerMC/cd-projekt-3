﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Navigation;
using PolyPaint.Modeles;
using PolyPaint.Services;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class ChoicePicture : Page
    {
        Word word;

        public ChoicePicture()
        {
            InitializeComponent();
            word = new Word();
        }

        public ChoicePicture(Word newWord)
        {
            InitializeComponent();
            word = newWord;
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            Navigator.GoBack();
        }
        
        private void DrawImage(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate(new DrawWord(word));
        }

        private void ImportImage(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate(new SelectPicture(word));
        }
    }
}
