﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PolyPaint.Modeles;
using Newtonsoft.Json;
using System.Diagnostics;
using PolyPaint.Services;

namespace PolyPaint.VueModeles
{
    public class RegisterViewModel
    {
        public async Task RegisterUser(String firstName, String lastName, String username, String password, String confirmPassword)
        {
            Dictionary<String, Dictionary<String, String>> user = new Dictionary<String, Dictionary<String, String>>();
            Dictionary<String, String> userInfo = new Dictionary<string, string>
            {
                { "first_name", firstName },
                { "last_name", lastName },
                { "username", username },
                { "password", password },
                { "password_confirmation", confirmPassword }
            };
            user.Add("user", userInfo);

            string jwt = await UserHttpService.UserSignup(JsonConvert.SerializeObject(user));

            UserInformation.Token = jwt;
        }

        public async void ConnectToSocket()
        {
            SocketService.InitSocket();

            await Task.Run(() => {
                SocketService.Connect(UserInformation.Token);
            });
            Trace.WriteLine("After Task Run LoginViewModel.");
        }
    }
}
