﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix;
using WebSocketSharp;
using System.Diagnostics;

namespace PolyPaint.Services
{
    public static class SocketService
    {
        public delegate void ChangeEvent(int connectionStatus);
        public static event ChangeEvent ConnectionChange;  // create an event variable 

        public static Socket Socket { get; set; }
        public static void InitSocket() {
            var socketFactory = new WebsocketSharpFactory();
            Socket = new Socket(socketFactory);
            Socket.OnError += OnErrorCallback;
            Socket.OnOpen += OnOpenCallback;
        }
            
        public static async void Connect(string token)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("token", token);
            Trace.WriteLine("SocketService Connect");
            await Task.Run(() => Socket.Connect("wss://" + HttpService._coreEndpoint + "/socket/", dict));
        }
        private static void OnOpenCallback()
        {
            Trace.WriteLine("OnOpenCallback");
            ConnectionChange(1); //successfully connected
        }
        private static void OnErrorCallback(String message)
        {
            Trace.WriteLine("OnErrorCallback");
            //ConnectionChange(2); //unsuccessful connection
        }
    }

    public sealed class WebsocketSharpAdapter : IWebsocket
    {

        private readonly WebSocket ws;
        private readonly WebsocketConfiguration config;


        public WebsocketSharpAdapter(WebSocket ws, WebsocketConfiguration config)
        {

            this.ws = ws;
            this.config = config;

            ws.OnOpen += OnWebsocketOpen;
            ws.OnClose += OnWebsocketClose;
            ws.OnError += OnWebsocketError;
            ws.OnMessage += OnWebsocketMessage;
        }


        #region IWebsocket methods

        public void Connect() { ws.Connect(); }
        public void Send(string message) { ws.Send(message); }
        public void Close(ushort? code = null, string message = null) { ws.Close(); }

        #endregion


        #region websocketsharp callbacks

        public void OnWebsocketOpen(object sender, EventArgs args) { config.onOpenCallback(this); }
        public void OnWebsocketClose(object sender, CloseEventArgs args) { config.onCloseCallback(this, args.Code, args.Reason); }
        public void OnWebsocketError(object sender, ErrorEventArgs args) { config.onErrorCallback(this, args.Message); }
        public void OnWebsocketMessage(object sender, MessageEventArgs args) { config.onMessageCallback(this, args.Data); }

        #endregion
    }

    public sealed class WebsocketSharpFactory : IWebsocketFactory
    {

        public IWebsocket Build(WebsocketConfiguration config)
        {

            var socket = new WebSocket(config.uri.AbsoluteUri);
            return new WebsocketSharpAdapter(socket, config);
        }
    }
}
