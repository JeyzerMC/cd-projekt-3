﻿using Phoenix;
using PolyPaint.Services;
using PolyPaint.VueModeles;
using PolyPaint.Modeles;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using PolyPaint.Services.Channels;
using Newtonsoft.Json;
using System.Windows.Media.Imaging;
using System.Linq;

namespace PolyPaint.Vues
{
    enum GameStateClassic { Wait, AboutToStart, Draw, Guess, Watch, Idle, Vote, End };

    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class GamePageClassic : Page
    {
        private double widthCanvas;
        private double heightCanvas;
        private string lobbyId;
        private GameStateClassic gameState;
        private string currentWord = "";
        private string playerTeam = "";
        private List<Player> team1;
        private List<Player> team2;
        private bool gameBegan;

        public GamePageClassic()
        {
            InitializeComponent();
            DataContext = new VueModele();

            widthCanvas = this.DrawingCanvas.Width;
            heightCanvas = this.DrawingCanvas.Height;
            gameBegan = false;
            Navigator.NavigationService.LoadCompleted += NavigationService_LoadCompleted;
        }

        void NavigationService_LoadCompleted(object sender, NavigationEventArgs e)
        {
            gameState = GameStateClassic.Wait;
            lobbyId = ((Dictionary<string, object>)e.ExtraData)["id"].ToString();
            playerTeam = ((Dictionary<string, object>)e.ExtraData)["team"].ToString();
            if (!(bool)((Dictionary<string, object>)e.ExtraData)["isCreator"]) //if is not lobby owner
            {
                StartButton.IsEnabled = false;
                StartButton.Content = "Attendre que le maître de la partie débute le jeu";
            }
            JoinGameChannel();
            team1 = LobbyChannelService.team1players;
            team2 = LobbyChannelService.team2players;
            CanAddAI();
            CanStartGame();
            RefreshPlayerList();
            LobbyChannelService.lobbyChannel.On("lobbies_update", m => {
                Dictionary<string, TeamLobby> activeLobbies = JsonConvert.DeserializeObject<Dictionary<string, TeamLobby>>(m.payload.ToString());
                if (activeLobbies.ContainsKey(lobbyId))
                {
                    TeamLobby currentLobby = activeLobbies[lobbyId];
                    team1 = currentLobby.team1;
                    team2 = currentLobby.team2;
                    RefreshPlayerList();
                    CanAddAI();
                    CanStartGame();
                }
            });
            Navigator.NavigationService.LoadCompleted -= NavigationService_LoadCompleted;
        }

        // Pour gérer les points de contrôles.
        private void GlisserCommence(object sender, DragStartedEventArgs e) => (sender as Thumb).Background = Brushes.Black;
        private void GlisserTermine(object sender, DragCompletedEventArgs e) => (sender as Thumb).Background = Brushes.White;
        private void GlisserMouvementRecu(object sender, DragDeltaEventArgs e)
        {
            String nom = (sender as Thumb).Name;
            //    if (nom == "horizontal" || nom == "diagonal") colonne.width = new gridlength(math.max(32, colonne.width.value + e.horizontalchange));
            //    if (nom == "vertical" || nom == "diagonal") ligne.height = new GridLength(Math.Max(32, ligne.Height.Value + e.VerticalChange));
        }

        // Pour la gestion de l'affichage de position du pointeur.
        //private void surfaceDessin_MouseLeave(object sender, MouseEventArgs e) => textBlockPosition.Text = "";
        private void surfaceDessin_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.GetPosition(DrawingCanvas);
            //textBlockPosition.Text = Math.Round(p.X) + ", " + Math.Round(p.Y) + "px";
        }

        private void DupliquerSelection(object sender, RoutedEventArgs e)
        {
            DrawingCanvas.CopySelection();
            DrawingCanvas.Paste();
        }

        private void SupprimerSelection(object sender, RoutedEventArgs e) => DrawingCanvas.CutSelection();

        private void DrawingCanvas_GetInkStrokes(object sender, MouseEventArgs e)
        {
            StrokeCollection strokes = this.DrawingCanvas.Strokes;
            if (strokes.Count == 0)  return;

            Stroke lastStroke = strokes[strokes.Count - 1];

            SendStroke(lastStroke);
        }

        private void SendStroke(Stroke lastStroke)
        {
            // convert color to int
            Color color = lastStroke.DrawingAttributes.Color;
            int colorInt = ColorToInt(color);
            string stylusInt = null;
            switch(lastStroke.DrawingAttributes.StylusTip)
            {
                case (StylusTip.Ellipse):
                    stylusInt = "ellipse";
                    break;
                case (StylusTip.Rectangle):
                    stylusInt = "rectangle";
                    break;
            }

            double width = lastStroke.DrawingAttributes.Width;

            StylusPointCollection points = lastStroke.StylusPoints;

            for (int i = 1; i < points.Count - 1; i++)
            {
                Dictionary<String, object> point = new Dictionary<string, object>
                {
                    { "color", colorInt },
                    { "width", width/widthCanvas },
                    { "startX", points[i-1].X/widthCanvas },
                    { "startY", points[i-1].Y/heightCanvas },
                    { "endX", points[i].X/widthCanvas },
                    { "endY",  points[i].Y/heightCanvas },
                    { "style",  stylusInt}
                };

                GameChannelService.gameChannel.Push("draw", point);
            }
        }

        private void JoinGameChannel()
        {
            GameChannelService.JoinGameChannel("classic_game:" + lobbyId);

            GameChannelService.gameChannel.On("start_game", m =>
            {
                gameState = GameStateClassic.AboutToStart;
                this.Dispatcher.Invoke(() => {
                    Window.GetWindow(this).Opacity = 0.80;
                    WarningLabel.Visibility = Visibility.Visible;
                    this.IsEnabled = false;
                    StartButton.Visibility = Visibility.Hidden;
                    AddAIButton.Visibility = Visibility.Hidden;
                });
            });

            GameChannelService.gameChannel.On("change_turn", m =>
            {
                this.Dispatcher.Invoke(() => {
                    ClearGuessBox();
                    //ClearCanvas();
                    this.DrawingGrid.IsEnabled = false;
                    this.GuessContainer.Visibility = Visibility.Hidden;
                    this.WordContainer.Visibility = Visibility.Hidden;
                    SaveButton.Visibility = Visibility.Hidden;
                });
                if(!gameBegan)
                {
                    this.Dispatcher.Invoke(() => {
                        Window.GetWindow(this).Opacity = 1;
                        WarningLabel.Visibility = Visibility.Hidden;
                        this.IsEnabled = true;
                        gameBegan = true;
                    });
                }
                Trace.WriteLine(m.payload);
                string drawer = m.payload["drawer"]["username"].ToString();
                string currentTeamTurn = m.payload["team"].ToString();
                if (drawer == UserInformation.Username)
                {
                    gameState = GameStateClassic.Draw;
                    currentWord = m.payload["word"].ToString();
                    this.Dispatcher.Invoke(() => {
                        ClearCanvas();
                        this.DrawingGrid.IsEnabled = true;
                        InfoLabel.Content = currentWord;
                        WordContainer.Visibility = Visibility.Visible;
                    });
                }
                else if (currentTeamTurn == playerTeam && drawer.Equals("-1"))
                {
                    gameState = GameStateClassic.Guess;
                    this.Dispatcher.Invoke(() => {
                        GuessContainer.Visibility = Visibility.Visible;
                    });
                    //Droit de replique
                }
                else if (currentTeamTurn == playerTeam && !drawer.Equals("-1"))
                {
                    gameState = GameStateClassic.Guess;
                    this.Dispatcher.Invoke(() => {
                        ClearCanvas();
                        GuessContainer.Visibility = Visibility.Visible;
                    });
                    //Normal guess
                }
                else
                {
                    gameState = GameStateClassic.Watch;
                    this.Dispatcher.Invoke(() => {
                        ClearCanvas();
                        InfoLabel.Content = "Attendez pour votre tour...";
                        WordContainer.Visibility = Visibility.Visible;
                        SaveButton.Visibility = Visibility.Visible;
                    });
                }
                Trace.WriteLine("GameSate: " + gameState);
            });

            GameChannelService.gameChannel.On("score", m => {
                Trace.WriteLine("Game channel score !");
                team1 = new List<Player>(JsonConvert.DeserializeObject<Dictionary<string, Player[]>>(m.payload.ToString())["team1"]);
                team2 = new List<Player>(JsonConvert.DeserializeObject<Dictionary<string, Player[]>>(m.payload.ToString())["team2"]);
                RefreshPlayerList();
            });

            GameChannelService.gameChannel.On("game_error", m =>
            {
                this.Dispatcher.Invoke(() => {
                    ExitGameView(null, null);
                });
            });

            GameChannelService.gameChannel.On("draw", m => {
                //Trace.WriteLine("somebody drawing!");

                StylusPoint startPoint = new StylusPoint((double)m.payload["startX"]*widthCanvas, (double)m.payload["startY"] * heightCanvas);
                StylusPoint endPoint = new StylusPoint((double)m.payload["endX"] * widthCanvas, (double)m.payload["endY"] * heightCanvas);

                StylusPointCollection col = new StylusPointCollection
                {
                    startPoint,
                    endPoint
                };

                Stroke stroke = new Stroke(col);

                stroke.DrawingAttributes.Color = IntToColor((int)m.payload["color"]);
                stroke.DrawingAttributes.Width = (double)m.payload["width"] * widthCanvas;
                stroke.DrawingAttributes.Height = (double)m.payload["width"] * widthCanvas;
                StylusTip style = new StylusTip();
                switch ((string)m.payload["style"])
                {
                    case ("ellipse"):
                        style = StylusTip.Ellipse;
                        break;
                    case ("rectangle"):
                        style = StylusTip.Rectangle;
                        break;
                }
                stroke.DrawingAttributes.StylusTip = style;

                this.Dispatcher.Invoke(() =>
                {
                    this.DrawingCanvas.Strokes.Add(stroke);
                });
            });

            GameChannelService.gameChannel.On("end_game", m =>
            {
                gameState = GameStateClassic.End;
                string winner = GetWinningTeam();
                if(winner.Equals("Draw"))
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        WinnerLabel.Content = "Match nul";
                    });
                }
                else
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        WinnerLabel.Content = "L'équipe gagante est: " + winner;
                    });
                }

                List<object> displayList = new List<object>();
                foreach (Player player in team1)
                {
                    displayList.Add(new
                    {
                        Username = player.username,
                        Score = player.score,
                        WrongGuesses = player.wrong_guesses,
                        CorrectReplies = player.correct_replies
                    });
                }
                foreach (Player player in team2)
                {
                    displayList.Add(new
                    {
                        Username = player.username,
                        Score = player.score,
                    });
                }
                this.Dispatcher.Invoke(() =>
                {
                    FinalScoreDisplay.ItemsSource = displayList;
                    ConfettiEffect.Visibility = Visibility.Visible;
                    FinalScorePanel.Visibility = Visibility.Visible;
                });
                EffectService.PlaySound("applause");
            });
        }

        private Color IntToColor(int colorInt)
        {
            var color = System.Drawing.Color.FromArgb(colorInt);

            return Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        private int ColorToInt(Color color)
        {
            string colorString = color.ToString().Split('#')[1];
            int colorInt = Int32.Parse(colorString, System.Globalization.NumberStyles.HexNumber);

            return colorInt;
        }

        private void Chatbox_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Save_Image(object sender, RoutedEventArgs e)
        {
            Rect rect = new Rect(DrawingCanvas.RenderSize);
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)rect.Right,
              (int)rect.Bottom, 96d, 96d, PixelFormats.Default);
            rtb.Render(DrawingCanvas);
            //endcode as PNG
            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

            //save to memory stream
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            pngEncoder.Save(ms);
            ms.Close();
            System.IO.File.WriteAllBytes($"../{System.Guid.NewGuid()}.png", ms.ToArray());
        }

        private void GuessBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                GameChannelService.gameChannel.Push("guess", new Dictionary<string, object>() { {"word", GuessBox.Text } });
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            GameChannelService.gameChannel.Push("start_game");
        }

        private void AddAI_Click(object sender, RoutedEventArgs e)
        {
            Trace.WriteLine("Added an AI!");
            GameChannelService.gameChannel.Push("add_AI");
        }

        private void ClearCanvas()
        {
            this.DrawingCanvas.Strokes.Clear();
        }

        private void ClearGuessBox()
        {
            this.GuessBox.Text = "";
            this.GuessBox.Background = SystemColors.WindowBrush;
        }

        private string GetWinningTeam()
        {
            int team1Score = 0;
            int team2Score = 0;
            foreach (Player player in team1)
                team1Score += player.score;
            foreach (Player player in team2)
                team2Score += player.score;
            if (team1Score > team2Score)
                return "Team 1";
            else if (team2Score > team1Score)
                return "Team 2";
            else
                return "Draw";
        }

        private void ExitGameView(object sender, RoutedEventArgs e)
        {
            LobbyChannelService.ResetLobbyChannel();
            GameChannelService.LeaveGame();
            Navigator.Navigate("Vues/HomePage.xaml");
        }

        private bool CanAddAI()
        {
            if(!gameBegan)
            {
                int countAI = 0;
                bool canAddAI = true;
                foreach (Player player in team1.Concat(team2))
                {
                    if (player.type.Equals("AI"))
                        countAI++;
                }
                if (countAI >= 2)
                    canAddAI = false;
                if (team1.Count + team2.Count >= 4)
                    canAddAI = false;
                if (!canAddAI)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        AddAIButton.IsEnabled = false;
                    });
                }
                return canAddAI;
            }
            return false;
        }

        private bool CanStartGame()
        {
            if (!gameBegan && (team1.Count + team2.Count != 4))
            {
                this.Dispatcher.Invoke(() =>
                {
                    StartButton.IsEnabled = false;
                });
                return false;
            }
            else
            {
                this.Dispatcher.Invoke(() =>
                {
                    StartButton.IsEnabled = true;
                });
                return true;
            }
        }

        private void RefreshPlayerList()
        {
            List<object> displayList = new List<object>();
            int columnIndex = 0;
            if(team1 != null)
            {
                foreach (Player player in team1)
                {
                    ImageSource avatarSource = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Avatars/Avatar{player.avatar}.png"));
                    avatarSource.Freeze();
                    if (player.type.Equals("AI"))
                    {
                        displayList.Add(new
                        {
                            Avatar = avatarSource,
                            Username = player.username,
                            Score = player.score,
                            WrongGuesses = player.wrong_guesses,
                            CorrectReplies = player.correct_replies,
                            Y = columnIndex,
                            Background = "#FF9C40"
                        });
                    }
                    else {
                        ImageSource badgeSource = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Badges/badge{player.title}.png"));
                        badgeSource.Freeze();
                        displayList.Add(new
                        {
                            Avatar = avatarSource,
                            Username = player.username,
                            Badge = badgeSource,
                            Title = Titles.title[player.title],
                            Score = player.score,
                            WrongGuesses = player.wrong_guesses,
                            CorrectReplies = player.correct_replies,
                            Y = columnIndex,
                            Background = "#FF9C40"
                        });
                    }
                    columnIndex++;
                }
            }
            columnIndex = 2;
            if(team2 != null)
            {
                foreach (Player player in team2)
                {
                    ImageSource avatarSource = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Avatars/Avatar{player.avatar}.png"));
                    avatarSource.Freeze();
                    if (player.type.Equals("AI"))
                    {
                        displayList.Add(new
                        {
                            Avatar = avatarSource,
                            Username = player.username,
                            Score = player.score,
                            WrongGuesses = player.wrong_guesses,
                            CorrectReplies = player.correct_replies,
                            Y = columnIndex,
                            Background = "#2196F3"
                        });
                    }
                    else
                    {
                        ImageSource badgeSource = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Badges/badge{player.title}.png"));
                        badgeSource.Freeze();
                        displayList.Add(new
                        {
                            Avatar = avatarSource,
                            Username = player.username,
                            Badge = badgeSource,
                            Title = Titles.title[player.title],
                            Score = player.score,
                            WrongGuesses = player.wrong_guesses,
                            CorrectReplies = player.correct_replies,
                            Y = columnIndex,
                            Background = "#2196F3"
                        });
                    }
                    columnIndex++;
                }
            }
            this.Dispatcher.Invoke(() =>
            {
                PlayerList.ItemsSource = displayList;
            });
        }
    }
}