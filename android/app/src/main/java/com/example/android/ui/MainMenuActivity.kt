package com.example.android.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.activity.viewModels
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.android.R
import com.example.android.ui.fragments.TutorialDialog
import com.example.android.viewmodel.MainMenuViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainMenuActivity : AppCompatActivity() {
    val model : MainMenuViewModel by viewModels ()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        val firstLogin = intent.getBooleanExtra(paramLogin, false)

        GlobalScope.launch {
            model.getMyUser()
            async {
                val message = model.connectToSocket()
                if (message != "open") {
                    toast("Une erreur s'est produite: $message")
                } else {
                    if (firstLogin) TutorialDialog().show(this@MainMenuActivity.supportFragmentManager, "?")
                }
            }.await()

            when (val message = model.joinChannel()) {
               "ok" -> toast("Rejoint le chat general")
               else -> toast("Une erreur s'est produite: $message")
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        disconnect()
    }

    private fun toast(message: String) {
        runOnUiThread {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(applicationContext, message, duration)
            toast.show()
        }
    }

    fun goToGameModesPage(view: View) {
        startActivity(Intent(this, GameModes::class.java))
    }

    fun goToProfilePage(view: View) {
        startActivity(Intent(this, ProfileSelectionActivity::class.java))
    }


    fun onClickDisconnectButton(view: View) {
        disconnect()
    }

    private fun disconnect() {
        model.onUserDisconnect()
        startActivity(Intent(applicationContext, LoginActivity::class.java))
    }

    companion object {
        const val paramLogin = "firstLogin"
    }
}
