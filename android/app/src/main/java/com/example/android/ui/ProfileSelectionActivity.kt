package com.example.android.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.android.R

class ProfileSelectionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_selection)
    }

    fun onClickMyProfileButton(view: View) {
        val intent = Intent(this, ProfileActivity::class.java)
        intent.putExtra(ProfileActivity.paramProfile, true)
        startActivity(intent)
    }

    fun onClickSearchProfileButton(view: View) {
        val intent = Intent(this, ProfileActivity::class.java)
        intent.putExtra(ProfileActivity.paramProfile, false)
        startActivity(intent)    }
}
