defmodule Server.Models.Word do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, except: [:__meta__]}

  schema "words" do
    field :name, :string
    field :strokes, {:array, :string}
    field :difficulty, :string
    field :image, :string
    field :tips, {:array, :string}

    timestamps()
  end

  @doc false
  def changeset(word, attrs) do
    word
    |> cast(attrs, [:name, :strokes, :difficulty, :image, :tips])
    |> validate_required([:name, :strokes, :difficulty, :image, :tips])
  end
end
