﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using PolyPaint.Modeles;
using PolyPaint.Utilitaires;
using PolyPaint.Services;
using Phoenix;
using Newtonsoft.Json;
using System.Windows.Data;
using PolyPaint.Services.Channels;
using System.Windows;

namespace PolyPaint.VueModeles
{
    class LobbyClassicViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<TeamLobby> LobbyList { get; }

        private object _lobbyListLock = new object();
        private TeamLobby _selectedLobby { get; set; }
        public RelayCommand<object> JoinLobbyClick { get; set; }
        public RelayCommand<object> CreateLobbyClick { get; set; }
        public TeamLobby SelectedLobby
        {
            get { return _selectedLobby; }
            set {
                _selectedLobby = value;
                if (_selectedLobby != null) //if a lobby is selected
                {
                    JoinGroupVisibility = Visibility.Visible;
                    if (_selectedLobby.open)
                        JoinGroupEnabled = true;  //group still has space
                    else
                        JoinGroupEnabled = false; //group is full
                }
                else
                    JoinGroupVisibility = Visibility.Hidden;
                NotifyPropertyChanged();
            }
        }
        public Visibility JoinGroupVisibility
        {
            get { return _joinGroupVisibility; }
            set
            {
                _joinGroupVisibility = value;
                HideGroupVisibility = value;
                NotifyPropertyChanged();
            }
        }

        public Visibility HideGroupVisibility
        {
            get
            {
                if (_joinGroupVisibility == Visibility.Visible)
                {
                    return Visibility.Hidden;
                }
                else
                {
                    return Visibility.Visible;
                }
            }
            set
            {
                NotifyPropertyChanged();
            }
        }

        public Visibility _joinGroupVisibility { get; set; }
        public bool JoinGroupEnabled
        {
            get { return _joinGroupEnabled; }
            set
            {
                _joinGroupEnabled = value;
                NotifyPropertyChanged();
            }
        }
        public bool _joinGroupEnabled { get; set; }
        public bool CreateGroupEnabled
        {
            get { return _createGroupEnabled; }
            set
            {
                _createGroupEnabled = value;
                NotifyPropertyChanged();
            }
        }
        public bool _createGroupEnabled { get; set; }

        public LobbyClassicViewModel()
        {
            this.LobbyList = new ObservableCollection<TeamLobby>();
            JoinLobbyClick = new RelayCommand<object>(JoinLobby);
            CreateLobbyClick = new RelayCommand<object>(CreateLobby);
            BindingOperations.EnableCollectionSynchronization(LobbyList, _lobbyListLock);
            JoinGroupVisibility = Visibility.Hidden;
            CreateGroupEnabled = true;
            JoinSocketChannel();
        }

        private void JoinSocketChannel()
        {
            LobbyChannelService.JoinLobbyChannel("lobby:classic");
            Trace.WriteLine("Connected to classic lobby socket.");
            LobbyChannelService.lobbyChannel.On("lobbies_update", m => {
                Trace.WriteLine("lobbies_update");
                lock (_lobbyListLock)
                    LobbyList.Clear();
                if (m.payload.Count > 0) //if the message-list of active lobbies is not empty
                {
                    Dictionary<string, TeamLobby> response = JsonConvert.DeserializeObject<Dictionary<string, TeamLobby>>(m.payload.ToString());
                    foreach (KeyValuePair<string, TeamLobby> kvp in response)
                        lock (_lobbyListLock)
                            LobbyList.Add(new TeamLobby(kvp.Key.ToString(), kvp.Value.Nb_players, kvp.Value.team1, kvp.Value.team2, kvp.Value.open));
                }
            });
        }

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void JoinLobby(object o)
        {
            if (_selectedLobby != null)
            {
                Dictionary<string, object> payload = new Dictionary<string, object>();
                payload.Add("id", _selectedLobby.Id);
                JoinGroupEnabled = false;
                LobbyChannelService.lobbyChannel.Push("join_lobby", payload).Receive(new Reply.Status(), reply =>
                {
                    Phoenix.Reply message = (Phoenix.Reply)reply;
                    Trace.WriteLine("Joined lobby: " + _selectedLobby.Id);
                    LobbyChannelService.SubscribeGetTeamAvatars(_selectedLobby.Id);
                    JoinGroupEnabled = true;
                    System.Windows.Application.Current.Dispatcher.Invoke(() => NavigateToGame(_selectedLobby.Id, message.response["team"].ToString(), true));
                });
            }
        }

        public void CreateLobby(object o)
        {
            CreateGroupEnabled = false;
            LobbyChannelService.lobbyChannel.Push("create_lobby").Receive(new Reply.Status(), reply => {
                Phoenix.Reply message = (Phoenix.Reply)reply;
                string lobbyId = message.response["id"].ToString();
                Trace.WriteLine("Created lobby: " + lobbyId);
                LobbyChannelService.SubscribeGetTeamAvatars(lobbyId);
                CreateGroupEnabled = true;
                System.Windows.Application.Current.Dispatcher.Invoke(() => NavigateToGame(lobbyId, message.response["team"].ToString(), true));
            });
        }

        private void NavigateToGame(string id, string team, bool isCreator)
        {
            Navigator.Navigate("Vues/GamePageClassic.xaml", new Dictionary<string, object>() { { "id", id }, { "team", team }, { "isCreator", isCreator } });
        }
    }
}
