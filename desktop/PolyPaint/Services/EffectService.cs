﻿using System;
using System.Diagnostics;
using System.Media;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace PolyPaint.Services
{
    public static class EffectService
    {
        private static SoundPlayer player;

        public static void PlaySound(string sound)
        {
            switch (sound)
            {
                case "bloop":
                    player = new SoundPlayer(Properties.Resources.bloop);
                    break;
                case "blip":
                    player = new SoundPlayer(Properties.Resources.blip);
                    break;
                case "applause":
                    player = new SoundPlayer(Properties.Resources.applause);
                    break;
                case "blurp":
                    player = new SoundPlayer(Properties.Resources.blurp);
                    break;
                case "boing":
                    player = new SoundPlayer(Properties.Resources.boing);
                    break;
                case "boo":
                    player = new SoundPlayer(Properties.Resources.boo);
                    break;
                default:
                    player = new SoundPlayer(Properties.Resources.blip);
                    break;
            }

            player.Play();
        }

        public static void animationFrame(DependencyProperty HeightProperty)
        {

            DoubleAnimation animation0 = new DoubleAnimation();
            animation0.From = Navigator.MainFrame.ActualHeight;
            animation0.To = Navigator.MainFrame.ActualHeight - 25;
            animation0.Duration = new Duration(TimeSpan.FromSeconds(0.1));
            animation0.AutoReverse = true;
            animation0.RepeatBehavior = new RepeatBehavior(3);


            Navigator.MainFrame.BeginAnimation(HeightProperty, animation0);
        }
    }
}
