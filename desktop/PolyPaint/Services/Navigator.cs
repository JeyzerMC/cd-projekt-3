﻿using PolyPaint.Services.Channels;
using PolyPaint.Vues;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace PolyPaint.Services
{
    public static class Navigator
    {

        public static MainWindow Window { get; } = Application.Current.MainWindow as MainWindow;

        public static NavigationService NavigationService { get; } = Window.mainFrame.NavigationService;
        public static Frame MainFrame { get; set; } = Window.mainFrame;
        public static Button GoBackButton { get; } = (Application.Current.MainWindow as MainWindow).BtnBackGlobal;

        public static int GameMode { get; set; } = 0;

        public static void Navigate(string path, object param = null)
        {
            NavigationService.Navigate(new Uri(path, UriKind.RelativeOrAbsolute), param);
        }

        public static void Navigate(Page page)
        {
            NavigationService.Navigate(page);
        }

        public static void GoBack()
        {
            NavigationService.GoBack();
            GameChannelService.LeaveGame();
        }

        public static void GoForward()
        {
            NavigationService.GoForward();
        }

        public static void OnLogin()
        {
            Window.borderMF.SetValue(Grid.ColumnSpanProperty, 1);
            Window.borderChat.Visibility = Visibility.Visible;
            Window.BtnTutorial.Visibility = Visibility.Visible;
            Window.ApplicationNameTB.Margin = new Thickness(50, 0, 0, 0);
            ChatService.Reset();
        }

        public static void OnLogout()
        {
            Window.borderMF.SetValue(Grid.ColumnSpanProperty, 2);
            Window.borderChat.Visibility = Visibility.Collapsed;
            Window.BtnTutorial.Visibility = Visibility.Collapsed;
            Window.ApplicationNameTB.Margin = new Thickness(350, 0, 0, 0);
            ChatService.roomChannel = null;
        }

        public static string GetCurrentTime()
        {
            return DateTime.Now.ToString("HH:mm:ss");
        }

        public static void ShowTutorial()
        {
            switch(GameMode)
            {
                case 0:
                    Window.borderTutorial0.Visibility = Visibility.Visible;
                    break;
                case 1:
                    Window.borderTutorial1.Visibility = Visibility.Visible;
                    break;
                case 2:
                    Window.borderTutorial2.Visibility = Visibility.Visible;
                    break;
                case 3:
                    Window.borderTutorial3.Visibility = Visibility.Visible;
                    break;
                default:
                    Window.borderTutorial0.Visibility = Visibility.Visible;
                    break;
            }
        }

        public static void HideTutorial()
        {
            Window.borderTutorial0.Visibility = Visibility.Collapsed;
            Window.borderTutorial1.Visibility = Visibility.Collapsed;
            Window.borderTutorial2.Visibility = Visibility.Collapsed;
            Window.borderTutorial3.Visibility = Visibility.Collapsed;
        }
    }
}
