package com.example.android.services

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.example.android.utilities.MyCustomApplication.Companion.appGlobalContext

class HttpService {
    companion object {
        @Volatile private var instance: HttpService? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: HttpService().also { instance = it }
            }
    }


    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(appGlobalContext)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }
}
