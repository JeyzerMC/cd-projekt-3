﻿using Newtonsoft.Json;
using PolyPaint.Modeles;
using PolyPaint.Modeles.Dessin;
using PolyPaint.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Logique d'interaction pour Previsualisation.xaml
    /// </summary>
    public partial class Previsualisation : Page
    {

        private Word word;
        private List<DessinSegment> paths;

        private List<DessinStroke> strokes;

        private int drawingMode = 0;
        private int drawingOption = 0;

        private int currentState = 0;

        private bool fromDrawing;

        private StrokeCollection drawingStrokes;
        private double inkCanvasHeight;
        private double inkCanvasWidth;

        public Previsualisation(Word w)
        {
            InitializeComponent();
            word = w;
            word.difficulty = "Facile";
            fromDrawing = false;
            strokes = new List<DessinStroke>();
        }

        public Previsualisation(Word w, StrokeCollection strks, double height, double width)
        {
            InitializeComponent();
            word = w;
            word.difficulty = "Facile";
            fromDrawing = true;
            drawingStrokes = strks;
            inkCanvasHeight = height;
            inkCanvasWidth = width;
            strokes = new List<DessinStroke>();
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            Navigator.GoBack();
        }

        public void PrevisualisationLoaded(object sender, RoutedEventArgs e)
        {
            RBEasy.Click += (s, o) => ChooseDifficulty("Facile");
            RBMedium.Click += (s, o) => ChooseDifficulty("Moyen");
            RBHard.Click += (s, o) => ChooseDifficulty("Difficile");

            RBRandom.Click += (s, o) => ChooseDrawingMode("Random");
            RBPano.Click += (s, o) => ChooseDrawingMode("Panoramique");
            RBCenter.Click += (s, o) => ChooseDrawingMode("Centered");

            RBLeft.Click += (s, o) => ChooseDrawingOption(0);
            RBRight.Click += (s, o) => ChooseDrawingOption(1);
            RBUp.Click += (s, o) => ChooseDrawingOption(2);
            RBDown.Click += (s, o) => ChooseDrawingOption(3);

            RbInside.Click += (s, o) => ChooseDrawingOption(0);
            RBOutside.Click += (s, o) => ChooseDrawingOption(1);

            if (fromDrawing)
            {
                TreatDrawing();
            }
            else
            {
                TreatSvg(word.image);
            }
        }

        public void ChooseDifficulty(string difficulty)
        {
            word.difficulty = difficulty;
        }

        public void ChooseDrawingMode(string mode)
        {
            if (mode == "Random")
            {
                drawingMode = 0;
                DrawingOptions.Visibility = Visibility.Collapsed;
            }
            else
            {
                DrawingOptions.Visibility = Visibility.Visible;
            }

            if (mode == "Panoramique")
            {
                drawingMode = 1;
                drawingOption = 0;
                RBLeft.IsChecked = true;
                PanoramiqueOptions.Visibility = Visibility.Visible;
                CenterOptions.Visibility = Visibility.Collapsed;
            }
            else
            {
                drawingMode = 2;
                drawingOption = 0;
                RbInside.IsChecked = true;
                PanoramiqueOptions.Visibility = Visibility.Collapsed;
                CenterOptions.Visibility = Visibility.Visible;
            }
        }

        public void ChooseDrawingOption(int option)
        {
            drawingOption = option;
        }

        public void PreviewClick(object sender, RoutedEventArgs e)
        {
            currentState++;
            PreviewCanvas.Children.Clear();

            OrderByMode();
            ConvertToStrokes();
            DrawOnCanvas(currentState);
        }

        public void NextClick(object sender, RoutedEventArgs e)
        {
            OrderByMode();
            ConvertToStrokes();
            Navigator.Navigate(new LexicalFieldWord(word));
        }

        private void TreatDrawing()
        {
            paths = new List<DessinSegment>();

            foreach (var stroke in drawingStrokes)
            {
                var path = new DessinSegment();
                foreach (var point in stroke.StylusPoints)
                {
                    var pt = ScalePtn(new Point(point.X, point.Y), (int)inkCanvasHeight, (int)inkCanvasWidth);
                    DessinStroke stk = new DessinStroke();
                    stk.X = pt.X;
                    stk.Y = pt.Y;

                    stk.Color = stroke.DrawingAttributes.Color;
                    stk.Width = stroke.DrawingAttributes.Width / inkCanvasWidth;
                    stk.Tip = stroke.DrawingAttributes.StylusTip;
                    path.Points.Add(stk);
                }
                paths.Add(path);
            }
        }

        private void TreatSvg(string svg)
        {
            var height = ExtractDimension(svg, "height");
            var width = ExtractDimension(svg, "width");

            var res = svg.Remove(svg.LastIndexOf("\"\n"));
            res = res.Substring(res.IndexOf("M "));

            var parsedGeomery = Geometry.Parse(res);
            var pathGeometry = parsedGeomery.GetFlattenedPathGeometry();

            paths = new List<DessinSegment>();

            foreach (var fig in pathGeometry.Figures)
            {
                var path = new DessinSegment();
                var tmpPt = ScalePtn(fig.StartPoint, height, width);
                DessinStroke stk = new DessinStroke();
                stk.X = tmpPt.X;
                stk.Y = tmpPt.Y;
                var stkWidth = 1.0;

                stk.Color = Brushes.Black.Color;
                stk.Width = stkWidth / width;
                stk.Tip = StylusTip.Ellipse;

                path.Points.Add(stk);

                foreach (var seg in fig.Segments)
                {
                    var lineSeg = seg as PolyLineSegment;
                    foreach (var pt in lineSeg.Points)
                    {
                        Point tmpPt2 = ScalePtn(pt, height, width);
                        DessinStroke stk2 = new DessinStroke();
                        stk2.X = tmpPt2.X;
                        stk2.Y = tmpPt2.Y;

                        stk2.Color = Brushes.Black.Color;
                        stk2.Width = stkWidth / width;
                        stk2.Tip = StylusTip.Ellipse;
                        path.Points.Add(stk2);
                    }
                }
                paths.Add(path);
            }
        }

        private int ExtractDimension(string svg, string dimension)
        {
            var res = svg.Substring(svg.IndexOf(dimension) + dimension.Length + 2);
            res = res.Remove(res.IndexOf("\""));

            return int.Parse(res);
        }

        private Point ScalePtn(Point pt, int imgHeight, int imgWidth)
        {
            var res = new Point();
            res.X = pt.X / imgWidth;
            res.Y = pt.Y / imgHeight;
            return res;
        }

        private void OrderByMode()
        {
            switch(drawingMode)
            {
                case 0: OrderizeRandom(); break;
                case 1: OrderizePanoramique(); break;
                case 2: OrderizeCentre(); break;
            }
        }

        private void OrderizeRandom()
        {
            paths = paths.OrderBy(a => Guid.NewGuid()).ToList();
        }

        private void OrderizePanoramique()
        {
            switch(drawingOption)
            {
                case 0:
                    paths = paths.OrderBy(a => a.Points[0].X).ToList();
                    break;
                case 1:
                    paths = paths.OrderByDescending(a => a.Points[0].X).ToList();
                    break;
                case 2:
                    paths = paths.OrderBy(a => a.Points[0].Y).ToList();
                    break;
                case 3:
                    paths = paths.OrderByDescending(a => a.Points[0].Y).ToList();
                    break;
            }
        }

        private void OrderizeCentre()
        {
            switch (drawingOption)
            {
                case 0:
                    paths = paths.OrderBy(a => DistToCenter(a.Points[0].X, a.Points[0].Y)).ToList();
                    break;
                case 1:
                    paths = paths.OrderByDescending(a => DistToCenter(a.Points[0].X, a.Points[0].Y)).ToList();
                    break;
            }
        }

        private double DistToCenter(double x, double y)
        {
            var distX = Math.Pow(x - 0.5, 2);
            var distY = Math.Pow(y - 0.5, 2);
            return Math.Sqrt(distX + distY);
        }

        private void ConvertToStrokes()
        {
            strokes = new List<DessinStroke>();
            foreach (var path in paths)
            {
                for (int i = 0; i < path.Points.Count - 2; i++)
                {
                    var stk = path.Points[i];
                    stk.X2 = path.Points[i + 1].X;
                    stk.Y2 = path.Points[i + 1].Y;
                    strokes.Add(stk);
                }
            }

           SerializeStrokes();
        }

        private void SerializeStrokes()
        {
            word.strokes = new List<string>();

            foreach (var stroke in strokes)
            {
                Dictionary<string, string> wordDic = new Dictionary<string, string>();

                wordDic.Add("X", stroke.X.ToString().Replace(",", "."));
                wordDic.Add("Y", stroke.Y.ToString().Replace(",", "."));
                wordDic.Add("X2", stroke.X2.ToString().Replace(",", "."));
                wordDic.Add("Y2", stroke.Y2.ToString().Replace(",", "."));
                wordDic.Add("Color", stroke.Color.ToString());
                wordDic.Add("Tip", stroke.Tip.ToString());
                wordDic.Add("Width", stroke.Width.ToString().Replace(",", "."));

                var st = JsonConvert.SerializeObject(wordDic);
                word.strokes.Add(st.Replace("\"", "\'"));
            }
        }


        private void DrawOnCanvas(int state)
        {
            Dispatcher.Invoke(async () =>
            {
                foreach (var stroke in strokes)
                {
                    if (currentState == state)
                    {
                        ProcessTask(stroke, state);
                        await Task.Delay(1);
                    }
                }
            });
        }

        private void ProcessTask(DessinStroke pt, int state)
        {
            if (currentState != state)
            {
                return;
            }

            Line line = new Line();
            line.Stroke = new SolidColorBrush(pt.Color);

            if (pt.Tip == StylusTip.Ellipse)
            {
                line.StrokeStartLineCap = PenLineCap.Round; 
                line.StrokeEndLineCap = PenLineCap.Round;
            } else
            {
                line.StrokeStartLineCap = PenLineCap.Square;
                line.StrokeEndLineCap = PenLineCap.Square;
            }

            line.StrokeThickness = pt.Width * PreviewCanvas.ActualWidth;

            line.X1 = pt.X * PreviewCanvas.ActualWidth;
            line.Y1 = pt.Y * PreviewCanvas.ActualHeight;
            line.X2 = pt.X2 * PreviewCanvas.ActualWidth;
            line.Y2 = pt.Y2 * PreviewCanvas.ActualHeight;
            PreviewCanvas.Children.Add(line);
        }
    }
}
