defmodule ServerWeb.SprintSoloChannel do
  use ServerWeb, :channel
  alias Server.Agents.SprintSoloAgent
  alias Server.Game.SprintSolo
  alias Server.Game.Strokes
  alias Server.Game.BaseGame
  alias Server.Game.Ai
  alias ServerWeb.ChatChannel

  alias Server.Repo

  alias Server.Structs.User

  alias Server.Models.Word

  @gametype "sprint_solo_game"

  def join("sprint_solo_game:" <> _game_id, _payload, socket) do
    {:ok, socket}
  end

  def ai_draw(socket) do
    "sprint_solo_game:" <> game_id = socket.topic

    {:ok, task_pid} =
      Task.start(fn ->
        lobby = SprintSoloAgent.get_lobby(game_id)

        word_name = lobby.word
        IO.puts("the word is #{word_name}")
        IO.puts("AI is drawing")

        word = Repo.get_by(Word, name: word_name)
        strokes = word.strokes

        Enum.each(strokes, fn stroke ->
          parsed_stroke = Strokes.parse_stroke(stroke)

          broadcast!(socket, "draw", parsed_stroke)
          :timer.sleep(10)
        end)
      end)

    current_lobby = SprintSoloAgent.get_lobby(game_id)
    SprintSoloAgent.update_lobby(game_id, %{current_lobby | ai_draw_pid: task_pid})
  end

  def handle_info(:change_turn, socket) do
    "sprint_solo_game:" <> game_id = socket.topic

    game =
      SprintSoloAgent.get_lobby(game_id)
      |> SprintSolo.change_turn()

    SprintSoloAgent.update_lobby(game_id, game)

    # IO.puts("AFTER CHANGING TURN=========================")
    # IO.inspect(game)
    # IO.puts("AFTER CHANGING TURN=========================")

    ai_draw(socket)

    # Ai.get_change_turn_message(game, socket.topic, @gametype)

    broadcast(socket, "change_turn", %{
      drawer: %User{},
      word: ""
    })

    {:noreply, socket}
  end

  def handle_info(:end_game, socket) do
    "sprint_solo_game:" <> game_id = socket.topic

    game =
      SprintSoloAgent.get_lobby(game_id)
      |> SprintSolo.end_game(socket.assigns.username)
      |> BaseGame.endgame()

    IO.puts("ending game")

    broadcast!(socket, "end_game", %{
      users: game.users
    })

    {:noreply, socket}
  end

  def handle_in("start_game", _payload, socket) do
    "sprint_solo_game:" <> game_id = socket.topic

    game =
      SprintSoloAgent.get_lobby(game_id)
      |> SprintSolo.start_game()

    SprintSoloAgent.update_lobby(game_id, game)

    # Ai.get_start_message(game, socket.topic, @gametype)

    # IO.puts("AFTER STARTING GAME=========================")
    # IO.inspect(game)
    # IO.puts("AFTER STARTING GAME=========================")

    broadcast!(socket, "start_game", %{})

    Process.send_after(self(), :change_turn, 3000)

    {:noreply, socket}
  end

  def handle_in("guess", %{"word" => word}, socket) do
    "sprint_solo_game:" <> game_id = socket.topic

    game =
      SprintSoloAgent.get_lobby(game_id)
      |> SprintSolo.validate_guess(word, socket.assigns.username)

    SprintSoloAgent.update_lobby(game_id, game)

    cond do
      game.word == word ->
        IO.puts("correct word")

        Process.exit(game.ai_draw_pid, :ok)

        broadcast!(socket, "score", %{users: game.users})
        send(self(), :change_turn)

        {:reply,
         {:ok,
          %{
            guessed: true,
            time_bonus: game.word_time_bonus,
            message: "You guessed the correct word"
          }}, socket}

      game.current_guesses == game.max_guesses ->
        IO.puts("out of guesses")

        Process.exit(game.ai_draw_pid, :ok)
        send(self(), :change_turn)

        {:reply,
         {:ok, %{guessed: false, time_bonus: 0, message: "You did not guess the correct word"}},
         socket}

      true ->
        IO.puts("wrong word")

        {:reply,
         {:ok, %{guessed: false, time_bonus: 0, message: "You did not guess the correct word"}},
         socket}
    end
  end

  def handle_in("time_out", _payload, socket) do
    IO.puts("sprint solo timeout recevied")
    "sprint_solo_game:" <> game_id = socket.topic

    if SprintSoloAgent.get_lobby(game_id) do
      send(self(), :end_game)
    end

    {:noreply, socket}
  end

  def handle_in("shout", %{"message" => message}, socket) do
    IO.puts("receiving a message in sprint solo")
    "sprint_solo_game:" <> game_id = socket.topic
    game = SprintSoloAgent.get_lobby(game_id)

    message = ChatChannel.check_censure(message)

    broadcast(socket, "shout", %{
      username: socket.assigns.username,
      message: message
    })

    if message =~ "hint" do
      IO.puts("giving a hint")

      broadcast(socket, "shout", %{
        username: game.current_drawer.username,
        message: Ai.get_hint(game)
      })
    end

    {:noreply, socket}
  end

  def terminate(reason, socket) do
    "sprint_solo_game:" <> game_id = socket.topic

    IO.puts("user leaved sprint solo game #{game_id}")
    IO.puts(socket.assigns.username)
    IO.inspect(reason)

    lobbies = SprintSoloAgent.get_lobbies()

    IO.puts("terminated and deleting solo sprint lobby")

    if Map.has_key?(lobbies, game_id) do
      SprintSoloAgent.leave_lobby(game_id, socket.assigns.username)

      ServerWeb.Endpoint.broadcast(
        "lobby:sprint_solo",
        "lobbies_update",
        SprintSoloAgent.get_lobbies_for_update()
      )
    else
      IO.puts("lobbies already terminated")
    end

    :ok
  end
end
