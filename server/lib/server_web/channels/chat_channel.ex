defmodule ServerWeb.ChatChannel do
  use ServerWeb, :channel
  alias ServerWeb.Presence
  alias Server.Agents.ChannelsAgent

  def join("chat:general", _, socket) do
    send(self(), :after_join)
    IO.puts("USER HAS JOINED HELLO USER")
    {:ok, socket}
  end

  def handle_in("shout", %{"message" => message}, socket) do
    message = check_censure(message)

    broadcast(socket, "shout", %{
      username: socket.assigns.username,
      message: message
    })

    {:noreply, socket}
  end

  def handle_in(
    "canal_message",
    %{"channel" => canal, "message" => message},
    socket
  ) do
    message = check_censure(message)

    broadcast(socket, "canal_message", %{
      channel: canal,
      sender: socket.assigns.username,
      message: message
    })

    {:noreply, socket}
  end

  def handle_in(
        "private_message",
        %{"sender" => sender, "receiver" => receiver, "message" => message},
        socket
      ) do
    message = check_censure(message)

    broadcast(socket, "private_message", %{
      sender: sender,
      receiver: receiver,
      message: message
    })

    {:noreply, socket}
  end

  def handle_in("create_channel", %{"name" => name}, socket) do
    ChannelsAgent.add_channel(name, socket.assigns.username)
    {:noreply, socket}
  end

  def handle_in("join_channel", %{"name" => name}, socket) do
    ChannelsAgent.join_channel(name, socket.assigns.username)
    {:noreply, socket}
  end

  def handle_in("leave_channel", %{"name" => name}, socket) do
    ChannelsAgent.leave_channel(name, socket.assigns.username)
    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do
    broadcast(socket, "user-joined", %{
      message: "User #{socket.assigns.username} joined"
    })

    {:ok, _} =
      Presence.track(socket, :users, %{
        username: socket.assigns.username
      })

    schedule_presence()
    {:noreply, socket}
  end

  def handle_info(:send_presence, socket) do
    {_, users} = Presence.get_users()

    broadcast(socket, "connected-users", users)
    broadcast(socket, "existing-channels", ChannelsAgent.get_channels())

    schedule_presence()
    {:noreply, socket}
  end

  defp schedule_presence() do
    Process.send_after(self(), :send_presence, 1000)
  end

  def check_censure(message) do
    pt = Path.join(:code.priv_dir(:server), "data/censures.txt")
    {:ok, censured} = File.read(pt)
    censured = String.split(censured, "\n")

    message
    |> String.split()
    |>
    Enum.map(fn word ->
      if Enum.member?(censured, word) do
        String.duplicate("*", String.length(word))
      else
        word
      end
    end)
    |> Enum.join(" ")
  end

  def terminate(reason, socket) do
    IO.puts "USER DISCONNECTED"
    IO.puts(socket.assigns.username)
    IO.inspect(reason)

    channels = ChannelsAgent.get_channels()

    Enum.each(channels, fn {k, v} ->
      if Enum.member?(v, socket.assigns.username) do
        ChannelsAgent.leave_channel(k, socket.assigns.username)
      end
    end)

    :ok
  end
end
