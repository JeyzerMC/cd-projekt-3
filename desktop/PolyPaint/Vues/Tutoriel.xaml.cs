﻿using PolyPaint.Services;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Logique d'interaction pour Tutoriel.xaml
    /// </summary>
    public partial class Tutoriel : UserControl
    {

        int currentSlide = 0;
        int MAX_SLIDES = 20;

        List<string> descriptions = new List<string>()
        {
            "Défilez le tutoriel pour plus d'informations sur chaque mode de jeu, ou consultez le tutoriel de chacun dans leur page respective.",

             /* CLASSIQUE*/
            "Général:\nEn équipe de 2, un des membres dessine et l'autre devine.\nDevinez plus de dessins que l'équipe adversaire pour gagner!",

            "Préparation:\nAvant le début de la partie, les joueurs sont répartis en deux équipes: ORANGE et BLEUE.\nSi vous n'êtes pas 4 joueurs, il est possible d'ajouter 1 JOUEUR VIRTUEL par équipe.",

            "Début de partie:\n Après avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\nLorsque celle-ci disparait, la partie commence immédiatement.",

            "Déroulement de partie:\nVotre tour:\nSi vous voyez un mot en gras en bas de l'écran, vous avez le rôle de DESSINEUR.\nAvec les outils de dessin, essayez de dessiner le mot en gras.",

            "Si vous voyez un champ pour deviner en bas de l'écran, vous avez le rôle de DEVINEUR. Vous avez une tentative pour deviner le mot que votre coéquipier dessine et gagner un point.\nSi vous donnez une mauvaise réponse, un droit de réplique est donné à l'équipe adversaire.\nEnsuite c'est le tour de l'équipe adverse.",

            "Tour de l'équipe adverse:\nSi vous voyez « Attendez votre tour...», c'est le tour de l'équipe adverse.\n Votre tour arrivera après que l'équipe adverse gagne un point ou vous donne un droit de réplique.",

            "Fin de partie:\nUne manche est finie après que chaque équipe joue leur tour une fois.\nLa partie se termine après deux manches. L'équipe gagnante est celle qui a accumulé le plus de points!",

             /* SOLO*/
            "Général:\nVous jouez en solo contre la montre! Devinez le plus de dessins possible dans le laps de temps accordé.",

            "Préparation:\nLorsque vous êtes prêt, vous pouvez choisir de COMMENCER LA PARTIE.",

            "Début de partie:\nAprès avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\nLorsque celle-ci disparait, la partie commence immédiatement.",

            "Déroulement de partie:\nDevinez le mot qui est en train de se faire dessiner en entrant votre réponse dans le champs en bas de l'écran.\nLe temps bonus accordé varie selon la difficulté du dessin.",

            "Lorsque vous donnez la bonne réponse, vous passez au prochain dessin.",

            "Fin de partie:\nLa partie se termine lorsque le temps est écoulé.",

            /* FFA*/
            "Général:\nChacun joue à son compte. Il peut y avoir jusqu'à 4 joueurs.\nDevinez le plus de dessins pour gagner.",

            "Préparation:\nLorsque vous êtes prêt, un joueur peut choisir de COMMENCER LA PARTIE.",

            "Début de partie:\nAprès avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\nLorsque celle-ci disparait, la partie commence immédiatement.",

            "Déroulement de partie:\nVotre tour:\nSi vous voyez un mot en gras en bas de l'écran, vous avez le rôle de DESSINEUR.\nAvec les outils de dessin, essayez de dessiner le mot en gras.\n",

            "Si vous avez un champ pour deviner en bas de l'écran, vous avez le rôle de DEVINEUR.\nEssayez de deviner ce que le DESSINEUR dessine.\nVous pouvez réessayer jusqu'à l'obtention de la bonne réponse OU jusqu'à l'écoulement du temps alloué.",

            "Lorsque le temps est écoulé, le mot à deviner s'affiche. Il est possible de voter pour le dessin et de le sauvegarder.\nEnsuite, c'est le tour d'un autre joueur d'être le DESSINEUR.",

            "Fin de partie:\nUne manche est finie après que chaque joueur a été une fois le DESSINATEUR.\n La partie se termine après deux manches.\nLe gagnant est celui qui a amassé le plus de points!",
        };

        public Tutoriel()
        {
            InitializeComponent();

            ImageBorder0.PreviewMouseDown += (s, e) => SlideTo(0);
            ImageBorder1.PreviewMouseDown += (s, e) => SlideTo(1);
            ImageBorder2.PreviewMouseDown += (s, e) => SlideTo(2);
            ImageBorder3.PreviewMouseDown += (s, e) => SlideTo(3);
            ImageBorder4.PreviewMouseDown += (s, e) => SlideTo(4);
            ImageBorder5.PreviewMouseDown += (s, e) => SlideTo(5);
            ImageBorder6.PreviewMouseDown += (s, e) => SlideTo(6);
            ImageBorder7.PreviewMouseDown += (s, e) => SlideTo(7);
            ImageBorder8.PreviewMouseDown += (s, e) => SlideTo(8);
            ImageBorder9.PreviewMouseDown += (s, e) => SlideTo(9);
            ImageBorder10.PreviewMouseDown += (s, e) => SlideTo(10);
            ImageBorder11.PreviewMouseDown += (s, e) => SlideTo(11);
            ImageBorder12.PreviewMouseDown += (s, e) => SlideTo(12);
            ImageBorder13.PreviewMouseDown += (s, e) => SlideTo(13);
            ImageBorder14.PreviewMouseDown += (s, e) => SlideTo(14);
            ImageBorder15.PreviewMouseDown += (s, e) => SlideTo(15);
            ImageBorder16.PreviewMouseDown += (s, e) => SlideTo(16);
            ImageBorder17.PreviewMouseDown += (s, e) => SlideTo(17);
            ImageBorder18.PreviewMouseDown += (s, e) => SlideTo(18);
            ImageBorder19.PreviewMouseDown += (s, e) => SlideTo(19);
            ImageBorder20.PreviewMouseDown += (s, e) => SlideTo(20);
        }

        public void CloseTutorial(object sender, MouseEventArgs e)
        {
            SlideTo(0);
            Navigator.HideTutorial();
        }

        public void PreviousSlide(object sender, MouseEventArgs e)
        {
            var slide = currentSlide - 1;

            if (slide == -1)
            {
                slide = MAX_SLIDES;
            }

            SlideTo(slide);
        }

        public void NextSlide(object sender, MouseEventArgs e)
        {
            var slide = currentSlide + 1;

            if (slide == MAX_SLIDES + 1)
            {
                slide = 0;
            }

            SlideTo(slide);
        }

        public void SlideTo(int slide)
        {
            Dispatcher.Invoke(() =>
            {
                ((Border)FindName("ImageBorder" + currentSlide)).Background = (SolidColorBrush)new BrushConverter().ConvertFrom("Gray"); ;
                currentSlide = slide;
                Description.Text = descriptions[currentSlide];
                ((Border)FindName("ImageBorder" + currentSlide)).Background = (SolidColorBrush)new BrushConverter().ConvertFrom("RoyalBlue");
                ((Image)FindName("TutorialImage")).Source = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Tutorial/tutorial{currentSlide}.PNG"));
                ChangeTop();
            });
        }

        public void ChangeTop()
        {
            if (currentSlide == 0)
            {
                TopLabel.Content = "Bienvenue sur PolyPaint Pro!";
            } else if (currentSlide >= 1 && currentSlide <= 7)
            {
                TopLabel.Content = "Mode de jeu Classique";
            } else if (currentSlide >= 8 && currentSlide <= 13) {
                TopLabel.Content = "Mode de jeu Solo";
            } else
            {
                TopLabel.Content = "Mode de jeu FFA";
            }
        }

        //public void MoveSlide(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Right)
        //    {
        //        var slide = currentSlide + 1;

        //        if (slide == MAX_SLIDES + 1)
        //        {
        //            slide = 0;
        //        }

        //        SlideTo(slide);
        //    }
        //    else if (e.Key == Key.Left)
        //    {
        //        var slide = currentSlide - 1;

        //        if (slide == -1)
        //        {
        //            slide = MAX_SLIDES;
        //        }

        //        SlideTo(slide);
        //    }
        //}
    }
}
