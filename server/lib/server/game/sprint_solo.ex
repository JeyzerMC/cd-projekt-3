defmodule Server.Game.SprintSolo do
  alias Server.Structs.Game
  alias Server.Structs.User
  alias Server.Repo
  alias Server.Models.Word

  def validate_guess(game, word, username) do
    if(word == game.word) do
      game
      |> add_score(username)
      |> increment_consecutive_guesses(username)
    else
      game
      |> increment_wrong_guesses(username)
      |> increment_guesses()
      |> reset_consecutive_guesses(username)
    end
  end

  def add_score(game, username) do
    users =
      Enum.map(game.users, fn user ->
        if user.username == username do
          %User{user | score: user.score + 1}
        else
          user
        end
      end)

    Map.put(game, :users, users)
  end

  def increment_wrong_guesses(game, username) do
    users =
      Enum.map(game.users, fn user ->
        if user.username == username do
          %User{user | wrong_guesses: user.wrong_guesses + 1}
        else
          user
        end
      end)

    Map.put(game, :users, users)
  end

  def increment_guesses(game) do
    %Game{game | current_guesses: game.current_guesses + 1}
  end

  def change_turn(game) do
    game
    |> change_word()
    |> reset_guesses()
    |> set_word_max_guesses()
    |> set_word_time_bonus()
  end

  def change_word(game) do
    word =
      Repo.all(Word)
      |> Enum.random()

    %Game{game | word: word.name, word_difficulty: word.difficulty}
  end

  def set_word_max_guesses(game) do
    # case game.word_difficulty do
    #   "Facile" ->
    #     %Game{game | max_guesses: 3}

    #   "Moyen" ->
    #     %Game{game | max_guesses: 4}

    #   "Difficile" ->
    #     %Game{game | max_guesses: 5}
    # end
    %Game{game | max_guesses: 3}
  end

  def set_word_time_bonus(game) do
    case game.word_difficulty do
      "Facile" ->
        %Game{game | word_time_bonus: 3}

      "Moyen" ->
        %Game{game | word_time_bonus: 4}

      "Difficile" ->
        %Game{game | word_time_bonus: 5}
    end
  end

  def start_game(game) do
    %Game{game | started: true, game_start_time: :os.system_time(:seconds)}
  end

  def reset_guesses(game) do
    %Game{game | current_guesses: 0}
  end

  def increment_consecutive_guesses(game, username) do
    users =
      Enum.map(game.users, fn user ->
        if user.username == username do
          %User{
            user
            | consecutive_guesses: user.consecutive_guesses + 1,
              biggest_consecutive_guesses:
                max(user.consecutive_guesses + 1, user.biggest_consecutive_guesses)
          }
        else
          user
        end
      end)

    Map.put(game, :users, users)
  end

  def reset_consecutive_guesses(game, username) do
    users =
      Enum.map(game.users, fn user ->
        if user.username == username do
          %User{
            user
            | consecutive_guesses: 0
          }
        else
          user
        end
      end)

    Map.put(game, :users, users)
  end

  def end_game(game, username) do
    end_time = :os.system_time(:seconds)

    users =
      Enum.map(game.users, fn user ->
        if user.username == username do
          %User{
            user
            | avg_guess_time:
                if user.score != 0 do
                  (end_time - game.game_start_time - 3) / user.score
                else
                  0
                end
          }
        else
          user
        end
      end)

    Map.put(game, :users, users)
  end
end
