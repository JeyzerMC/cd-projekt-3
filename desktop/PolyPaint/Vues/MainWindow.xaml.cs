﻿using PolyPaint.Modeles;
using PolyPaint.Services;
using System.Windows;
using System.Windows.Media;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string PosLeft { get; set; }
        public string PosLeftAfter { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            BrushConverter bc = new BrushConverter();
            //borderChat.Background = (Brush)bc.ConvertFrom("#c78f3e");
            borderChat.Background = (Brush)bc.ConvertFrom("#002045");
            borderHeader.Background = (Brush)bc.ConvertFrom("#0D47A1");
            mainFrame.Background = (Brush)bc.ConvertFrom("#002045");
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            if (Navigator.GoBackButton.Content.ToString() == "Quitter") {
                SocketService.Socket.Disconnect();
                UserInformation.Token = null;
                Navigator.Navigate("Vues/LoginPage.xaml");
                Navigator.OnLogout();
            } else if (Navigator.GoBackButton.Content.ToString() == " Retour ") {
                Navigator.Navigate("Vues/HomePage.xaml");
            }
            else{
                Navigator.GoBack();
            }
        }

        private void ShowTutorial(object sender, RoutedEventArgs e)
        {
            Navigator.ShowTutorial();
        }
    }
}
