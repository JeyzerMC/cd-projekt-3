package com.example.android.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.android.R
import com.example.android.databinding.ActivityLoginBinding
import com.example.android.ui.fragments.RegisterDialog
import com.example.android.viewmodel.LoginViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@SuppressLint("ByteOrderMark")
class LoginActivity : AppCompatActivity() {
    private val model: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this,
            R.layout.activity_login
        )

        setupPermissions()
        binding.model = model
        binding.lifecycleOwner = this
    }

    fun onLoginButtonClick(view: View) {
        GlobalScope.launch {
            val errors = model.signInParams.value!!.validate()
            if (errors.isNotEmpty()) {
                toast(errors[0])

                return@launch
            }
            model.loading.postValue(true)
            when( val message = model.onUserLogin()) {

                "ok" -> {
                    toast("connection effectuée avec succes")
                    goToMainMenuPage()
                }
                else -> toast(message)
            }
            model.loading.postValue(false)
        }
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i("AAAAAAAAAAAAAAAAAAAAAAA", "Permission to record denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 5555)
    }

    override fun onRequestPermissionsResult(requestCode: Int,
        permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            5555 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user")
                } else {
                    Log.i("", "Permission has been granted by user")
                }
            }
        }
    }

    fun toast(message: String) {
        runOnUiThread {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(this, message, duration)
            toast.show()
        }
    }

    fun onRegisterButtonClick(view: View) {
        showRegisterDialog()
    }

    private fun goToMainMenuPage() {
        startActivity(Intent(this, MainMenuActivity::class.java))
    }

    private fun showRegisterDialog() {
        RegisterDialog().show(supportFragmentManager, "marcus")
    }

}
