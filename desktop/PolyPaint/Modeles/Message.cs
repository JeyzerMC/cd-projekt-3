﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyPaint.Modeles
{
    public class Message
    {
        //Takes timestamp and sender and wraps a format around
        public Message(string rawSender, string messageContent, string rawTimestamp)
        {
            this.Sender = rawSender + ": ";
            this.MessageContent = messageContent;
            this.Timestamp = "[" + rawTimestamp + "] ";
        }

        //Takes only a message string (for admin purposes)
        public Message(string messageContent)
        {
            this.MessageContent = messageContent;
        }

        public string Timestamp { get; }

        public string Sender { get; }

        public string MessageContent { get; }
    }
}
