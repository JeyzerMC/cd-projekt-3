﻿using System.Net.Http;

namespace PolyPaint.Services
{
    public static class HttpService
    {
        // HttpClient for the communication with the API
        public readonly static HttpClient Client = new HttpClient();

        public readonly static string DefaultUser = "jeyzer";

        // Base shared endpoint between the socket and API
        public readonly static string _coreEndpoint = "kai26.herokuapp.com";

        // Endpoint for the server API 
        public readonly static string ApiEndpoint = "https://" + _coreEndpoint + "/api";

        // Endpoint for the socket client
        public readonly static string SocketEndpoint = "wss://" + _coreEndpoint + "/socket";
    }
}
