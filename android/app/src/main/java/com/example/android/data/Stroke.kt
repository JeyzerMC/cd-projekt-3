package com.example.android.data

import com.example.android.viewmodel.PaintBrushStyle

data class Stroke(val color: Int, val width: Double, val style: PaintBrushStyle ,val start: Point, val end: Point)