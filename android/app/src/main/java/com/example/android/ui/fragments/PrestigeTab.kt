package com.example.android.ui.fragments

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.android.R
import com.example.android.databinding.FragmentPrestigeTabBinding
import com.example.android.viewmodel.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_prestige_tab.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

val prestige: List<String> = listOf(
    "sdfasdf",
    "sdfasdf",
    "sdfasdf",
    "sdfasdf",
    "sdfasdf"
)

class PrestigeTab : Fragment() {
    val model: ProfileViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentPrestigeTabBinding.inflate(layoutInflater, container, false)

        binding.model = model
        binding.lifecycleOwner = this

        model.profile.observe(this, Observer { profile ->
            updatePrestige(profile.exp_points, profile.current_avatar)
        })
        return binding.root
    }

    fun updatePrestige(exp_points: Int, current_avatar: Int) {
        val level = exp_points / 10

        val expBar=  activity!!.findViewById<ProgressBar>(R.id.expBar)
        expBar.progress = exp_points

        val avatarRadioGroup=  activity!!.findViewById<RadioGroup>(R.id.avatarRadioGroup)
        avatarRadioGroup.removeAllViews()
        prestige.mapIndexed { index, avatar ->
            val radioButton = RadioButton(activity)
            radioButton.id = index

            val blue = ResourcesCompat.getColor(resources, R.color.bluePrimary, null)
            val colorStateList = ColorStateList(
                arrayOf(intArrayOf(-android.R.attr.state_enabled),
                    intArrayOf(android.R.attr.state_enabled)),intArrayOf(Color.BLACK, blue)
            )

            radioButton.buttonTintList = colorStateList
            radioButton.setTextColor(blue)

            val bmp = BitmapFactory.decodeResource(resources, resources.getIdentifier("avatar_$index", "drawable", activity!!.packageName))
            val bmpScaled = Bitmap.createScaledBitmap(bmp, 50, 50, false)
            val avatar =  BitmapDrawable(resources, bmpScaled)
            radioButton.setOnClickListener(this::onTitleRadioButtonClick)

            if (index > level) {
                radioButton.isEnabled = false
            }

            if (index == current_avatar) {
                radioButton.isChecked = true
            }

            radioButton.setCompoundDrawablesWithIntrinsicBounds(null, avatar, null, null);
            avatarRadioGroup.addView(radioButton)
        }
    }

    fun onTitleRadioButtonClick(view: View) {
        val prestigeRadioGroup=  activity!!.findViewById<RadioGroup>(R.id.avatarRadioGroup)

        GlobalScope.launch {
            val (profile, error) = model.updateAvatar(prestigeRadioGroup.checkedRadioButtonId)

            if (profile != null) {
                toast("avatar mis a jour")
                model.profile.postValue(profile)
            }

            if (error != null) {
                toast("erreur durant la mise a jour de l'avatar")
            }
        }
    }

    private fun toast(message: String) {
        activity!!.runOnUiThread {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(activity!!, message, duration)
            toast.show()
        }
    }
}
