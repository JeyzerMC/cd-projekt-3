package com.example.android.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceGroup
import android.preference.PreferenceManager
import com.example.android.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val token= PreferenceManager.getDefaultSharedPreferences(applicationContext).getString("token", "")
        val page = when (token) {
           "" -> LoginActivity::class
           else -> LobbyActivity::class
        }
        startActivity(Intent(applicationContext, page.java))
    }
}
