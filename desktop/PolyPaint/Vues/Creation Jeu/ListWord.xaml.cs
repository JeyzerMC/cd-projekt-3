﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PolyPaint.Modeles;
using PolyPaint.Services;
using PolyPaint.VueModeles;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class ListWord : Page
    {
        private List<Word> list;
        public ListWord()
        {
            InitializeComponent();
            list = new List<Word>();
            LLS.ItemsSource = list;

            WordCreationService.FetchWords().ContinueWith(resp =>
            {
                Dispatcher.Invoke(async () =>
                {
                    list = await resp;
                    LLS.ItemsSource = list;
                    LLS.Items.Refresh();
                });
            });
            Navigator.GoBackButton.Content = " Retour ";
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            Word mot = (sender as Button).DataContext as Word;

            WordCreationService.DeleteWord(mot).ContinueWith(ok =>
            {
                WordCreationService.FetchWords().ContinueWith(resp =>
                {
                    Dispatcher.Invoke(async () =>
                    {
                        list = await resp;
                        LLS.ItemsSource = list;
                        LLS.Items.Refresh();
                    });
                });
            });
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Vues/HomePage.xaml");
        }

        private void CreateWord(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate(new CreateWord(list));
        }

        private void ShowWord_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            string name = (sender as TextBlock).Text;
            Word word= list.Find(n => n.name == name);
            ShowWord page = new ShowWord(word);
            var textblock = sender as TextBlock;
            textblock.IsEnabled = false;
            Dispatcher.Invoke(() =>
            {
                Navigator.Navigate(page);
                e.Handled = true;
            });

        }
    }
}
