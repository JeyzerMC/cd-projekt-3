package com.example.android.viewmodel

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.DisplayMetrics
import androidx.lifecycle.ViewModel
import com.example.android.data.ChatRepository
import com.example.android.data.FingerPath
import com.example.android.data.Point
import com.example.android.data.Stroke
import kotlin.math.abs

enum class PaintBrushStyle(val style: String ) {
    SQUARE("square"), ROUND("round")
}

class PaintViewModel: ViewModel() {
    private var currentBrushStyle: PaintBrushStyle? = null
    private var oldPoint: Point = Point(0f,0f)
    var canvasHeight: Int = 0
    var canvasWidth: Int = 0

    private var chatRepository = ChatRepository.getInstance()
    private var isErasing = false
    private val tolerance = 4f

    private var currentX: Float = 0f
    private var currentY: Float = 0f

    private var currentPath: Path? = null
    private val paths : MutableList<FingerPath> = mutableListOf()


    private var oldColor: Int = 0
    private var currentColor: Int = 0
    private var currentWidth: Int = 0

    private var paint: Paint = Paint()
    private var bitmap: Bitmap? = null
    private var canvas: Canvas? = null

    private fun onStrokeSend(stroke: Stroke) {
        chatRepository.onStrokeSend(stroke)
    }

    private fun initPaint() {
        paint = Paint()
        paint.isAntiAlias = true
        paint.isDither = true
        paint.color = Color.BLUE
        paint.style = Paint.Style.STROKE
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeCap = Paint.Cap.ROUND
        paint.xfermode = null
        paint.alpha = Color.WHITE
    }

    fun initCanvas(metrics: DisplayMetrics, context: Context){
        initPaint()

        val height = (metrics.heightPixels * 0.7).toInt()
        val width = height

        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        canvas = Canvas(bitmap)
        canvasWidth = width
        canvasHeight = height
        currentWidth = 20
        currentColor = paint.color
        currentBrushStyle = PaintBrushStyle.ROUND
        oldColor = currentColor

    }

    fun draw(canvas: Canvas){
        canvas.save()
        this.canvas?.drawColor(Color.WHITE)
        synchronized(paths) {
            for (fp in paths) {
                paint.color = fp.color
                paint.strokeWidth = fp.strokeWidth.toFloat()
                paint.maskFilter = null
                paint.strokeCap = when (fp.brushStyle) {
                    PaintBrushStyle.SQUARE -> Paint.Cap.SQUARE
                    PaintBrushStyle.ROUND -> Paint.Cap.ROUND
                }

                this.canvas?.drawPath(fp.path, paint)
            }
        }
        canvas.drawBitmap(bitmap, 0f , 0f , paint)

        canvas.restore()
    }

    fun startPath(x: Float, y: Float) {
        currentPath = Path()
        val fp = FingerPath(currentColor, currentWidth, currentPath!!, currentBrushStyle!!)

        paths.add(fp)

        currentPath!!.reset()
        currentPath!!.moveTo(x, y)
        currentX = x
        currentY = y
    }

    fun addToPath(x: Float, y: Float) {
        val dx = abs(x - currentX)
        val dy = abs(y - currentY)

        if (dx >= tolerance || dy >= tolerance) {
            currentPath?.lineTo(currentX, currentY)
            val stroke = Stroke(currentColor, currentWidth / canvasWidth.toDouble(), currentBrushStyle!!, Point(currentX / canvasWidth, currentY / canvasHeight), Point(x / canvasWidth, y / canvasHeight))

            onStrokeSend(stroke)
            oldPoint = Point(currentX, currentY)

            currentX = x
            currentY = y
        }
    }

    fun endPath() {
        currentPath?.lineTo(currentX, currentY)
        onStrokeSend(Stroke(currentColor, currentWidth / canvasWidth.toDouble(), currentBrushStyle!!, Point(oldPoint.x / canvasWidth, oldPoint.y / canvasHeight), Point(currentX / canvasWidth, currentY / canvasHeight)))
    }

    fun initStroke(stroke: Stroke, fp: FingerPath) {
        paths.add(fp)
        fp.path.moveTo(stroke.start.x, stroke.start.y)

    }

    fun checkStrokeTolerance(stroke : Stroke): Boolean {
        val dx = abs(stroke.end.x - stroke.start.x)
        val dy = abs(stroke.end.y - stroke.start.y)

        return (dx >= tolerance || dy >= tolerance)
    }

    fun clearCanvas() {
        paths.clear()
    }

    fun changeSize(size: Int) {
        currentWidth = size
    }

    fun toggleEraser() {
        isErasing = !isErasing
        currentColor = if(isErasing) Color.WHITE else oldColor
    }

    fun changeColor(color: Int) {
        isErasing = false
        currentColor = color
        oldColor = color
    }

    fun changeBrushStyle(style: PaintBrushStyle) {
        currentBrushStyle = style
    }
}