defmodule Server.Game.FfaGame do
  alias Server.Structs.Game
  alias Server.Structs.User
  alias Server.Game.BaseGame
  alias Server.Repo
  alias Server.Models.User, as: UserModel

  alias Server.Accounts

  @random_words ["apple", "patato", "anime", "test", "pear", "poly", "something"]

  def validate_guess(game, word, username) do
    # TODO: CHECK IF CHANGE TURN
    cond do
      game.word == word ->
        IO.puts("CORRECT WORD")

        game
        |> add_score(username)
        |> increment_correct_guesses()
        |> check_if_change_turn()

      true ->
        game
        |> increment_guesses_for_user(username)
    end
  end

  def change_turn(game) do
    # TODO: CHECK IF END GAME
    game = check_if_end_game(game)

    if game.end_game do
      game
    else
      game =
        game
        |> change_drawer()
        |> change_word()
        |> apply_votes()
        |> reset_guesses()
        |> reset_round_start_time()
        |> reset_change_turn()

      # IO.puts("AFTER CHANGING TURN==================")
      # IO.inspect(game)
      # IO.puts("AFTER CHANGING TURN==================")

      game
    end
  end

  def add_score(game, username) do
    score = determine_score(game, username)

    users =
      Enum.map(game.users, fn user ->
        if user.username == username do
          %User{user | score: user.score + score}
        else
          user
        end
      end)

    Map.put(game, :users, users)
  end

  def determine_score(game, username) do
    # TODO: determine score to give
    user =
      Enum.find(game.users, fn user ->
        user.username == username
      end)

    score = 100 - (:os.system_time(:seconds) - game.round_start_time) - user.number_guesses * 5

    if score > 0 do
      score
    else
      0
    end
  end

  def apply_votes(game) do
    current_drawer = Repo.get_by(UserModel, username: game.current_drawer.username)

    Accounts.update_user(current_drawer, %{
      exp_points:
        current_drawer.exp_points +
          if game.vote > 0 do
            game.vote * 2
          else
            0
          end
    })

    %Game{game | vote: 0}
  end

  def increment_guesses_for_user(game, username) do
    users =
      Enum.map(game.users, fn user ->
        if username == user.username do
          %User{
            user
            | number_guesses: user.number_guesses + 1,
              wrong_guesses: user.wrong_guesses + 1
          }
        else
          user
        end
      end)

    %Game{game | users: users}
  end

  def increment_correct_guesses(game) do
    %Game{game | correct_guesses: game.correct_guesses + 1}
  end

  def start_game(game) do
    %Game{game | open: false, started: true, game_start_time: :os.system_time(:seconds)}
  end

  def change_drawer(game) do
    already_drawn =
      Enum.map(game.already_drawn, fn user ->
        user.username
      end)

    not_drawn_users =
      Enum.filter(game.users, fn user ->
        !Enum.member?(already_drawn, user.username)
      end)

    IO.puts("CHANGING DRAWER")
    IO.puts("NOT DRAWN USERS")
    IO.inspect(not_drawn_users)
    IO.puts("ALREADY DRAWN")
    IO.inspect(game.already_drawn)

    new_drawer = Enum.random(not_drawn_users)

    %Game{game | current_drawer: new_drawer, already_drawn: game.already_drawn ++ [new_drawer]}
  end

  def change_word(game) do
    new_word = BaseGame.get_random_word()

    if new_word == game.word do
      change_word(game)
    else
      Map.put(game, :word, new_word)
    end
  end

  def reset_round_start_time(game) do
    %Game{game | round_start_time: :os.system_time(:seconds)}
  end

  def reset_guesses(game) do
    new_users =
      Enum.map(game.users, fn user ->
        %User{user | number_guesses: 0}
      end)

    %Game{game | correct_guesses: 0, users: new_users}
  end

  def reset_change_turn(game) do
    %Game{game | change_turn: false}
  end

  def check_if_change_turn(game) do
    if game.correct_guesses == game.nb_players - 1 do
      %{game | change_turn: true}
    else
      game
    end
  end

  def check_if_end_game(game) do
    if Enum.count(game.already_drawn) == game.nb_players do
      if game.first_round_ffa do
        %{game | end_game: true}
      else
        %{game | first_round_ffa: true, already_drawn: []}
      end
    else
      game
    end
  end

  def get_random_word() do
    # words =
    #   File.stream!(:code.priv_dir("dico.txt"))
    #   |> Stream.map(&String.trim_trailing/1)
    #   |> Enum.to_list()

    Enum.random(@random_words)
  end
end
