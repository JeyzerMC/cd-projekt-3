﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Phoenix;
using PolyPaint.Services;
using System.Diagnostics;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Interaction logic for TestPage.xaml
    /// </summary>
    public partial class TestPage : Page
    {
        private Channel channel1;
        private Channel channel2;
        private Channel channel3;

        private string roomId = "";
        public TestPage()
        {
            InitializeComponent();
            channel1 = SocketService.Socket.MakeChannel("lobby:selection");
            channel1.On("lobbies_update", m => {
                Dictionary<string, object> payload = m.payload.ToObject<Dictionary<string, object>>();
                if(payload.Count != 0)
                {
                roomId = payload.Keys.First();

                }
                Trace.WriteLine("11111================================");
                Trace.WriteLine(roomId);
                Trace.WriteLine("======================================");
            });
            channel1.Join();

            //channel2 = SocketService.Socket.MakeChannel("game:3456");
            //channel2.On("draw", m =>
            //{
            //    Trace.WriteLine("2222222================================");
            //    Trace.WriteLine(m.payload);
            //    Trace.WriteLine("======================================");
            //});
            //channel2.Join();

        }

        private void Join_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("id", roomId);
            channel1.Push("join_lobby", payload);
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            channel1.Push("create_lobby");
        }

        private void Game_Click(object sender, RoutedEventArgs e)
        {
            channel2 = SocketService.Socket.MakeChannel("game:" + roomId);
            channel2.On("start_game", m =>
            {
                Trace.WriteLine("2222222================================");
                Trace.WriteLine("start game");
                Trace.WriteLine("======================================");
            });
            channel2.On("change_turn", m =>
            {
                Trace.WriteLine("2222222================================");
                Trace.WriteLine(m.payload);
                Trace.WriteLine("======================================");
            });
            channel2.On("draw", m =>
            {
                Trace.WriteLine("2222222================================");
                Trace.WriteLine("draw");
                Trace.WriteLine("======================================");
            });
            channel2.Join();
        }

        private void Test_Click(object sender, RoutedEventArgs e)
        {
            channel3 = SocketService.Socket.MakeChannel("game:" + "test");
            channel3.On("start_game", m =>
            {
                Trace.WriteLine("2222222================================");
                Trace.WriteLine("start game");
                Trace.WriteLine("======================================");
            });
            channel3.On("change_turn", m =>
            {
                Trace.WriteLine("2222222================================");
                Trace.WriteLine(m.payload);
                Trace.WriteLine("======================================");
            });
            channel3.On("draw", m =>
            {
                Trace.WriteLine("2222222================================");
                Trace.WriteLine("draw");
                Trace.WriteLine("======================================");
            });
            channel3.Join();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            channel2.Push("start_game");
        }

        private void Change_Click(object sender, RoutedEventArgs e)
        {
            channel2.Push("guess");
        }

        private void Testing_Click(object sender, RoutedEventArgs e)
        {
            channel3.Push("guess");
        }

        private void Draw_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("message", "test");
            channel2.Push("draw", payload);
        }

        //private void Test_Click(object sender, RoutedEventArgs e)
        //{
        //    Dictionary<string, object> payload = new Dictionary<string, object>();
        //    payload.Add("message", "test");
        //    channel1.Push("draw", payload);

        //    Dictionary<string, object> payload2 = new Dictionary<string, object>();
        //    payload2.Add("message", "test2");
        //    channel2.Push("draw", payload2);
        //}
    }
}
