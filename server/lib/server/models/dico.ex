defmodule Server.Models.Dico do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, except: [:__meta__]}

  schema "dico" do
    field :name, :string
  end

  @doc false
  def changeset(dico, attrs) do
    dico
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
