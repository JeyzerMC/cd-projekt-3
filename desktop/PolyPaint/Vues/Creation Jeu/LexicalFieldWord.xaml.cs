﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PolyPaint.Modeles;
using PolyPaint.Services;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class LexicalFieldWord : Page
    {

        List<string> indices = new List<string>();
        public LexicalFieldWord()
        {
            InitializeComponent();
        }

        private Word word;

        public LexicalFieldWord(Word newWord)
        {
            InitializeComponent();
            word = newWord;
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            Navigator.GoBack();
        }

        private void indice_keyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                AddTip(sender, e);
                e.Handled = true;
            }
        }

        private void AddTip(object sender, RoutedEventArgs e)
        {
            if(Indice.Text != "")
            {
                errorMessage.Visibility = Visibility.Collapsed;
                indices.Add(Indice.Text);
                Indices.ItemsSource = indices;
                Indices.Items.Refresh();
                Indice.Text = "";
                Indice.Focus();
            }
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            string indice = (sender as Button).DataContext as string;
            indices.RemoveAll(c => c == indice);
            Indices.ItemsSource = indices;
            Indices.Items.Refresh();
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            if (indices.Count != 0)
            {
                word.tips = indices;

                Next.IsEnabled = false;

                WordCreationService.AddWord(word).ContinueWith(resp =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        Navigator.Navigate(new ListWord());
                    });
                });
            }
            else {
                errorMessage.Visibility = Visibility.Visible;
            }
        }
    }
}
