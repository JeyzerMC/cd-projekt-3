﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using PolyPaint.Modeles;
using PolyPaint.Utilitaires;
using PolyPaint.Services;
using Phoenix;
using Newtonsoft.Json;
using System.Windows.Data;
using PolyPaint.Services.Channels;
using System.Windows;

namespace PolyPaint.VueModeles
{
    class LobbySprintSoloViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Lobby> LobbyList { get; }

        private object _lobbyListLock = new object();
        private Lobby _selectedLobby { get; set; }
        public RelayCommand<object> JoinLobbyClick { get; set; }
        public RelayCommand<object> CreateLobbyClick { get; set; }
        public Lobby SelectedLobby
        {
            get { return _selectedLobby; }
            set {
                _selectedLobby = value;
                if (_selectedLobby != null) //if a lobby is selected
                {
                    JoinGroupVisibility = Visibility.Visible;
                }
                else
                    JoinGroupVisibility = Visibility.Hidden;
                NotifyPropertyChanged();
            }
        }

        public Visibility JoinGroupVisibility
        {
            get { return _joinGroupVisibility; }
            set
            {
                _joinGroupVisibility = value;
                HideGroupVisibility = value;
                NotifyPropertyChanged();
            }
        }

        public Visibility _joinGroupVisibility { get; set; }

        public Visibility HideGroupVisibility
        {
            get
            {
                if (_joinGroupVisibility == Visibility.Visible)
                {
                    return Visibility.Hidden;
                }
                else
                {
                    return Visibility.Visible;
                }
            }
            set
            {
                NotifyPropertyChanged();
            }
        }

        public bool CreateGroupEnabled
        {
            get { return _createGroupEnabled; }
            set
            {
                _createGroupEnabled = value;
                NotifyPropertyChanged();
            }
        }
        public bool _createGroupEnabled { get; set; }

        public LobbySprintSoloViewModel()
        {
            this.LobbyList = new ObservableCollection<Lobby>();
            CreateLobbyClick = new RelayCommand<object>(CreateLobby);
            BindingOperations.EnableCollectionSynchronization(LobbyList, _lobbyListLock);
            CreateGroupEnabled = true;
            JoinGroupVisibility = Visibility.Hidden;
            JoinSocketChannel();
        }

        private void JoinSocketChannel()
        {
            LobbyChannelService.JoinLobbyChannel("lobby:sprint_solo");
            LobbyChannelService.lobbyChannel.On("lobbies_update", m => {
                Trace.WriteLine("lobbies_update");
                lock (_lobbyListLock)
                    LobbyList.Clear();
                if (m.payload.Count > 0) //if the message-list of active lobbies is not empty
                {
                    Dictionary<string, Lobby> lobbyList = JsonConvert.DeserializeObject<Dictionary<string, Lobby>>(m.payload.ToString());
                    foreach (KeyValuePair<string, Lobby> kvp in lobbyList)
                        lock (_lobbyListLock)
                            LobbyList.Add(new Lobby(kvp.Key.ToString(), kvp.Value.users.Count, kvp.Value.users, kvp.Value.open));
                }
            });
        }

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void CreateLobby(object o)
        {
            CreateGroupEnabled = false;
            LobbyChannelService.lobbyChannel.Push("create_lobby").Receive(new Reply.Status(), reply => {
                Phoenix.Reply message = (Phoenix.Reply)reply;
                string lobbyId = message.response["id"].ToString();
                Trace.WriteLine("Create lobby: " + lobbyId);
                LobbyChannelService.SubscribeGetAvatars(lobbyId);
                CreateGroupEnabled = true;
                System.Windows.Application.Current.Dispatcher.Invoke(() => NavigateToGame(lobbyId, true));
            });
        }

        private void NavigateToGame(string id, bool isCreator)
        {
            Navigator.Navigate("Vues/GamePageSprintSolo.xaml", new Dictionary<string, object>() { { "id", id }, { "isCreator", isCreator } });
        }
    }
}
