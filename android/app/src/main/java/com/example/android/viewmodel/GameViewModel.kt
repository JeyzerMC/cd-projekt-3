package com.example.android.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.android.data.ChatRepository
import com.example.android.data.GameMode
import com.example.android.data.GameState
import java.math.RoundingMode
import java.text.DecimalFormat

class GameViewModel: ViewModel(){
    val repository = ChatRepository.getInstance()

    val gameMode = repository.gameMode

    val isCreator: MutableLiveData<Boolean> = MutableLiveData()

    val gameState = repository.gameState
    val newStroke = repository.newStroke

    var lobbies = repository.lobbies

    val lobbyId: MutableLiveData<String> = MutableLiveData()

    val currentLobby = repository.currentLobby
    val currentWord = repository.currentWord
    val df = DecimalFormat("#.#")
    val remainingTime = Transformations.map(repository.remainingTime) { a ->
       df.format(a)
    }

    // TODO: integrate all of these into the gameState object and use transformations
    val canAddAI: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }
    val canVote: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
    val canStartGame: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply{ value = false }
    val endGameStats = repository.endGameStats

    val username = repository.username
    val goodGuess = repository.goodGuess
    init {
        reset()
        df.roundingMode = RoundingMode.HALF_DOWN
    }

    suspend fun joinGameChannel() = repository.joinGameChannel(gameMode.value!!, lobbyId.value!!)

    fun reset() {
        lobbyId.value = ""
        // TODO: check for better initial values

        currentLobby.value = Lobby("", "", GameMode.CLASSIC, true, 0, mapOf(), null, null)
        currentWord.value = ""
        repository.remainingTime.value = 0.0
        repository.remainingTimeTimer?.cancel()
        repository.goodGuess.value = true

        gameState.value = GameState.WAIT
    }

    fun leaveLobby() {
        repository.leaveLobby(lobbyId.value!!)
        reset()
    }

    fun startGame() = repository.startGame(lobbyId.value!!)
    fun sendGuess(text: String) = repository.onGuessSend(text)
    fun sendVote(rating: Int) {
        canVote.value = false
        repository.vote(rating)
    }
    fun addAI() = repository.onAddAI()

}


/**
 * Sets the value to the result of a function that is called when both `LiveData`s have data
 * or when they receive updates after that.
 */
fun <T, A, B> LiveData<A>.combineAndCompute(other: LiveData<B>, onChange: (A, B) -> T): MediatorLiveData<T> {

    var source1emitted = false
    var source2emitted = false

    val result = MediatorLiveData<T>()

    val mergeF = {
        val source1Value = this.value
        val source2Value = other.value

        if (source1emitted && source2emitted) {
            result.value = onChange.invoke(source1Value!!, source2Value!! )
        }
    }

    result.addSource(this) { source1emitted = true; mergeF.invoke() }
    result.addSource(other) { source2emitted = true; mergeF.invoke() }

    return result
}