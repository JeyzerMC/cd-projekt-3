package com.example.android.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.data.ChatRepository
import com.example.android.data.RegisterInfo

class RegisterViewModel: ViewModel() {
    private val repository = ChatRepository.getInstance()
    val registerInfo: MutableLiveData<RegisterInfo> = repository.registerInfo


    suspend fun signUp(): String = repository.signUp()

}