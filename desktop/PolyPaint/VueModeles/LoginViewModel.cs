﻿using PolyPaint.Modeles;
using PolyPaint.Services;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PolyPaint.VueModeles
{
    class LoginViewModel
    {
        
        public async Task LoginUser(string username, string password)
        {
            Dictionary<string, string> userInfo = new Dictionary<string, string>
            {
                { "username", username },
                { "password", password },
            };

            string jwt = await UserHttpService.UserSignin(userInfo);

            UserInformation.Token = jwt;
            HttpService.Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt);
        }

        public async void ConnectToSocket()
        {
            SocketService.InitSocket();

            await Task.Run(() => {
                SocketService.Connect(UserInformation.Token);
            });
            Trace.WriteLine("After Task Run LoginViewModel.");
        }
    }
}
