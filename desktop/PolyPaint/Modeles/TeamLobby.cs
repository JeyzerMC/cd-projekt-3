﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyPaint.Modeles
{
    class TeamLobby
    {
        public TeamLobby(string id, int status, List<Player> playersTeam1, List<Player> playersTeam2, bool open)
        {
            this.Id = id;
            this.Nb_players = status;
            this.team1 = playersTeam1;
            this.team2 = playersTeam2;
            this.open = open;
        }

        public string Id { get; set; }
        public int Nb_players { get; set; }
        public List<Player> team1 { get; set; }
        public List<Player> team2 { get; set; }
        public bool open { get; set; }
    }
}
