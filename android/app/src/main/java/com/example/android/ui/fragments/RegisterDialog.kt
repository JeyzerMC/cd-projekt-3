package com.example.android.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.android.R
import com.example.android.data.GameMode
import com.example.android.viewmodel.RegisterViewModel
import com.example.android.databinding.FragmentRegisterDialogBinding
import com.example.android.ui.LobbyActivity
import com.example.android.ui.MainMenuActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch


class RegisterDialog : androidx.fragment.app.DialogFragment() {
    private val model: RegisterViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentRegisterDialogBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.model = model
        binding.lifecycleOwner = this

        val submitButton: Button = view.findViewById(R.id.submitButton)

        submitButton.setOnClickListener {
            GlobalScope.launch {
                try {
                    onSignUp()
                } catch(exception: Exception) {
                    toast(exception.message!!)
                }
            }
        }
        
        return view
    }

    private suspend fun onSignUp() {
        val errors = model.registerInfo.value!!.validate()
        if (errors.isNotEmpty()) {
            throw java.lang.Exception(errors[0])
        }

        coroutineScope {
            launch {
                when (val message = model.signUp()) {
                   "ok" -> goToMainMenuPage()
                   else -> toast(message)
                }
            }
        }
    }

    private fun toast(message: String) {
       activity!!.runOnUiThread {
           val duration = Toast.LENGTH_SHORT
           val toast = Toast.makeText(activity!!, message, duration)
           toast.show()
       }
    }

    private fun goToMainMenuPage() {
        val intent = Intent(activity!!.applicationContext, MainMenuActivity::class.java)
        intent.putExtra(MainMenuActivity.paramLogin, true)

        startActivity(intent)
    }

}
