package com.example.android.services

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.android.utilities.MyCustomApplication

class SharedPreferencesService {
    val preferences: SharedPreferences by lazy{
        PreferenceManager.getDefaultSharedPreferences( MyCustomApplication.appGlobalContext)
    }

    companion object {
        @Volatile private var instance: SharedPreferencesService? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: SharedPreferencesService().also { instance = it }
            }
    }

    fun get(key: String): String {
        return preferences.getString(key, "")!!
    }

    fun set(key: String, value: String) {
        preferences.edit().putString(key, value).apply()
    }

    fun remove(key: String) {
        preferences.edit().remove(key).apply()
    }
}