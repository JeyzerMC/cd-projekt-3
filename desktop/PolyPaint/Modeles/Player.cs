﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyPaint.Modeles
{
    public class Player
    {
        public int avatar { get; set; }
        public int title { get; set; }
        public string username { get; set; }
        public int score { get; set; }
        public string type { get; set; }
        public int wrong_guesses { get; set; }
        public int correct_replies { get; set; }
        public int biggest_consecutive_guesses { get; set; }
        public float avg_guess_time { get; set; }
    }
}
