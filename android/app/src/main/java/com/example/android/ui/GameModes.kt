package com.example.android.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.android.R
import com.example.android.data.GameMode

class GameModes : androidx.fragment.app.FragmentActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_modes)
    }

    fun onClassicModeButtonClick(view: View) {
        val intent = Intent(this, LobbyActivity::class.java)
        intent.putExtra(LobbyActivity.paramGameMode, GameMode.CLASSIC)

        startActivity(intent)
    }

    fun onSprintSoloModeButtonClick(view: View) {
        // TODO: create lobby automatically and go into game view page
        val intent = Intent(this, LobbyActivity::class.java)
        intent.putExtra(LobbyActivity.paramGameMode, GameMode.SOLOSPRINT)

        startActivity(intent)
    }

    fun onFfaModeButtonClick(view: View) {
        val intent = Intent(this, LobbyActivity::class.java)
        intent.putExtra(LobbyActivity.paramGameMode, GameMode.FFA)

        startActivity(intent)
    }
}
