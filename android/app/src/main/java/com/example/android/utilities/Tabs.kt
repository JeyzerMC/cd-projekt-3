package com.example.android.utilities

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

class TabsAdapter(fragmentManager: FragmentManager, val tabList: List<Pair<String, KClass<out Fragment>>>): FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment = tabList[position].second .createInstance()

    override fun getCount(): Int = tabList.count()
}


fun linkTabViewPager(supportFragmentManager: FragmentManager, tabLayout: TabLayout, viewPager: ViewPager, tabList: List<Pair<String, KClass<out Fragment>>>) {
    tabList.forEach {
        tabLayout.addTab(tabLayout.newTab().setText(it.first))
    }

    viewPager.adapter = TabsAdapter(supportFragmentManager, tabList )
    viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

    tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            viewPager.currentItem = tab!!.position
        }
    })
}
