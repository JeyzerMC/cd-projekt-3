package com.example.android.ui.views

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import com.example.android.data.FingerPath
import com.example.android.data.Point
import com.example.android.data.Stroke
import com.example.android.viewmodel.PaintBrushStyle
import com.example.android.viewmodel.PaintViewModel
import java.io.ByteArrayOutputStream

class PaintView : View {
    private var isActive: Boolean = false
    private val viewModel = PaintViewModel()

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?): super(context, attrs)

    fun init(metrics: DisplayMetrics) {
        viewModel.initCanvas(metrics, context)
    }

    override fun onDraw(canvas: Canvas) {
        viewModel.draw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isActive)
            return false

        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                viewModel.startPath(x, y)
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                viewModel.addToPath(x, y)
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                viewModel.endPath()
                invalidate()
            }
        }

        return true
    }

    fun getDrawableCanvas(): Drawable {
        val baos: ByteArrayOutputStream = ByteArrayOutputStream()
        val bitmap: Bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas: Canvas  = Canvas(bitmap)
        draw(canvas)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        // This converts the bitmap to a drawable
        return BitmapDrawable(getResources(),bitmap);
    }

    fun onChangeColor(color: Int) {
        viewModel.changeColor(color)
    }

    fun onToggleEraser(){
        viewModel.toggleEraser()
    }

    fun onChangeSize(size: Int) {
        viewModel.changeSize(size)
    }

    fun onClearCanvas() {
        viewModel.clearCanvas()
        invalidate()
    }

    fun drawOneStroke(stroke: Stroke) {
        val width = viewModel.canvasWidth
        val height = viewModel.canvasWidth
        val newStroke = Stroke(
            stroke.color,
            stroke.width,
            stroke.style,
            Point(stroke.start.x * width, stroke.start.y * height),
            Point(stroke.end.x * width, stroke.end.y * height)
        )

            var fp = FingerPath(stroke.color, (stroke.width * width).toInt(), Path(), stroke.style)
            viewModel.initStroke(newStroke, fp)
            // if (viewModel.checkStrokeTolerance(newStroke)) {
                fp.path.lineTo(newStroke.end.x, newStroke.end.y)
                invalidate()
            // }
    }

    fun setActivePaintView(active: Boolean) {
        isActive = active
    }

    fun onChangeBrushStyle(style: PaintBrushStyle) {
        viewModel.changeBrushStyle(style)
    }
}

