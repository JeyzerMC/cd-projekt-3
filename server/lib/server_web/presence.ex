defmodule ServerWeb.Presence do
  use Phoenix.Presence,
    otp_app: :server,
    pubsub_server: Server.PubSub

  def get_users() do
    presence_status = list("chat:general")
    pres = presence_status["users"]

    if pres != nil do
      users = Enum.map(pres.metas, fn x -> x.username end)
      {:ok, %{users: users}}
    else
      {:ok, %{users: []}}
    end
  end

  def verify_connected(username) do
    {:ok, users} = get_users()

    if Enum.member?(users.users, username) do
      {:error, :already_connected}
    else
      {:ok, username}
    end
  end
end
