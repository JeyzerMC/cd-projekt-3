defmodule ServerWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use ServerWeb, :controller

  # TODO: RM this function if unused
  # def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
  #   conn
  #   |> put_status(:unprocessable_entity)
  #   |> put_view(ServerWeb.ChangesetView)
  #   |> render("error.json", changeset: changeset)
  # end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(ServerWeb.ErrorView)
    |> render(:"404")
  end

  def call(conn, {:error, :unauthorized}) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "Generic login error"})
  end

  def call(conn, {:error, :invalid_username}) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "There is no account associated to this username"})
  end

  def call(conn, {:error, :invalid_password}) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "Invalid password"})
  end

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: changeset.errors })
  end

  def call(conn, {:error, :no_token}) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "No token"})
  end

  def call(conn, {:error, :already_connected}) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "User is already connected"})
  end
end
