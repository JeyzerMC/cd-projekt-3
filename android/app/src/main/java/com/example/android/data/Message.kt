package com.example.android.data

import android.graphics.Color
import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.SpannedString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan

fun getUserColor(username: String): Int {
    val hash: Int = username.hashCode()
    val r = (hash and 0xFF0000) shr 16
    val g = (hash and 0x00FF00) shr 8
    val b = (hash and 0xFF0000)
    return Color.argb(130, r, 0, b)
}

data class Message(
    val time: String,
    val sender: String,
    val content: String
) {

    override fun toString(): String {
        return "[$time] [$sender] $content"
    }

    fun toSpannedString(): SpannedString {
        // TODO: this is ugly use image span later https://www.dev2qa.com/android-spannablestring-example/
        // TODO: Refactor to ktx spans
        val color = getUserColor(sender)

        val timeSpan = SpannableString(time)
        timeSpan.setSpan(
            ForegroundColorSpan(color), 0,  time.length,
            Spanned.SPAN_INCLUSIVE_EXCLUSIVE
        )
        val senderSpan = SpannableString(sender)
        senderSpan.setSpan(
            ForegroundColorSpan(color), 0,  sender.length,
            Spanned.SPAN_INCLUSIVE_EXCLUSIVE
        )
        senderSpan.setSpan(
            StyleSpan(Typeface.BOLD), 0,  sender.length,
            Spanned.SPAN_INCLUSIVE_EXCLUSIVE
        )
        val contentSpan = SpannableString(content)
        contentSpan.setSpan(
            StyleSpan(Typeface.ITALIC), 0,  content.length,
            Spanned.SPAN_INCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(
            "[",
            timeSpan,
            "][",
            senderSpan,
            "] : ",
            contentSpan
        ) as SpannedString
    }
}