defmodule Server.Agents.ClassicGamesAgent do
  use Agent
  alias Server.Structs.User
  alias Server.Structs.Game
  alias Server.Repo
  alias Server.Models.User, as: UserModel

  alias Server.Game.BaseGame

  @name __MODULE__
  @min_players 1
  @max_players 3

  def start_link(_) do
    Agent.start_link(fn -> Map.new() end, name: @name)
  end

  def create_lobby(username) do
    # id = Ecto.UUID.generate()
    id = BaseGame.get_random_word() <> BaseGame.get_random_word()

    user = get_user_from_repo(username)

    Agent.update(@name, fn map ->
      map
      |> Map.put_new(id, %Game{
        nb_players: @min_players,
        max_players: @max_players,
        team1: [
          %User{username: user.username, avatar: user.current_avatar, title: user.selected_title}
        ],
        type: "classic"
      })
    end)

    IO.puts("creating lobby for classic")

    %{id: id, team: "team1"}
  end

  def join_lobby(id, username) do
    lobby = get_lobby(id)

    user = get_user_from_repo(username)
    IO.puts("the user is")
    IO.inspect(user)

    if Enum.count(lobby.team1) <= Enum.count(lobby.team2) do
      update_lobby(id, %Game{
        lobby
        | nb_players: lobby.nb_players + 1,
          team1:
            lobby.team1 ++
              [
                %User{
                  username: user.username,
                  avatar: user.current_avatar,
                  title: user.selected_title
                }
              ],
          open:
            if lobby.nb_players == @max_players do
              false
            else
              true
            end
      })

      "team1"
    else
      update_lobby(id, %Game{
        lobby
        | nb_players: lobby.nb_players + 1,
          team2:
            lobby.team2 ++
              [
                %User{
                  username: user.username,
                  avatar: user.current_avatar,
                  title: user.selected_title
                }
              ],
          open:
            if lobby.nb_players == @max_players do
              false
            else
              true
            end
      })

      "team2"
    end
  end

  def leave_lobby(id, username) do
    lobby = get_lobby(id)

    IO.puts("#{username} is LEAVING LOBBY")
    # IO.inspect(get_lobbies())
    # IO.puts("first")
    # IO.inspect(lobby)

    if lobby do
      game_contains_user =
        Enum.any?(lobby.team1 ++ lobby.team2, fn user ->
          user.username == username
        end)

      if game_contains_user do
        case lobby.nb_players do
          @min_players ->
            Agent.update(@name, fn map ->
              map
              |> Map.delete(id)
            end)

          _ ->
            update_lobby(id, %Game{
              lobby
              | team1: Enum.filter(lobby.team1, fn user -> user.username != username end),
                team2: Enum.filter(lobby.team2, fn user -> user.username != username end),
                open:
                  if lobby.end_game do
                    false
                  else
                    true
                  end,
                nb_players: lobby.nb_players - 1
            })

            lobby = get_lobby(id)

            # IO.puts("second")
            # IO.inspect(lobby)

            if Enum.all?(lobby.team1, fn user -> user.type == "AI" end) &&
                 Enum.all?(lobby.team2, fn user -> user.type == "AI" end) do
              Agent.update(@name, fn map ->
                map
                |> Map.delete(id)
              end)
            end
        end
      end
    end
  end

  def delete_lobby(id) do
    Agent.update(@name, fn map ->
      map
      |> Map.delete(id)
    end)
  end

  def start_game(id) do
    lobby = get_lobby(id)

    update_lobby(id, %Game{lobby | started: true})
  end

  def get_lobbies_for_update() do
    lobbies = Agent.get(@name, fn map -> map end)

    for {id, lobby} <- lobbies, into: %{}, do: {id, %{lobby | ai_draw_pid: nil, timer_pid: nil}}
  end

  def get_lobbies() do
    Agent.get(@name, fn map -> map end)
  end

  def get_open_lobbies() do
    all_lobbies = get_lobbies()

    :maps.filter(
      fn _id, lobby_params -> lobby_params.open and !lobby_params.started end,
      all_lobbies
    )
  end

  def get_current_drawer(id) do
    lobby = get_lobby(id)

    lobby.current_drawer
  end

  def get_current_word(id) do
    lobby = get_lobby(id)

    lobby.current_word
  end

  def get_lobby(id) do
    lobbies = get_lobbies()
    lobby = Map.get(lobbies, id)
    lobby
  end

  def is_started(id) do
    lobby = get_lobby(id)
    lobby.started
  end

  def update_lobby(id, lobby_params) do
    Agent.update(@name, fn map ->
      map
      |> Map.put(id, lobby_params)
    end)
  end

  def get_user_from_repo(username) do
    Repo.get_by!(UserModel, username: username)
  end
end
