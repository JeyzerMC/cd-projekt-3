defmodule ServerWeb.WordController do
  use ServerWeb, :controller

  alias Server.Repo
  alias Server.Models.Word

  action_fallback ServerWeb.FallbackController

  def get_words(conn, _) do
    with words <- Repo.all(Word) do
      res = Jason.encode!(words)
      conn |> json(%{body: res})
    end
  end

  def create_word(conn, %{"word" => word_params }) do
    with {:ok, %Word{}} <- add_word(Jason.decode!(word_params)) do
      conn |> json(%{body: "Word was added"})
    else
      _ ->
      conn |> json(%{body: "Couldn't add the word"})
    end
  end

  def delete_word(conn, %{"name" => word_name}) do
    with word <- Repo.get_by(Word, name: word_name),
         {:ok, _} <- Repo.delete(word) do
      conn |> json(%{body: true})
    else
      _ ->
        IO.puts "ERROR DELETING THE WORD"
        conn |> json(%{body: false})
    end
  end

  defp add_word(word) do
    %Word{}
    |> Word.changeset(word)
    |> Repo.insert()
  end
end
