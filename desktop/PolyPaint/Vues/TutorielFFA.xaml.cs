﻿using PolyPaint.Services;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Logique d'interaction pour TutorielFFA.xaml
    /// </summary>
    public partial class TutorielFFA : UserControl
    {
        int currentSlide = 0;
        int MAX_SLIDES = 6;

        List<string> descriptions = new List<string>()
        {
            "Général:\nChacun joue à son compte. Il peut y avoir jusqu'à 4 joueurs.\nDevinez le plus de dessins pour gagner.",

            "Préparation:\nLorsque vous êtes prêt, un joueur peut choisir de COMMENCER LA PARTIE.",

            "Début de partie:\nAprès avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\nLorsque celle-ci disparait, la partie commence immédiatement.",

            "Déroulement de partie:\nVotre tour:\nSi vous voyez un mot en gras en bas de l'écran, vous avez le rôle de DESSINEUR.\nAvec les outils de dessin, essayez de dessiner le mot en gras.\n",

            "Si vous avez un champ pour deviner en bas de l'écran, vous avez le rôle de DEVINEUR.\nEssayez de deviner ce que le DESSINEUR dessine.\nVous pouvez réessayer jusqu'à l'obtention de la bonne réponse OU jusqu'à l'écoulement du temps alloué.",

            "Lorsque le temps est écoulé, le mot à deviner s'affiche. Il est possible de voter pour le dessin et de le sauvegarder.\nEnsuite, c'est le tour d'un autre joueur d'être le DESSINEUR.",

            "Fin de partie:\nUne manche est finie après que chaque joueur a été une fois le DESSINATEUR.\n La partie se termine après deux manches.\nLe gagnant est celui qui a amassé le plus de points!",
        };

        public TutorielFFA()
        {
            InitializeComponent();

            ImageBorder0.PreviewMouseDown += (s, e) => SlideTo(0);
            ImageBorder1.PreviewMouseDown += (s, e) => SlideTo(1);
            ImageBorder2.PreviewMouseDown += (s, e) => SlideTo(2);
            ImageBorder3.PreviewMouseDown += (s, e) => SlideTo(3);
            ImageBorder4.PreviewMouseDown += (s, e) => SlideTo(4);
            ImageBorder5.PreviewMouseDown += (s, e) => SlideTo(5);
            ImageBorder6.PreviewMouseDown += (s, e) => SlideTo(6);
        }

        public void CloseTutorial(object sender, MouseEventArgs e)
        {
            SlideTo(0);
            Navigator.HideTutorial();
        }

        public void PreviousSlide(object sender, MouseEventArgs e)
        {
            var slide = currentSlide - 1;

            if (slide == -1)
            {
                slide = MAX_SLIDES;
            }

            SlideTo(slide);
        }

        public void NextSlide(object sender, MouseEventArgs e)
        {
            var slide = currentSlide + 1;

            if (slide == MAX_SLIDES + 1)
            {
                slide = 0;
            }

            SlideTo(slide);
        }

        public void SlideTo(int slide)
        {
            Dispatcher.Invoke(() =>
            {
                ((Border)FindName("ImageBorder" + currentSlide)).Background = (SolidColorBrush)new BrushConverter().ConvertFrom("Gray"); ;
                currentSlide = slide;
                Description.Text = descriptions[currentSlide];
                ((Border)FindName("ImageBorder" + currentSlide)).Background = (SolidColorBrush)new BrushConverter().ConvertFrom("RoyalBlue");
                ((Image)FindName("TutorialImage")).Source = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Tutorial/tutorial{currentSlide + 13}.PNG"));
            });
        }
    }
}
