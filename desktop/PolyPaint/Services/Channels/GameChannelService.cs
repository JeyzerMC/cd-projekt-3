﻿using Phoenix;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyPaint.Services.Channels
{
    class GameChannelService
    {
        public static Channel gameChannel { get { return _gameChannel; } }

        private static Channel _gameChannel { get; set; }

        public static void JoinGameChannel(string gameType)
        {
            _gameChannel = SocketService.Socket.MakeChannel(gameType);

            if (ChatService.Discussions.ContainsKey("Game"))
                ChatService.Discussions.Remove("Game");

            ChatService.Discussions.Add("Game", new Modeles.Discussion());
            ChatService.Discussions["Game"].Type = "GM";

            _gameChannel.On("shout", m =>
            {
                string username = m.payload["username"].ToString();
                string message = m.payload["message"].ToString();

                Trace.Write("Username: " + username);
                Trace.WriteLine("Message:" + message);

                ChatService.Discussions["Game"].Messages.Add(new Modeles.Message(username, message, Navigator.GetCurrentTime()));
                ChatService.triggerGameMessage.Add(true);
            });

            _gameChannel.Join()
              .Receive(Reply.Status.Ok, r => Trace.WriteLine("Joined GameChannel."))
              .Receive(Reply.Status.Error, r => Trace.WriteLine("Error in GameChannel."));
        }

        public static void LeaveGame()
        {
            if (ChatService.Discussions.ContainsKey("Game"))
            {
                ChatService.Discussions.Remove("Game");
                _gameChannel.Leave();
            }
        }

        public static void Shout(string message)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("message", message);
            _gameChannel.Push("shout", payload);
        }
    }
}
