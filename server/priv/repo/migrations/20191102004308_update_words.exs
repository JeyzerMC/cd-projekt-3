defmodule Server.Repo.Migrations.UpdateWords do
  use Ecto.Migration

  def change do
    alter table(:words) do
      modify :mode, :string
      modify :difficulty, :string
    end
  end
end
