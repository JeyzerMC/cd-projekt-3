﻿using System.Collections.Generic;
using System.Windows;

namespace PolyPaint.Modeles.Dessin
{
    class DessinSegment
    {
        public List<DessinStroke> Points { get; set; }

        public DessinSegment()
        {
            Points = new List<DessinStroke>();
        }
    }
}
