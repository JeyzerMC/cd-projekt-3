defmodule Server.Repo.Migrations.CreateWords do
  use Ecto.Migration

  def change do
    create table(:words) do
      add :name, :string
      add :mode, :integer
      add :difficulty, :integer
      add :image, :string
      add :tips, {:array, :string}

      timestamps()
    end
  end
end
