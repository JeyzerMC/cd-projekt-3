﻿using Phoenix;
using PolyPaint.Modeles;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PolyPaint.Services
{
    public static class ChatService
    {

        public static string currentChat = "General";

        public static Channel roomChannel;

        public static ObservableCollection<string> players = new ObservableCollection<string>();

        public static ObservableCollection<Tuple<string, int>> CanauxExistants = new ObservableCollection<Tuple<string, int>>();

        public static Dictionary<string, Discussion> Discussions { get; set; } = new Dictionary<string, Discussion>();

        public static ObservableCollection<bool> triggerGameMessage = new ObservableCollection<bool>();

        public static ObservableCollection<bool> updateControl = new ObservableCollection<bool>();

        public static void UpdateUsersList(string users)
        {
            players.Clear();

            while (users.Contains("\""))
            {
                users = users.Substring(users.IndexOf("\"") + 1);
                var user = users.Substring(0, users.IndexOf("\""));
                users = users.Substring(users.IndexOf("\"") + 1);
                if (user != UserInformation.Username)
                    players.Add(user);
            }
        }

        public static void Reset()
        {
            Discussions.Clear();
            triggerGameMessage.Clear();
            currentChat = "General";
            updateControl.Add(true);
        }
    }
}
