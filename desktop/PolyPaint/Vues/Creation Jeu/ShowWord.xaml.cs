﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using PolyPaint.Modeles;
using PolyPaint.Modeles.Dessin;
using PolyPaint.Services;
using PolyPaint.VueModeles;
using Svg;


namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class ShowWord : Page
    {
        public Word word = new Word();

        public ShowWord(Word newWord)
        {
            InitializeComponent();
            word = newWord;

        }

        public void ShowWordLoaded(object sender, RoutedEventArgs e)
        {
            ShowDifficulty.Content = word.difficulty;
            wordShow.Content = word.name;
            Indices.ItemsSource = word.tips;
            showImage();
        }

        private void showImage() {
            //SvgDocument document = SvgDocument.FromSvg<SvgDocument>(word.image);
            //System.Drawing.Bitmap bmp = document.Draw();
            //Draw.Source = BitmapToBitmapImage(bmp, ImageFormat.Png);
            Dispatcher.Invoke(async () =>
            {
                while(true)
                {
                    PreviewCanvas.Children.Clear();
                    foreach (var stroke in word.strokes)
                    {
                        ProcessTask(stroke);
                        await Task.Delay(1);
                    }
                }
            });
        }

        private void ProcessTask(string stroke)
        {
            var pt = JsonConvert.DeserializeObject<DessinStroke>(stroke);

            Line line = new Line();
            line.Stroke = new SolidColorBrush(pt.Color);

            if (pt.Tip == StylusTip.Ellipse)
            {
                line.StrokeStartLineCap = PenLineCap.Round;
                line.StrokeEndLineCap = PenLineCap.Round;
            }
            else
            {
                line.StrokeStartLineCap = PenLineCap.Square;
                line.StrokeEndLineCap = PenLineCap.Square;
            }

            line.StrokeThickness = pt.Width * PreviewCanvas.ActualWidth;
            line.X1 = pt.X * PreviewCanvas.ActualWidth;
            line.Y1 = pt.Y * PreviewCanvas.ActualHeight;
            line.X2 = pt.X2 * PreviewCanvas.ActualWidth;
            line.Y2 = pt.Y2 * PreviewCanvas.ActualHeight;
            PreviewCanvas.Children.Add(line);
        }

        public static BitmapImage BitmapToBitmapImage(System.Drawing.Bitmap bitmap, ImageFormat imageFormat)
        {
            BitmapImage bitmapImage = new BitmapImage();
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, imageFormat);
                memory.Position = 0;
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
            }
            return bitmapImage;
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            //NavigateToPage("Vues/Creation Jeu/ListWord.xaml");
            Navigator.GoBack();
        }
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Vues/Creation Jeu/CreateWord.xaml");
        }

        private void Indices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
