﻿using PolyPaint.Services;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PolyPaint.Vues
{
    /// <summary>
    /// Logique d'interaction pour TutorielSolo.xaml
    /// </summary>
    public partial class TutorielSolo : UserControl
    {
        int currentSlide = 0;
        int MAX_SLIDES = 5;

        List<string> descriptions = new List<string>()
        {
            "Général:\nVous jouez en solo contre la montre! Devinez le plus de dessins possible dans le laps de temps accordé.",

            "Préparation:\nLorsque vous êtes prêt, vous pouvez choisir de COMMENCER LA PARTIE.",

            "Début de partie:\nAprès avoir COMMENCER LA PARTIE, un écran s'affiche avec Préparez-vous!!!\nLorsque celle-ci disparait, la partie commence immédiatement.",

            "Déroulement de partie:\nDevinez le mot qui est en train de se faire dessiner en entrant votre réponse dans le champs en bas de l'écran.\nLe temps bonus accordé varie selon la difficulté du dessin.",

            "Lorsque vous donnez la bonne réponse, vous passez au prochain dessin.",

            "Fin de partie:\nLa partie se termine lorsque le temps est écoulé.",
        };

        public TutorielSolo()
        {
            InitializeComponent();

            ImageBorder0.PreviewMouseDown += (s, e) => SlideTo(0);
            ImageBorder1.PreviewMouseDown += (s, e) => SlideTo(1);
            ImageBorder2.PreviewMouseDown += (s, e) => SlideTo(2);
            ImageBorder3.PreviewMouseDown += (s, e) => SlideTo(3);
            ImageBorder4.PreviewMouseDown += (s, e) => SlideTo(4);
            ImageBorder5.PreviewMouseDown += (s, e) => SlideTo(5);
        }

        public void CloseTutorial(object sender, MouseEventArgs e)
        {
            SlideTo(0);
            Navigator.HideTutorial();
        }

        public void PreviousSlide(object sender, MouseEventArgs e)
        {
            var slide = currentSlide - 1;

            if (slide == -1)
            {
                slide = MAX_SLIDES;
            }

            SlideTo(slide);
        }

        public void NextSlide(object sender, MouseEventArgs e)
        {
            var slide = currentSlide + 1;

            if (slide == MAX_SLIDES + 1)
            {
                slide = 0;
            }

            SlideTo(slide);
        }

        public void SlideTo(int slide)
        {
            Dispatcher.Invoke(() =>
            {
                ((Border)FindName("ImageBorder" + currentSlide)).Background = (SolidColorBrush)new BrushConverter().ConvertFrom("Gray"); ;
                currentSlide = slide;
                Description.Text = descriptions[currentSlide];
                ((Border)FindName("ImageBorder" + currentSlide)).Background = (SolidColorBrush)new BrushConverter().ConvertFrom("RoyalBlue");
                ((Image)FindName("TutorialImage")).Source = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Tutorial/tutorial{currentSlide + 8}.PNG"));
            });
        }
    }
}
