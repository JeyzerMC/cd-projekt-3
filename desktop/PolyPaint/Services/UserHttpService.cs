﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Diagnostics;
using Newtonsoft.Json;
using PolyPaint.Modeles;

namespace PolyPaint.Services
{
    public static class UserHttpService
    {
        public static async Task<string> UserSignup(string json)
        {
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var httpResponse = await HttpService.Client.PostAsync(HttpService.ApiEndpoint + "/sign_up", httpContent);
            Trace.WriteLine(httpResponse);
            var res = await httpResponse.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);

            return responseObj["jwt"];
        }

        public static async Task<string> UserSignin(Dictionary<string, string> userInfo)
        {
            var parameters = new FormUrlEncodedContent(userInfo);
            var httpResponse = await HttpService.Client.PostAsync(HttpService.ApiEndpoint + "/sign_in", parameters);
            var res = await httpResponse.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);

            return responseObj["jwt"];
        }
        
        public static async Task<Profil> FetchUserProfile()
        {
            var httpResponse = await HttpService.Client.GetAsync(HttpService.ApiEndpoint + "/profile");
            var res = await httpResponse.Content.ReadAsStringAsync();

            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);

            if (!responseObj.ContainsKey("body"))
            {
                return new Profil();
            }

            var body = responseObj["body"];

            var profile = JsonConvert.DeserializeObject<Profil>(body);

            return profile;
        }

        public static async Task<Profil> UpdateUserProfile(string json)
        {
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var httpResponse = await HttpService.Client.PostAsync(HttpService.ApiEndpoint + "/update_profile", httpContent);
            var res = await httpResponse.Content.ReadAsStringAsync();

            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);

            if (!responseObj.ContainsKey("body"))
            {
                return new Profil();
            }

            var body = responseObj["body"];

            var profile = JsonConvert.DeserializeObject<Profil>(body);

            return profile;
        }

        public static async Task<Profil> FetchProfileByName(string username)
        {
            var httpResponse = await HttpService.Client.GetAsync(HttpService.ApiEndpoint + "/public_profile?username=" + username);
            var res = await httpResponse.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);

            if (!responseObj.ContainsKey("body"))
            {
                return new Profil();
            }

            var body = responseObj["body"];

            var profile = JsonConvert.DeserializeObject<Profil>(body);
            return profile;
        }
    }
}
