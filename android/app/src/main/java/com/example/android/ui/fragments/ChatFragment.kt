package com.example.android.ui.fragments

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.android.R
import com.example.android.databinding.FragmentChatBinding
import com.example.android.viewmodel.ChatViewModel

data class OnlineChannel(val name: String, val numberOfUsers: String)
private const val TAG = "Chat"

class ChatFragment : androidx.fragment.app.Fragment() {

    private val viewModel: ChatViewModel by viewModels()
    private lateinit var chatSendButton: Button
    private lateinit var chatInput: EditText
    private lateinit var chatText: TextView
    private lateinit var chatGeneralButton: Button
    private lateinit var chatGroupButton: Button
    private lateinit var currentChatTitle: TextView
    private lateinit var currentJoinedChannels: LinearLayout
    private lateinit var pinnedChannels: LinearLayout
    private lateinit var channelsScrollView: ScrollView
    private lateinit var channelsListLayout: LinearLayout
    private lateinit var toggleChannelsListButton: Button
    private lateinit var quitChannelButton: Button
    private lateinit var onlineUsersList: LinearLayout
    private lateinit var createChannelButton: Button
    private lateinit var createChannelLayout: LinearLayout
    private lateinit var channelNameInput: EditText
    private lateinit var channelNameSendButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentChatBinding.inflate(layoutInflater, container, false)
        val view = binding.root

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        initViews(view)
        viewModel.showAddChannelsList.value = false

        toggleChannelsListButton.background = createCircleShape(ResourcesCompat.getColor(resources, R.color.orange, null))
        toggleChannelsListButton.setOnClickListener {
            viewModel.showAddChannelsList.postValue(!viewModel.showAddChannelsList.value!!)
        }
        quitChannelButton.setOnClickListener {
            onClickQuitChannelButton(view)
        }

        setChatButton(chatGeneralButton, "GE", "GENERAL", Color.WHITE)

        setChatButton(chatGroupButton, "GR", "GROUPE", ResourcesCompat.getColor(resources, R.color.bluePrimary, null))

        chatSendButton.setOnClickListener(this@ChatFragment::onChatSendButtonClick)

        chatInput.setOnEditorActionListener { textView: TextView, actionId: Int, event: KeyEvent? ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE, EditorInfo.IME_ACTION_SEARCH -> {
                    onChatInputSend(textView)
                    return@setOnEditorActionListener true
                }
            }
            when (event?.action) {
                KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER -> {
                    if (!event.isShiftPressed)
                        onChatInputSend(textView)
                    return@setOnEditorActionListener true
                }
            }
            false
        }

        createChannelButton.setOnClickListener {
            createChannelButton.visibility = View.GONE
            createChannelLayout.visibility = View.VISIBLE
        }

        channelNameSendButton.setOnClickListener {onChannelNameSendButtonClick(it)}

        channelNameInput.setOnEditorActionListener { textView: TextView, actionId: Int, event: KeyEvent? ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    onChannelNameSend(textView)
                    return@setOnEditorActionListener true
                }
                else -> false
            }
        }

        createChannelButton.setOnClickListener {
            createChannelButton.visibility = View.GONE
            createChannelLayout.visibility = View.VISIBLE
        }
        viewModel.chatsContents.observe(this, Observer {
            val scrollView: ScrollView = view.findViewById(R.id.chatScroll)
            val runnable = Runnable { scrollView.fullScroll(ScrollView.FOCUS_DOWN) }
            scrollView.postDelayed(runnable, 100)
            val currentChat = it[viewModel.currentChat]
            println(currentChat)
            viewModel.currentChatContent.value = it[viewModel.currentChat]
        })

        viewModel.newMessageSender.observe(this, Observer {
            if(viewModel.currentChat != it) {
                val button = view.findViewWithTag<Button>(it)

                if (button == null) {
                    createChannelButton(it)
                    val newButton = view.findViewWithTag<Button>(it)
                    toggleMessageNotification(newButton, true)
                } else
                    toggleMessageNotification(button, true)
            }
        })

        viewModel.onlineChannels.observe(this, Observer { channel ->
            channelsListLayout.removeAllViews()
            channel.forEach { channel -> addOnlineChannel(channel) }

        })

        // viewModel.chats.observe(this, Observer {
        //     currentJoinedChannels.removeAllViews()
        //     val users = it.keys.toList()
        //     users.subList(2, users.size).forEach {username ->
        //         if (viewModel.username != username)
        //             addChannelButton(username)
        //     }
        // })

        viewModel.onlineUsers.observe(this, Observer { users ->
            onlineUsersList.removeAllViews()
            users.forEach { username -> if (username != viewModel.username) addOnlineUser(username) }
        })

        return view
    }

    private fun initViews(view: View) {
        chatSendButton = view.findViewById(R.id.chatSendButton)
        chatInput = view.findViewById(R.id.chatInput)
        chatText = view.findViewById(R.id.chatText)
        currentJoinedChannels = view.findViewById(R.id.onlineUserChatsLayout)
        chatGeneralButton = view.findViewById(R.id.generalChatButton)
        chatGroupButton = view.findViewById(R.id.groupChatButton)
        currentChatTitle = view.findViewById(R.id.currentChatTitle)
        pinnedChannels = view.findViewById(R.id.pinnedChatsLayout)
        channelsListLayout = view.findViewById(R.id.channelsList)
        toggleChannelsListButton = view.findViewById(R.id.toggleChannelsListButton)
        channelsScrollView = view.findViewById(R.id.channelsScrollView)
        quitChannelButton = view.findViewById(R.id.quitChannelButton)
        onlineUsersList = view.findViewById(R.id.onlineUsersList)
        createChannelButton = view.findViewById(R.id.createChannelButton)
        createChannelLayout = view.findViewById(R.id.createChannelLayout)
        channelNameInput = view.findViewById(R.id.channelNameInput)
        channelNameSendButton = view.findViewById(R.id.createChannelNameButton)
    }

    private fun toggleMessageNotification(button: Button, isDisplayed: Boolean) {
        button.setCompoundDrawablesRelativeWithIntrinsicBounds(
            null,
            null,
            if (isDisplayed) ResourcesCompat.getDrawable(resources, R.drawable.notification_circle, null) else null,
            null
        )
    }

    private fun setChatButton(button: Button, text: String, chatRoom: String, color: Int) {
        button.background = createRoundedRectShape(color)
        button.tag = chatRoom
        button.text = text
        button.setTextColor(ResourcesCompat.getColor(resources, R.color.blueDark, null))

        button.setOnClickListener {
            onClickChannelButton(button)
        }
    }

    private fun addOnlineChannel(channel: OnlineChannel) {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.HORIZONTAL
        layout.setPadding(0,0,0,30)
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layout.layoutParams = params

        val paramsContent = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        paramsContent.weight = 0.5f

        val usernameTextView = TextView(context)
        usernameTextView.text = channel.name
        usernameTextView.layoutParams = paramsContent
        layout.addView(usernameTextView)

        val joinButton = Button(context)
        paramsContent.height = 40
        joinButton.layoutParams = paramsContent
        joinButton.gravity = Gravity.CENTER
        joinButton.text = "Joindre"
        joinButton.textSize = 10f
        joinButton.setPadding(5,5,5,5)
        joinButton.setTextColor(Color.WHITE)
        joinButton.background = createRoundedRectShape(ResourcesCompat.getColor(resources, R.color.bluePrimary, null))
        joinButton.setOnClickListener {
            addChannelButton("CH: ${channel.name}")
            viewModel.onJoinChannel(channel.name)
        }
        layout.addView(joinButton)

        channelsListLayout.addView(layout)
    }

    private fun addOnlineUser(username: String){

        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.HORIZONTAL
        layout.setPadding(0,0,0,30)
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layout.layoutParams = params

        val paramsContent = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        paramsContent.weight = 0.5f

        val usernameTextView = TextView(context)
        usernameTextView.text = username
        usernameTextView.layoutParams = paramsContent
        layout.addView(usernameTextView)

        val dmButton = Button(context)
        paramsContent.height = 40
        dmButton.layoutParams = paramsContent
        dmButton.gravity = Gravity.CENTER
        dmButton.text = "Contacter"
        dmButton.textSize = 10f
        dmButton.setPadding(5,5,5,5)
        dmButton.setTextColor(Color.WHITE)
        dmButton.background = createRoundedRectShape(ResourcesCompat.getColor(resources, R.color.bluePrimary, null))
        dmButton.setOnClickListener {
            addChannelButton("DM: $username")
        }
        layout.addView(dmButton)

        onlineUsersList.addView(layout)
    }

    private fun createChannelButton(username: String) {
        val button = Button(context)
        button.tag = username
        button.text = username[4].toString() + if(username[5] != null) username[5].toString() else ""
        button.setTextColor(ResourcesCompat.getColor(resources, R.color.blueDark, null))

        button.background = createRoundedRectShape(ResourcesCompat.getColor(resources, R.color.bluePrimary, null))

        button.height = 20
        button.width = 20

        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(0, 5, 0, 5)
        button.layoutParams = params

        button.setOnClickListener {
            onClickChannelButton(button)
        }

        currentJoinedChannels.addView(button)
    }

    private fun addChannelButton(username: String){
        val button = currentJoinedChannels.findViewWithTag<Button>(username)
        if (button == null) {
            createChannelButton(username)
            val newButton = currentJoinedChannels.findViewWithTag<Button>(username)
            onClickChannelButton(newButton)
        }
        else
            onClickChannelButton(button)
    }

    private fun createRoundedRectShape(color: Int): Drawable {
        val unwrappedDrawable = ResourcesCompat.getDrawable(resources, R.drawable.channel_button, null)
        val wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable!!)
        DrawableCompat.setTint(wrappedDrawable, color)

        return wrappedDrawable
    }

    private fun onClickQuitChannelButton(view: View){
        if(viewModel.currentChat.substring(0,2) == "CH")
            viewModel.onLeaveChannel(viewModel.currentChat.substring(4))
        currentJoinedChannels.removeView(view.findViewWithTag(viewModel.currentChat))
        viewModel.currentChat = "GENERAL"
        onClickChannelButton(view.findViewWithTag(viewModel.currentChat))
    }

    private fun onClickChannelButton(button : Button) {
        toggleMessageNotification(button, false)
        viewModel.currentChat = button.tag.toString()
        currentChatTitle.text = viewModel.currentChat

        val blue = ResourcesCompat.getColor(resources, R.color.bluePrimary, null)
        for(i in 0 until pinnedChannels.childCount) {
            val child = pinnedChannels.getChildAt(i)
            child.background = createRoundedRectShape(blue)
        }

        for (i in 0 until currentJoinedChannels.childCount) {
            if (currentJoinedChannels.childCount != 0) {
                val child = currentJoinedChannels.getChildAt(i)
                child.background = createRoundedRectShape(blue)
            }
        }
        button.background = createRoundedRectShape(Color.WHITE)

        quitChannelButton.visibility = if (viewModel.currentChat == "GENERAL" || viewModel.currentChat == "GROUPE") View.GONE else View.VISIBLE
        viewModel.showAddChannelsList.postValue(false)
    }

    private fun onChatInputSend(textView: TextView) {
        Log.v(TAG, "enter pressed")
        val content = textView.text.toString()
        clearChatInput()

        if (content != ""){
            viewModel.onMessageSend(content)
        }
    }

    private fun onChannelNameSend(textView: TextView) {
        val content = textView.text.toString()
        channelNameInput.setText("")

        val contentNoSpace = content.replace("\\s".toRegex(), "")
        if (contentNoSpace != "" && content != "GENERAL" && content != "GROUPE")
            viewModel.onCreateChannel(content)
        else
            toast("Nom de canal invalide.")
    }

    private fun onChannelNameSendButtonClick(buttonView: View) {
        Log.v(TAG, "send button click")
        val content = channelNameInput.text.toString()
        channelNameInput.setText("")

        val contentNoSpace = content.replace("\\s".toRegex(), "")
        if (contentNoSpace != "" && content != "GENERAL" && content != "GROUPE")
            viewModel.onCreateChannel(content)
        else
            toast("Nom de canal invalide.")
    }

    private fun onChatSendButtonClick(buttonView: View) {
        Log.v(TAG, "send button click")
        val content = chatInput.text.toString()
        clearChatInput()

        if (content != ""){
            viewModel.onMessageSend(content)
        }
    }

    private fun clearChatInput() {
        chatInput.setText("")
    }

    private fun createCircleShape(color: Int): GradientDrawable {
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.OVAL
        shape.setColor(color)
        return shape
    }

    private fun closeKeyboard() {
        val imm: InputMethodManager = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    private fun toast(message: String) {
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(context, message, duration)
            toast.show()
    }
}


