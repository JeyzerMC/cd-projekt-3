defmodule Server.Structs.Game do
  @derive {Jason.Encoder, except: [:__meta__]}
  defstruct nb_players: 0,
            users: [],
            team1: [],
            team2: [],
            current_drawer: "",
            current_team: "",
            open: true,
            started: false,
            word: "",
            end_game: false,
            end_game_classic_hack: false,
            change_turn: false,
            first_round_ffa: false,
            correct_guesses: 0,
            max_players: 0,
            timer_pid: nil,
            already_drawn: [],
            ai_draw_pid: nil,
            max_guesses: 0,
            current_guesses: 0,
            game_difficulty: "",
            word_difficulty: "",
            word_time_bonus: 0,
            game_start_time: nil,
            round_start_time: nil,
            type: "",
            guessed: false,
            round_counter: 0,
            round: 0,
            vote: 0
end
