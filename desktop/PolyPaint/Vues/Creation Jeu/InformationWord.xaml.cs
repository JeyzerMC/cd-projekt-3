﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PolyPaint.Modeles;
using PolyPaint.Services;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class InformationWord : Page
    {
        public InformationWord()
        {
            InitializeComponent();
        }
        private Word word;
        public InformationWord(Word newWord)
        {
            InitializeComponent();
            word = newWord;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            //Navigator.Navigate(new LexicalFieldWord(word));
            Navigator.GoBack();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            foreach(var radioButton in (Difficulty as Grid).Children) {
                if((radioButton as RadioButton).IsChecked == true)
                    word.difficulty = (radioButton as RadioButton).Content.ToString();
            }

            foreach(var radioButton in (Mode as Grid).Children)
            {
                if ((radioButton as RadioButton).IsChecked == true)
                    word.mode = (radioButton as RadioButton).Content.ToString();
            }

            if (word.mode == "Panoramique") {
                Navigator.Navigate(new ParametrePanoramique(word));
            } else if (word.mode == "Centré") {
                Navigator.Navigate(new ParametreCentre(word));
            } else {
                WordCreationService.AddWord(word).ContinueWith(resp =>
                {
                    Trace.WriteLine("RESPONSE FROM CREATE WORLD");
                    Trace.WriteLine(resp);
                    Dispatcher.Invoke(() =>
                    {
                        Navigator.Navigate(new ListWord());
                    });
                });
            }
        }

        public Word newWord;

        void NavigationService_LoadCompleted(object sender, NavigationEventArgs e)
        {
            newWord = (Word)e.ExtraData;
        }
    }
}
