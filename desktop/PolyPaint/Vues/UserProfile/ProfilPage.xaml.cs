﻿using Newtonsoft.Json;
using PolyPaint.Modeles;
using PolyPaint.Services;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace PolyPaint.Vues.UserProfile
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class ProfilPage : Page
    {
        Profil profil;

        public ProfilPage(Profil profile)
        {
            InitializeComponent();
            profil = profile;
            
        }

        public void ProfilLoaded(object sender, RoutedEventArgs e)
        {
            InitializeProfile();
        }

        public void InitializeProfile()
        {
            PseudoProfil.Content = profil.username;

            NbrPartieJouerProfil.Content = profil.nb_games;
            RatioVictoireProfil.Content = profil.ratio_win;

            ClassiqueProfil.Content = profil.hs_classic;
            SoloProfil.Content = profil.hs_solo;
            FfaProfil.Content = profil.hs_ffa;

            TempsTotalProfil.Content = profil.total_hours;
            TempsMoyenProfil.Content = string.Format("{0:F2}", GetMediumTime(profil.total_hours, profil.nb_games));

            ExpBar.Value = profil.exp_points % 10;

            updateAcomplissement(profil.achievements);
            updatePrestige(profil.exp_points / 10);
            UpdateAvatar();
            UpdateTitle();
        }

        private void updateAcomplissement(List<int> achievements)
        {
            Dispatcher.Invoke(() =>
            {
                //for (int i = 0; i < 7; i++)
                //{
                //    var btn = ((RadioButton)FindName("AchievementRB" + i));
                //    btn.IsEnabled = true;
                //    var idx = i;
                //    btn.Click += (s, e) => SelectTitle(idx);
                //}

                if (profil.selected_title != -1)
                    ((RadioButton)FindName("AchievementRB" + profil.selected_title)).IsChecked = true;
            });
        }

        // =======================================
        // =======================================
        //            CLIQUE ON BUTTON
        // =======================================
        // =======================================

        private void ClickButton0(object sender, RoutedEventArgs e)
        {
            SelectTitle(0);
        }

        private void ClickButton1(object sender, RoutedEventArgs e)
        {
            SelectTitle(1);
        }

        private void ClickButton2(object sender, RoutedEventArgs e)
        {
            SelectTitle(2);
        }

        private void ClickButton3(object sender, RoutedEventArgs e)
        {
            SelectTitle(3);
        }

        private void ClickButton4(object sender, RoutedEventArgs e)
        {
            SelectTitle(4);
        }

        private void ClickButton5(object sender, RoutedEventArgs e)
        {
            SelectTitle(5);
        }

        private void ClickButton6(object sender, RoutedEventArgs e)
        {
            SelectTitle(6);
        }

        // ================
        // AVATARSSS
        // ================

        private void ClickAvatar0(object sender, RoutedEventArgs e)
        {
            SelectAvatar(0);
        }

        private void ClickAvatar1(object sender, RoutedEventArgs e)
        {
            SelectAvatar(1);
        }

        private void ClickAvatar2(object sender, RoutedEventArgs e)
        {
            SelectAvatar(2);
        }

        private void ClickAvatar3(object sender, RoutedEventArgs e)
        {
            SelectAvatar(3);
        }

        private void ClickAvatar4(object sender, RoutedEventArgs e)
        {
            SelectAvatar(4);
        }

        // =======================================
        // =======================================
        //           END CLIQUE ON BUTTON
        // =======================================
        // =======================================

        private void updatePrestige(int threshold)
        {
            Dispatcher.Invoke(() =>
            {
                for (int i = 0; i <= threshold; i++)
                {
                    if (i > 4)
                    {
                        break;
                    }

                    var btn = ((RadioButton)FindName("BtnAvatar" + i));
                    var img = ((Image)FindName("Avatar" + i));

                    btn.IsEnabled = true;
                    img.IsEnabled = true;

                    var idx = i;
                    //btn.Click += (s, e) => SelectAvatar(idx);

                    img.Opacity = 1.0;
                }

            ((RadioButton)FindName("BtnAvatar" + profil.current_avatar)).IsChecked = true;
            });
        }

        public void SelectTitle(int index)
        {
            Dispatcher.Invoke(async () =>
            {
                var btn = ((RadioButton)FindName("AchievementRB" + index));
                btn.IsEnabled = false;

                var dic = new Dictionary<string, int>();
                dic.Add("selected_title", index);
                var new_profil = await UserHttpService.UpdateUserProfile(JsonConvert.SerializeObject(dic));
                profil = new_profil;

                btn.IsEnabled = true;
                InitializeProfile();
            });
        }

        public void SelectAvatar(int index)
        {
            Dispatcher.Invoke(async () =>
            {
                var dic = new Dictionary<string, int>();
                dic.Add("current_avatar", index);
                var new_profil = await UserHttpService.UpdateUserProfile(JsonConvert.SerializeObject(dic));
                profil = new_profil;
                InitializeProfile();
            });
        }

        private double GetMediumTime(int time, int nbParties)
        {
            if (nbParties == 0)
            {
                return 0.0;
            }

            return time / nbParties;
        }

        private void UpdateAvatar()
        {
            AvatarProfil.Source = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Avatars/Avatar{profil.current_avatar}.png"));
        }

        private void UpdateTitle()
        {
            TitreProfil.Content = ((Label)FindName("AchievementTitle" + profil.selected_title)).Content;
            SelectedImage.Source = ((Image)FindName("Badge" + profil.selected_title)).Source;
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            Navigator.GoBack();
        }
    }
}
