defmodule ServerWeb.FfaGameChannel do
  use ServerWeb, :channel
  alias Server.Agents.FfaGameAgent
  alias Server.Game.FfaGame
  alias Server.Structs.Game
  alias Server.Game.BaseGame
  alias ServerWeb.ChatChannel

  def join("ffa_game:" <> _game_id, _, socket) do
    {:ok, socket}
  end

  def start_timer(socket) do
    "ffa_game:" <> game_id = socket.topic

    {:ok, task_pid} =
      Task.start(fn ->
        # TODO: MAKE SURE TIME IS COHERENT WITH CLIENTS
        :timer.sleep(45000)
        IO.puts("round ending")

        change_turn(socket)
      end)

    current_game = FfaGameAgent.get_lobby(game_id)
    FfaGameAgent.update_lobby(game_id, %Game{current_game | timer_pid: task_pid})
  end

  def change_turn(socket) do
    broadcast!(socket, "vote", %{})
    IO.puts("")
    Process.send_after(socket.channel_pid, :change_turn, 7000)
  end

  def handle_info(:end_game, socket) do
    "ffa_game:" <> game_id = socket.topic

    game =
      FfaGameAgent.get_lobby(game_id)
      |> BaseGame.endgame()

    FfaGameAgent.update_lobby(game_id, game)

    # TODO: send end game statistics

    broadcast(socket, "end_game", %{users: game.users})

    {:noreply, socket}
  end

  def handle_info(:vote, socket) do
    broadcast!(socket, "vote", %{})
    {:noreply, socket}
  end

  def handle_info(:change_turn, socket) do
    "ffa_game:" <> game_id = socket.topic

    # TODO: CHECK IF END

    game =
      FfaGameAgent.get_lobby(game_id)
      |> FfaGame.change_turn()

    FfaGameAgent.update_lobby(game_id, game)

    # IO.puts("CHANGING TURN======================================")
    # IO.inspect(FfaGameAgent.get_lobbies())
    # IO.puts("CHANGING TURN======================================")

    if game.end_game do
      IO.puts("BROADCAST END GAME")
      send(self(), :end_game)
      {:noreply, socket}
    else
      IO.puts("BROADCAST CHANGE TURN")
      start_timer(socket)

      broadcast(socket, "change_turn", %{
        drawer: game.current_drawer,
        word: game.word
      })

      {:noreply, socket}
    end
  end

  def handle_in("draw", payload, socket) do
    "ffa_game:" <> game_id = socket.topic

    game = FfaGameAgent.get_lobby(game_id)

    if game.started do
      if(game.current_drawer.username == socket.assigns.username) do
        broadcast_from!(socket, "draw", payload)
        {:noreply, socket}
      else
        {:noreply, socket}
      end
    else
      broadcast_from(socket, "draw", payload)
      {:noreply, socket}
    end
  end

  def handle_in("start_game", _payload, socket) do
    "ffa_game:" <> game_id = socket.topic
    IO.puts("#{socket.assigns.username} has started the game")

    # TODO: start timer

    game =
      FfaGameAgent.get_lobby(game_id)
      |> FfaGame.start_game()

    FfaGameAgent.update_lobby(game_id, game)

    # IO.puts("STARTING GAME======================================")
    # IO.inspect(FfaGameAgent.get_lobbies())
    # IO.puts("STARTING GAME======================================")

    # TODO: broadcast update lobbies to lobby
    ServerWeb.Endpoint.broadcast!(
      "lobby:ffa",
      "lobbies_update",
      FfaGameAgent.get_lobbies_for_update()
    )

    broadcast!(socket, "start_game", %{})
    Process.send_after(self(), :change_turn, 3000)

    {:noreply, socket}
  end

  def handle_in("guess", %{"word" => word}, socket) do
    "ffa_game:" <> game_id = socket.topic

    # IO.puts("BEFORE GUESS======================================")
    # IO.inspect(FfaGameAgent.get_lobby(game_id))
    # IO.puts("BEFORE GUESS======================================")

    game =
      FfaGameAgent.get_lobby(game_id)
      |> FfaGame.validate_guess(word, socket.assigns.username)

    FfaGameAgent.update_lobby(game_id, game)

    # IO.puts("AFTER GUESS======================================")
    # IO.inspect(FfaGameAgent.get_lobbies())
    # IO.puts("AFTER GUESS======================================")

    if game.change_turn do
      IO.puts("killing timer process")
      Process.exit(game.timer_pid, :ok)
      # TODO: MAKE SURE TIME COHERENT WITH CLIENTS
      Process.send_after(self(), :vote, 1000)
      Process.send_after(self(), :change_turn, 7000)
      {:noreply, socket}
    end

    if word == game.word do
      broadcast!(socket, "score", %{users: game.users})
      {:reply, {:ok, %{guessed: true, message: "You guessed the correct word"}}, socket}
    else
      {:reply, {:ok, %{guessed: false, message: "You did not guess the correct word"}}, socket}
    end
  end

  def handle_in("vote", %{"rating" => rating}, socket) do
    # TODO: GET USER, ADD RATING TO PRESTIGE AND INSERT TO REPO
    IO.puts("GETTING A VOTE #{rating}")
    "ffa_game:" <> game_id = socket.topic
    game = FfaGameAgent.get_lobby(game_id)

    FfaGameAgent.update_lobby(game_id, %Game{game | vote: game.vote + rating})
    {:noreply, socket}
  end

  def handle_in("shout", %{"message" => message}, socket) do
    message = ChatChannel.check_censure(message)

    broadcast(socket, "shout", %{
      username: socket.assigns.username,
      message: message
    })

    {:noreply, socket}
  end

  def terminate(reason, socket) do
    "ffa_game:" <> game_id = socket.topic

    IO.puts("user leaved ffa game #{game_id}")
    IO.puts(socket.assigns.username)
    IO.inspect(reason)

    lobbies = FfaGameAgent.get_lobbies()

    if Map.has_key?(lobbies, game_id) do
      game = FfaGameAgent.get_lobby(game_id)

      if game.started do
        IO.puts("user leave during ffa game")
        broadcast!(socket, "game_error", %{})
        FfaGameAgent.delete_lobby(game_id)
      else
        FfaGameAgent.leave_lobby(game_id, socket.assigns.username)

        ServerWeb.Endpoint.broadcast(
          "lobby:ffa",
          "lobbies_update",
          FfaGameAgent.get_lobbies_for_update()
        )
      end
    else
      IO.puts("lobbies already terminated")
    end

    :ok
  end
end
