package com.example.android.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.VolleyError
import com.example.android.data.ChatRepository
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class Title(val name: String, val description: String)

data class Profile(
    @SerializedName("username")
    val username: String,
    @SerializedName("classicHighScore")
    val classicHighScore: Int,
    @SerializedName("hs_solo")
    val hs_solo: Int,
    @SerializedName("hs_ffa")
    val hs_ffa: Int,
    @SerializedName("nb_games")
    val nb_games: Int,
    @SerializedName("ratio_win")
    val ratio_win: Double,
    @SerializedName("total_hours")
    val total_hours: Int,
    @SerializedName("achievements")
    val achievements: List<Int>,
    @SerializedName("exp_points")
    val exp_points: Int,
    @SerializedName("selected_title")
    var selected_title: Int,
    @SerializedName("current_avatar")
    var current_avatar: Int) {

    fun getTitleName(): String {
        return titles[selected_title].name
    }

    companion object {
        val titles: List<Title> = listOf(
            Title( "Petit debutant", "Bienvenue au jeu"),
            Title( "Bob Ross", "Grand dessinateur"),
            Title( "Sanic", "Vraiment tres rapide"),
            Title( "Gros chef bandit", "Assassin"),
            Title( "Damien de la tour", "Se deplace uniquement en ligne"),
            Title( "Geralt of Rivia", "Le witcher est arrive Toussaint"),
            Title( "Vla Poutine", "Vraiment delicieux")
        )
    }
}

class ProfileViewModel: ViewModel(){
    val profile: MutableLiveData<Profile> = MutableLiveData()
    val publicProfile: MutableLiveData<Profile> = MutableLiveData()
    val repository = ChatRepository.getInstance()
    var isMyProfile: MutableLiveData<Boolean> = MutableLiveData()

    suspend fun getProfile() = repository.getProfile()

    suspend fun updateTitle(newTitleId: Int): Pair<Profile?, VolleyError?> {
        val newProfile = profile.value!!.copy()
        newProfile.selected_title = newTitleId
        return repository.updateProfile(newProfile)
    }

    suspend fun updateAvatar(newAvatarId: Int): Pair<Profile?, VolleyError?> {
        // todo: find a better way to do a deep copy here
        val gson = Gson()
        val newProfile = gson.fromJson(gson.toJson(profile.value!!), Profile::class.java)
        newProfile.current_avatar = newAvatarId
        return repository.updateProfile(newProfile)
    }

    suspend fun getPublicProfile(username: String) = repository.getPublicProfile(username)
}