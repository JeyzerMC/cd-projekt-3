defmodule Server.Game.Strokes do
  def parse_stroke(stroke) do
    new_string = String.replace(stroke, "\'", "\"")
    {:ok, decoded} = Jason.decode(new_string)

    "#" <> color_string = decoded["Color"]
    {parsed_color, ""} = Integer.parse(color_string, 16)

    <<complement_color::integer-signed-32>> = <<parsed_color::integer-unsigned-32>>

    {x, ""} = Float.parse(decoded["X"])
    {y, ""} = Float.parse(decoded["Y"])
    {x2, ""} = Float.parse(decoded["X2"])
    {y2, ""} = Float.parse(decoded["Y2"])
    {width, ""} = Float.parse(decoded["Width"])


    %{
      startX: x,
      startY: y,
      endX: x2,
      endY: y2,
      color: complement_color,
      width: width,
      style: String.downcase(decoded["Tip"])
    }
  end
end
