using System.Collections.Generic;

namespace PolyPaint.Modeles
{
    public class Profil
    {
        public string username;

        public int hs_classic;
        public int hs_solo;
        public int hs_ffa;

        public int nb_games;
        public double ratio_win;
        public int total_hours;
        
        public int selected_title;
        public List<int> achievements;

        public int current_avatar;
        public int exp_points;
    }
}
