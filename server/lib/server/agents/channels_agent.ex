defmodule Server.Agents.ChannelsAgent do
  use Agent
  alias ServerWeb.Presence
  @name __MODULE__

  def start_link(_) do
    Agent.start_link(fn -> Map.new() end, name: @name)
  end

  # Get the list of channels
  def get_channels() do
    Agent.get(@name, fn map -> map end)
  end

  # Add a new channel
  def add_channel(channel, username) do
    Agent.update(@name, fn map ->
      map
      |> Map.put_new(channel, [username])
    end)
  end

  # Join the given channel
  def join_channel(channel, username) do
    Agent.update(@name, fn map ->
      map
      |> Map.update!(channel, fn val ->
        val ++ [username]
      end)
    end)
  end

  # Leave the given channel
  def leave_channel(channel, username) do
    IO.puts "LEAVING CHANNEL #{channel}"
    Agent.update(@name, fn map ->
      map
      |> Map.update!(channel, fn val ->
        val |> List.delete(username)
      end)
    end)

    # Check if empty channels and delete
    Enum.each(get_channels(), fn {k, v} ->
      if length(v) == 0 do
        Agent.update(@name, fn m ->
          Map.delete(m, k)
        end)
      end
    end)

    IO.puts "LEFT CHANNEL #{channel}"
    IO.puts "STATE OF CHANNELS"
  end
end
