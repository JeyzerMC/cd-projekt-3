﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Controls.Primitives;
using PolyPaint.VueModeles;
using System.IO;
using PolyPaint.Modeles;
using System.Collections.Generic;
using CsPotrace;
using PolyPaint.Services;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class DrawWord : Page
    {
        Word word;

        public DrawWord()
        {
            InitializeComponent();
            DataContext = new VueModele();
            word = new Word();
        }

        public DrawWord(Word newWord)
        {
            InitializeComponent();
            DataContext = new VueModele();
            word = newWord;
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            Navigator.GoBack();
        }

        private void SaveImage(object sender, RoutedEventArgs e)
        {
            var name = word.name;
            var dirPath = Path.Combine(Directory.GetCurrentDirectory(), "Dessins");
            Directory.CreateDirectory(dirPath);
           
            var final = dirPath + name;

            WriteableBitmap dessin = SaveAsWriteableBitmap(DrawingCanvas);
            string svg = SvgFromWriteableBitmap(dessin, final);
            word.image = svg;

            var traits = (DataContext as VueModele).Traits;
            var height = DrawingCanvas.ActualHeight;
            var width = DrawingCanvas.ActualWidth;

            Navigator.Navigate(new Previsualisation(word, traits, height, width));
        }

        private string SvgFromWriteableBitmap(WriteableBitmap writeBmp, string path)
        {
            var pathBmp = path + ".bmp";
            var pathSvg = path + ".svg";
            System.Drawing.Bitmap bmp;
            using (FileStream outStream = new FileStream(pathBmp, FileMode.Create))
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((BitmapSource)writeBmp));
                enc.Save(outStream);
                bmp = new System.Drawing.Bitmap(outStream);
                Potrace.Potrace_Trace(bmp, new List<List<Curve>>());
                string svg = Potrace.getSVG();
                StreamWriter writer = new StreamWriter(pathSvg);
                writer.WriteLine(svg);
                writer.Flush();
                writer.Close();
                Potrace.Clear();

                return svg;
            }
        }

        public WriteableBitmap SaveAsWriteableBitmap(InkCanvas surface)
        {
            if (surface == null) return null;

            // Save current canvas transform
            Transform transform = surface.LayoutTransform;
            // reset current transform (in case it is scaled or rotated)
            surface.LayoutTransform = null;

            // Get the size of canvas
            Size size = new Size(surface.ActualWidth, surface.ActualHeight);
            // Measure and arrange the surface
            // VERY IMPORTANT
            surface.Measure(size);
            surface.Arrange(new Rect(size));

            // Create a render bitmap and push the surface to it
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
              (int)size.Width,
              (int)size.Height,
              96d,
              96d,
              PixelFormats.Pbgra32);
            renderBitmap.Render(surface);

            //Restore previously saved layout
            surface.LayoutTransform = transform;

            //create and return a new WriteableBitmap using the RenderTargetBitmap
            return new WriteableBitmap(renderBitmap);
        }

        // Pour gérer les points de contrôles.
        private void GlisserCommence(object sender, DragStartedEventArgs e) => (sender as Thumb).Background = Brushes.Black;
        private void GlisserTermine(object sender, DragCompletedEventArgs e) => (sender as Thumb).Background = Brushes.White;
        private void GlisserMouvementRecu(object sender, DragDeltaEventArgs e)
        {
            string nom = (sender as Thumb).Name;
            //if (nom == "horizontal" || nom == "diagonal") colonne.Width = new GridLength(Math.Max(32, colonne.Width.Value + e.HorizontalChange));
            //if (nom == "vertical" || nom == "diagonal") ligne.Height = new GridLength(Math.Max(32, ligne.Height.Value + e.VerticalChange));
        }

        // Pour la gestion de l'affichage de position du pointeur.
        //private void surfaceDessin_MouseLeave(object sender, MouseEventArgs e) => textBlockPosition.Text = "";
        private void surfaceDessin_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.GetPosition(DrawingCanvas);
            //textBlockPosition.Text = Math.Round(p.X) + ", " + Math.Round(p.Y) + "px";
        }

        private void DupliquerSelection(object sender, RoutedEventArgs e)
        {
            DrawingCanvas.CopySelection();
            DrawingCanvas.Paste();
        }

        private void SupprimerSelection(object sender, RoutedEventArgs e) => DrawingCanvas.CutSelection();
    }
}
