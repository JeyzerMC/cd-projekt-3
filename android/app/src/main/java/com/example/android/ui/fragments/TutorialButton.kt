package com.example.android.ui.fragments

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.android.R

class TutorialButton : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_tutorial_button, container, false)
        view.findViewById<Button>(R.id.tutorialButton).setOnClickListener { onClickTutorial() }

        val shape = GradientDrawable()
        shape.shape = GradientDrawable.OVAL
        shape.setColor(Color.WHITE)
        val tutorialButton = view.findViewById<Button>(R.id.tutorialButton)
        tutorialButton.text = "?"
        tutorialButton.background = shape

        return view
    }

    private fun onClickTutorial() {
        TutorialDialog().show(activity!!.supportFragmentManager, "?")
    }
}
