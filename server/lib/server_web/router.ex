defmodule ServerWeb.Router do
  use ServerWeb, :router

  alias Server.Auth

  pipeline :api do
    plug(CORSPlug, origin: "*")
    plug :accepts, ["json"]
  end

  pipeline :jwt_authenticated do
    plug Auth.AuthPipeline
  end

  scope "/api", ServerWeb do
    pipe_through :api

    post "/sign_up", UserController, :sign_up
    post "/sign_in", UserController, :sign_in

    get "/get_words", WordController, :get_words
    post "/create_word", WordController, :create_word
    post "/delete_word", WordController, :delete_word

    get "/fetch_dico", DicoController, :fetch_words
    post "/add_dico_entry", DicoController, :add_word

    get "/public_profile", UserController, :get_public_profile
  end

  scope "/api", ServerWeb do
    pipe_through [:api, :jwt_authenticated]

    get "/profile", UserController, :get_profile
    post "/update_profile", UserController, :update_profile

    post "/sign_out", UserController, :sign_out
  end
end
