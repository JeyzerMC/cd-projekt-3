﻿using CsPotrace;
using PolyPaint.Modeles;
using PolyPaint.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Logique d'interaction pour searchOnlinePage.xaml
    /// </summary>
    public partial class SearchOnlinePage : Page
    {

        public Word word;
        public int indexImage;
        private MatchCollection urls;
        private Bitmap bitmap;
        public SearchOnlinePage(Word newWord)
        {
            InitializeComponent();
            word = newWord;
            string html = GetHtmlCode();
            string pattern = @",""ou"":""(.*?)"",";
            urls = Regex.Matches(html, pattern); //GetUrls(html);
            if (urls.Count <= 0)
            {
                errorMessage.Visibility = Visibility.Visible;
                suivant.IsEnabled = false;
                reload.IsEnabled = false;
            }
            else
            {
                generateImage();
                indexImage = 0;
            }


        }
        //source: https://stackoverflow.com/questions/27846337/select-and-download-random-image-from-google
        private string GetHtmlCode()
        {
            //string url = "https://www.google.com/search?q=" + word.name + "&tbs=sur:fmc,itp:lineart&tbm=isch";
            string url = "https://www.google.com/search?q=" + word.name + "&as_st=y&hl=en&tbas=0&tbs=itp:lineart,ift:png,sur:fm&tbm=isch&sxsrf=ACYBGNSXDiJqdWNHWSX-EeujcWMVtH8-Yg:1574778613777&source=lnt&sa=X&ved=0ahUKEwi_zf6-i4jmAhUurlkKHSFRDJMQpwUIJA&biw=1500&bih=890&dpr=2";
            string data = "";

            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Accept = "text/html, application/xhtml+xml, /";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko";

            var response = (HttpWebResponse)request.GetResponse();

            using (Stream dataStream = response.GetResponseStream())
            {
                if (dataStream == null)
                    return "";
                using (var sr = new StreamReader(dataStream))
                {
                    data = sr.ReadToEnd();
                }
            }
            return data;
        }

        private System.Drawing.Image GetImage(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
        
            using (Stream dataStream = response.GetResponseStream())
            {
                if (dataStream == null)
                {
                    return null;
                }
                    
                try
                {
                    Console.WriteLine(url);
                    var originalCoverData = new System.Net.WebClient().DownloadData(url);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream(originalCoverData);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
                    reload.IsEnabled = true;
                    suivant.IsEnabled = true;
                    return img;
        
                }
                catch (System.ArgumentException) {
                    generateImage();
                }
                    //img.Save(response.GetResponseStream(), System.Drawing.Imaging.ImageFormat.Jpeg);
        
                //System.Drawing.Image image = System.Drawing.Image.FromStream(new MemoryStream(da));
                
                
                //using (var sr = new BinaryReader(dataStream))
                //{
                //    byte[] bytes = sr.ReadBytes(100000);
                //
                //    using (MemoryStream ms = new MemoryStream(bytes))
                //    {
                //        try
                //        {
                //            System.Drawing.Image img = (System.Drawing.Image)System.Drawing.Image.FromStream(ms);
                //            return img;
                //        } catch (System.ArgumentException)
                //        { }
                //    }
                //
                //}
            }
        
                return null;
        }

        //private byte[] GetImage(string url)
        //{
        //    var request = (HttpWebRequest)WebRequest.Create(url);
        //    var response = (HttpWebResponse)request.GetResponse();
        //
        //    using (Stream dataStream = response.GetResponseStream())
        //    {
        //        if (dataStream == null)
        //            return null;
        //        using (var sr = new BinaryReader(dataStream))
        //        {
        //            byte[] bytes = sr.ReadBytes(100000000);
        //
        //            return bytes;
        //        }
        //    }
        //
        //    return null;
        //}

        private void generateImage() {

            
            var rnd = new Random();
            int randomUrl;
            if (urls.Count <= 0)
            {
                //generateImage();
            }
            else
            {
                randomUrl = indexImage;
                indexImage++;
                string luckyUrl = urls[randomUrl].Groups[1].Value;
                uploadImage(luckyUrl);
            }

            


            
            //byte[] image = GetImage(luckyUrl);
            //BitmapImage imageGoogle = LoadImage(image);
            //pictureBox1.Source = imageGoogle;

        }

        //source : https://stackoverflow.com/questions/9564174/convert-byte-array-to-image-in-wpf
 
        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }
        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        private bool uploadImage(string url)
        {
            //byte[] tableauByte = GetImage(url);
            //System.Drawing.Image image = byteArrayToImage(tableauByte);
            System.Drawing.Image image = GetImage(url);
            if (image != null)
            {
                var bmp = new Bitmap(image);
                IntPtr bmpPt = bmp.GetHbitmap();
                BitmapSource bitmapSource =
                 System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                       bmpPt,
                       IntPtr.Zero,
                       Int32Rect.Empty,
                       BitmapSizeOptions.FromEmptyOptions());

                bitmapSource.Freeze();
                pictureBox1.Source = bitmapSource;

                bitmap = GetBitmap(bitmapSource);
                return true;
            }
            return false;
        }
        private Bitmap GetBitmap(BitmapSource source)
        {
            Bitmap bmp = new Bitmap(
              source.PixelWidth,
              source.PixelHeight,
              System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            BitmapData data = bmp.LockBits(
              new System.Drawing.Rectangle(System.Drawing.Point.Empty, bmp.Size),
              ImageLockMode.WriteOnly,
              System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            source.CopyPixels(
              Int32Rect.Empty,
              data.Scan0,
              data.Height * data.Stride,
              data.Stride);
            bmp.UnlockBits(data);
            return bmp;
        }


        private void ImportImage(object sender, RoutedEventArgs e)
        {
            
            Potrace.Potrace_Trace(bitmap, new List<List<Curve>>());
            word.image = Potrace.getSVG();
            Navigator.Navigate(new SelectPicture(word, bitmap));
        }

        private void reloadImage(object sender, RoutedEventArgs e)
        {
            reload.IsEnabled = false;
            suivant.IsEnabled = false;
            generateImage();
        }
    }
}
