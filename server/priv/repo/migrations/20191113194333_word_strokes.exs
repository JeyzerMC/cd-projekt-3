defmodule Server.Repo.Migrations.WordStrokes do
  use Ecto.Migration

  def change do
    alter table(:words) do
      remove :mode
      add :strokes, {:array, :text}
    end
  end
end
