﻿using Newtonsoft.Json;
using Phoenix;
using PolyPaint.Modeles;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyPaint.Services.Channels
{
    class LobbyChannelService
    {
        public static Channel lobbyChannel { get { return _lobbyChannel; } }
        private static Channel _lobbyChannel { get; set; }

        public static List<Player> team1players { get { return _team1players; } }
        private static List<Player> _team1players { get; set; }
        public static List<Player> team2players { get { return _team2players; } }
        private static List<Player> _team2players { get; set; }

        public static void JoinLobbyChannel(string lobbyType)
        {
            _lobbyChannel = SocketService.Socket.MakeChannel(lobbyType);
            _lobbyChannel.Join()
              .Receive(Reply.Status.Ok, r => Trace.WriteLine("Joined LobbyChannel."))
              .Receive(Reply.Status.Error, r => Trace.WriteLine("Error in LobbyChannel."));
        }

        public static void ResetLobbyChannel()
        {
            _lobbyChannel = null;
        }

        public static void SubscribeGetAvatars(string lobbyId)
        {
            LobbyChannelService.lobbyChannel.On("lobbies_update", m =>
            {
                Trace.WriteLine("lobbies_update SubscribeGetAvatars.");
                Dictionary<string, Lobby> returnedLobbies = JsonConvert.DeserializeObject<Dictionary<string, Lobby>>(m.payload.ToString());
                if(returnedLobbies.ContainsKey(lobbyId))
                {
                    Lobby joinedLobby = JsonConvert.DeserializeObject<Dictionary<string, Lobby>>(m.payload.ToString())[lobbyId];
                    _team1players = joinedLobby.users;
                }
            });
        }

        public static void SubscribeGetTeamAvatars(string lobbyId)
        {
            LobbyChannelService.lobbyChannel.On("lobbies_update", m =>
            {
                Trace.WriteLine("lobbies_update SubscribeGetTeamAvatars.");
                Dictionary<string, TeamLobby> returnedLobbies = JsonConvert.DeserializeObject<Dictionary<string, TeamLobby>>(m.payload.ToString());
                if (returnedLobbies.ContainsKey(lobbyId))
                {
                    TeamLobby joinedLobby = JsonConvert.DeserializeObject<Dictionary<string, TeamLobby>>(m.payload.ToString())[lobbyId];
                    _team1players = joinedLobby.team1;
                    _team2players = joinedLobby.team2;
                }
            });
        }
    }
}
