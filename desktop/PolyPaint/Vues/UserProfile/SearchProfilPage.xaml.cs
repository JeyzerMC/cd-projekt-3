﻿using PolyPaint.Modeles;
using PolyPaint.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PolyPaint.Vues.UserProfile
{
    /// <summary>
    /// Logique d'interaction pour SearchProfilPage.xaml
    /// </summary>
    public partial class SearchProfilPage : Page
    {

        private readonly List<string> Titres = new List<string>
        {
            "Petit débutant",
            "Bob Ross",
            "Sanic",
            "Gros chef bandit",
            "Damien de la Tour",
            "Geralt of Rivia",
            "Vla Poutine"
        };

        public SearchProfilPage()
        {
            InitializeComponent();
        }

        private void UpdatePublicProfile(Profil profile)
        {
            PseudoProfilPublique.Content = profile.username;
            AvatarProfilPublique.Source = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Avatars/Avatar{profile.current_avatar}.png"));
            TitreProfilPublique.Content = Titres[profile.selected_title];
            SelectedImage.Source = new BitmapImage(new Uri($"pack://application:,,,/PolyPaint;component/Resources/Badges/badge{profile.selected_title}.png"));



            NbrPartieJouerProfilPublique.Content = profile.nb_games;
            RatioVictoireProfilPublique.Content = profile.ratio_win;

            ClassiqueProfilPublique.Content = profile.hs_classic;
            SoloProfilPublique.Content = profile.hs_solo;
            FfaProfilPublique.Content = profile.hs_ffa;

            TempsTotalProfilPublique.Content = profile.total_hours;
            if (profile.nb_games > 0)
            {
                TempsMoyenProfilPublique.Content = profile.total_hours / profile.nb_games;
            } else
            {
                TempsMoyenProfilPublique.Content = "0.0";
            }
        }

        private void FindUser(object sender, KeyEventArgs e)
        {
            OtherUserError.Visibility = Visibility.Hidden;

            if (e.Key == Key.Enter)
            {
                if (OtherUserTextbox.Text != "")
                {
                    Dispatcher.Invoke(async () =>
                    {
                        var profile = await UserHttpService.FetchProfileByName(OtherUserTextbox.Text);
                        if (profile.username != null)
                        {
                            borderProfil.Visibility = Visibility.Visible;
                            UpdatePublicProfile(profile);
                            
                        }
                        else
                            OtherUserError.Visibility = Visibility.Visible;
                    });
                }
            }
        }
    }
}
