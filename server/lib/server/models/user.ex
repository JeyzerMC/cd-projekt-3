defmodule Server.Models.User do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder,
           except: [
             :__meta__,
             :id,
             :password,
             :password_confirmation,
             :first_name,
             :last_name,
             :updated_at,
             :inserted_at
           ]}

  schema "users" do
    field :username, :string
    field :first_name, :string
    field :last_name, :string
    field :password, :string
    field :password_confirmation, :string, virtual: true

    field :hs_classic, :integer
    field :hs_solo, :integer
    field :hs_ffa, :integer

    field :nb_games, :integer
    field :ratio_win, :float
    field :total_hours, :integer

    field :selected_title, :integer
    field :achievements, {:array, :integer}

    field :current_avatar, :integer
    field :exp_points, :integer

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [
      :username,
      :first_name,
      :last_name,
      :password,
      :password_confirmation,
      :hs_classic,
      :hs_solo,
      :hs_ffa,
      :nb_games,
      :ratio_win,
      :total_hours,
      :selected_title,
      :achievements,
      :current_avatar,
      :exp_points
    ])
    |> unique_constraint(:username)
    |> validate_username(:username)
  end

  def validate_username(changeset, field) do
    validate_change(changeset, field, fn _, username ->
      username = String.downcase(username)

      case String.starts_with?(username, "ai") do
        false -> []
        true -> [username: "cannot start with ai"]
      end
    end)
  end
end
