﻿using PolyPaint.Services;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PolyPaint.Vues.UserProfile
{
    /// <summary>
    /// Logique d'interaction pour selectionProfil.xaml
    /// </summary>
    public partial class selectionProfil : Page
    {
        public selectionProfil()
        {
            InitializeComponent();
            BrushConverter bc = new BrushConverter();
            btnProfil.Background = (Brush)bc.ConvertFrom("#2196F3");
            btnSearchProfil.Background = (Brush)bc.ConvertFrom("#2196F3");

            Navigator.GoBackButton.Content = "Retour";
        }

        private void Profil_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(async () =>
            {
                var profil = await UserHttpService.FetchUserProfile();
                Navigator.Navigate(new ProfilPage(profil));
            });
        }

        private void SearchProfil_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Vues/UserProfile/SearchProfilPage.xaml");
        }
    }
}
