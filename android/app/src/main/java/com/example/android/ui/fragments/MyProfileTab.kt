package com.example.android.ui.fragments

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.android.R

import com.example.android.databinding.FragmentMyProfileTabBinding
import com.example.android.viewmodel.Profile
import com.example.android.viewmodel.ProfileViewModel

class MyProfileTab : Fragment() {
    val model: ProfileViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentMyProfileTabBinding.inflate(layoutInflater, container, false)

        binding.profile = model.profile
        binding.lifecycleOwner = this


        model.profile.observe(this, Observer {profile ->
            updateAvatar(profile)
        })
        return binding.root
    }

    fun updateAvatar(profile: Profile) {
        val bitmapAvatar = BitmapFactory.decodeResource(resources, resources.getIdentifier("avatar_${profile.current_avatar}", "drawable", activity!!.packageName))
        activity!!.findViewById<ImageView>(R.id.avatar).setImageBitmap(
            Bitmap.createScaledBitmap(
                bitmapAvatar,
                400,
                400,
                true)
        )
    }
}
