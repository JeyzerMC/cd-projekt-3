package com.example.android.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.data.ChatRepository
import com.example.android.data.GameMode


data class Player(val name: String, val team: String?, val score: Int, val titleId: Int, val avatarId: Int, val isAI: Boolean)
data class Lobby(val id: String, val myUsername: String, val gameMode: GameMode, val open: Boolean, val nbPlayers: Int, val teams: Map<String, List<Player>>, val currentDrawer: String?, val currentDrawingTeam: String?) {

    fun info(): String {
       return "${if (id.length >= 4) id.substring(0,4) else ""} $currentDrawer $currentDrawingTeam"

    }

    fun isWinner(): Boolean {
        val candidates = getWinners()
        // TODO: are you always a winner in sprint solo?
        return when (gameMode ) {
            GameMode.CLASSIC -> {
                candidates.any { (team, score) -> teams[team]!!.count { user -> user.name == myUsername } > 0}
            }
            GameMode.SOLOSPRINT -> {
                true
            }
            GameMode.FFA -> {
                candidates.count { (username, score) -> username == myUsername } > 0
            }
            GameMode.NONE -> {true}
        }
    }

    fun getWinners(): List<Pair<String, Int>> {
        var scores: List<Pair<String, Int>> = listOf()

        when (gameMode) {
            GameMode.CLASSIC -> {
                scores = teams.map { (team, users) ->
                    Pair(team, users.sumBy { user -> user.score })
                }
            }
            GameMode.SOLOSPRINT -> {
                val user = teams["all"]!![0]
                scores = listOf(Pair(user.name, user.score))
            }
            GameMode.FFA -> {
                scores = teams["all"]!!.map { user ->
                    Pair(user.name, user.score)
                }
            }
        }
        val max = scores.maxBy { it.second }?.second?: -1

        return scores.filter { (username, score) -> score == max }
    }

    fun winnerText(): String? {
        val candidates = getWinners()

        return when (candidates.size) {
            1 -> "${candidates[0].first} emporte la victoire"
            else -> candidates.joinToString(", ") { (username, score) -> username} + " sont a Ex aequo"
        }
    }
}

class LobbyViewModel: ViewModel() {
    private val repository = ChatRepository.getInstance()
    val lobbies: MutableLiveData<Map<String, Lobby>> = repository.lobbies
    val selectedLobby: MutableLiveData<Lobby?> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    val gameMode = repository.gameMode

    init {
        loading.value = false
    }

    fun selectLobby(lobby: Lobby) {
        selectedLobby.postValue(lobby)
    }

    fun joinSelectedLobby() {
        repository.onJoinLobby(selectedLobby!!.value!!)
    }

    suspend fun createLobby() = repository.onCreateLobby()

    suspend fun joinLobbySelectionChannel() = repository.joinLobbySelectionChannel(gameMode.value!!)

    fun leave() = repository.leaveSelectionLobby()
}