﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using PolyPaint.Modeles;
using PolyPaint.Services;

namespace PolyPaint.Vues.Creation_Jeu
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class CreateWord : Page
    {
        public Word newWord;

        private IEnumerable<string> _currentWords;
        private bool _addAnyway = false;

        public CreateWord(List<Word> words)
        {
            InitializeComponent();
            newWord = new Word();
            _currentWords = words.Select(x => x.name);
            Navigator.GoBackButton.Content = "Retour";
        }

        private void NavigateBack(object sender, RoutedEventArgs e)
        {
            Navigator.GoBack();
        }

        private void word_keyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AddWord(sender, e);
                e.Handled = true;
            }

            errorLabel.Visibility = Visibility.Hidden;
            addAnywayLabel.Visibility = Visibility.Hidden;
            addAnywayCheckbox.Visibility = Visibility.Hidden;
            ajouterButton.IsEnabled = true;
            _addAnyway = false;
        }

        private void AddAnywayTrigger(object sender, RoutedEventArgs e)
        {
            _addAnyway = true;
            ajouterButton.IsEnabled = true;
        }

        private void AddWord(object sender, RoutedEventArgs e)
        {
            if (Input.Text != "")
            {
                newWord.name = Input.Text.ToLower();

                Dispatcher.Invoke(async () =>
                {
                    if (_addAnyway || await IsWordValid(newWord.name))
                    {
                        if (_addAnyway)
                            await DictionnaryService.AddEntry(newWord.name);

                        Navigator.Navigate(new ChoicePicture(newWord));
                    }
                    else
                    {
                        errorLabel.Visibility = Visibility.Visible;
                        ajouterButton.IsEnabled = false;
                    }
                });
            }
        }

        private async Task<bool> IsWordValid(string word)
        {
            if (_currentWords.Contains(word))
            {
                errorLabel.Text = "Il y'a deja une image associe a ce mot.";
                return false;
            }

            if (await InDictionnary(word))
            {
                return true;
            }

            errorLabel.Text = "Le mot n'est pas valide, voulez vous quand meme l'ajouter?";
            addAnywayCheckbox.Visibility = Visibility.Visible;
            addAnywayLabel.Visibility = Visibility.Visible;
            addAnywayCheckbox.IsChecked = false;

            return false;
        }

        private async Task<bool> InDictionnary(string word)
        {
            string[] lines = File.ReadAllLines(Directory.GetCurrentDirectory() + "\\..\\..\\Resources\\dico.txt");

            if (lines.Contains(word))
                return true;

            var res = await DictionnaryService.FetchDictionnary();
            return res.Contains(word);
        }
    }
}
