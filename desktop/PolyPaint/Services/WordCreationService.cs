﻿using Newtonsoft.Json;
using PolyPaint.Modeles;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PolyPaint.Services
{
    public static class WordCreationService
    {
        // Fetch the list of words from the server
        public async static Task<List<Word>> FetchWords()
        {
            var httpResponse = await HttpService.Client.GetAsync(HttpService.ApiEndpoint + "/get_words");

            var res = await httpResponse.Content.ReadAsStringAsync();

            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);
            var body = responseObj["body"];

            var words = JsonConvert.DeserializeObject<List<Word>>(body);

            return words;
        }

        // Add the word to the server and return whether the operation was succesful
        public async static Task<string> AddWord(Word word)
        {
            Dictionary<string, string> wordDic = new Dictionary<string, string>();
            word.image = word.image.Replace("\"", "\'");
            wordDic.Add("word", JsonConvert.SerializeObject(word));

            var httpContent = new StringContent(JsonConvert.SerializeObject(wordDic), Encoding.UTF8, "application/json");
            var httpResponse = await HttpService.Client.PostAsync(HttpService.ApiEndpoint + "/create_word", httpContent);
            Trace.WriteLine(httpResponse);

            var res = await httpResponse.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);

            return responseObj["body"];
        }

        // Delete the word from the server and return whether the operation was succesful
        public async static Task<string> DeleteWord(Word word)
        {
            Dictionary<string, string> wordDic = new Dictionary<string, string>();
            wordDic.Add("name", word.name);
            var httpContent = new StringContent(JsonConvert.SerializeObject(wordDic), Encoding.UTF8, "application/json");
            var httpResponse = await HttpService.Client.PostAsync(HttpService.ApiEndpoint + "/delete_word", httpContent);
            Trace.WriteLine(httpResponse);

            var res = await httpResponse.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);

            return responseObj["body"];
        }
    }
}
